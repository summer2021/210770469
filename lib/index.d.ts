import BaseComponent from './common/base';
import { InputProps } from './common/entity';
export default class FcTunnelInvokeComponent extends BaseComponent {
    constructor(props: any);
    static readonly supportedDebugIde: string[];
    report(componentName: string, command: string, accountID?: string, access?: string): Promise<void>;
    handlerInputs(inputs: InputProps): Promise<{
        [key: string]: any;
    }>;
    /**
     * setup
     * @param inputs
     * @returns
     */
    setup(inputs: InputProps): Promise<void>;
    /**
     * invoke
     * @param inputs
     * @returns
     */
    invoke(inputs: InputProps): Promise<void>;
    /**
     * clean
     * @param inputs
     * @returns
     */
    clean(inputs: InputProps): Promise<void>;
}
