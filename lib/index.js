"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var base_1 = __importDefault(require("./common/base"));
var logger_1 = __importDefault(require("./common/logger"));
var lodash_1 = __importDefault(require("lodash"));
var core = __importStar(require("@serverless-devs/core"));
var stdout_formatter_1 = __importDefault(require("./lib/component/stdout-formatter"));
var path_1 = __importDefault(require("path"));
var devs_1 = require("./lib/devs");
var tunnel_service_1 = __importDefault(require("./lib/tunnel-service"));
var local_invoke_1 = __importDefault(require("./lib/local-invoke/local-invoke"));
var validate_1 = require("./lib/validate");
var definition_1 = require("./lib/definition");
var debug_1 = require("./lib/local-invoke/debug");
var path_2 = require("./lib/utils/path");
var FcTunnelInvokeComponent = /** @class */ (function (_super) {
    __extends(FcTunnelInvokeComponent, _super);
    function FcTunnelInvokeComponent(props) {
        return _super.call(this, props) || this;
    }
    FcTunnelInvokeComponent.prototype.report = function (componentName, command, accountID, access) {
        return __awaiter(this, void 0, void 0, function () {
            var uid, credentials;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        uid = accountID;
                        if (!lodash_1.default.isEmpty(accountID)) return [3 /*break*/, 2];
                        return [4 /*yield*/, core.getCredential(access)];
                    case 1:
                        credentials = _a.sent();
                        uid = credentials.AccountID;
                        _a.label = 2;
                    case 2:
                        try {
                            core.reportComponent(componentName, {
                                command: command,
                                uid: uid,
                            });
                        }
                        catch (e) {
                            logger_1.default.warning(stdout_formatter_1.default.stdoutFormatter.warn('component report', "component name: " + componentName + ", method: " + command, e.message));
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    FcTunnelInvokeComponent.prototype.handlerInputs = function (inputs) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var project, access, creds, properties, appName, args, curPath, devsPath, nasBaseDir, baseDir, projectName, region, parsedArgs, argsData, serviceConfig, triggerConfigList, customDomainConfigList, functionConfig;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, stdout_formatter_1.default.initStdout()];
                    case 1:
                        _b.sent();
                        project = inputs === null || inputs === void 0 ? void 0 : inputs.project;
                        access = project === null || project === void 0 ? void 0 : project.access;
                        return [4 /*yield*/, core.getCredential(access)];
                    case 2:
                        creds = _b.sent();
                        validate_1.validateCredentials(creds);
                        return [4 /*yield*/, this.report('fc-tunnel-invoke', inputs === null || inputs === void 0 ? void 0 : inputs.command, creds === null || creds === void 0 ? void 0 : creds.AccountID, (_a = inputs === null || inputs === void 0 ? void 0 : inputs.project) === null || _a === void 0 ? void 0 : _a.access)];
                    case 3:
                        _b.sent();
                        properties = inputs === null || inputs === void 0 ? void 0 : inputs.props;
                        appName = inputs === null || inputs === void 0 ? void 0 : inputs.appName;
                        args = inputs === null || inputs === void 0 ? void 0 : inputs.args.replace(/(^\s*)|(\s*$)/g, '');
                        curPath = inputs === null || inputs === void 0 ? void 0 : inputs.path;
                        devsPath = curPath === null || curPath === void 0 ? void 0 : curPath.configPath;
                        nasBaseDir = devs_1.detectNasBaseDir(devsPath);
                        baseDir = path_1.default.dirname(devsPath);
                        projectName = project === null || project === void 0 ? void 0 : project.projectName;
                        region = properties.region;
                        parsedArgs = core.commandParse(inputs, {
                            boolean: ['help'],
                            alias: { help: 'h' },
                        });
                        argsData = (parsedArgs === null || parsedArgs === void 0 ? void 0 : parsedArgs.data) || {};
                        if (argsData === null || argsData === void 0 ? void 0 : argsData.help) {
                            return [2 /*return*/, {
                                    region: region,
                                    creds: creds,
                                    path: path_1.default,
                                    args: args,
                                    access: access,
                                    isHelp: true,
                                }];
                        }
                        serviceConfig = properties === null || properties === void 0 ? void 0 : properties.service;
                        triggerConfigList = properties === null || properties === void 0 ? void 0 : properties.triggers;
                        customDomainConfigList = properties === null || properties === void 0 ? void 0 : properties.customDomains;
                        functionConfig = devs_1.updateCodeUriWithBuildPath(baseDir, properties === null || properties === void 0 ? void 0 : properties.function, serviceConfig.name);
                        return [2 /*return*/, {
                                serviceConfig: serviceConfig,
                                functionConfig: functionConfig,
                                triggerConfigList: triggerConfigList,
                                customDomainConfigList: customDomainConfigList,
                                region: region,
                                creds: creds,
                                curPath: curPath,
                                args: args,
                                appName: appName,
                                projectName: projectName,
                                devsPath: devsPath,
                                nasBaseDir: nasBaseDir,
                                baseDir: baseDir,
                                access: access,
                            }];
                }
            });
        });
    };
    /**
     * setup
     * @param inputs
     * @returns
     */
    FcTunnelInvokeComponent.prototype.setup = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, serviceConfig, functionConfig, triggerConfigList, customDomainConfigList, region, devsPath, nasBaseDir, baseDir, creds, isHelp, access, appName, curPath, parsedArgs, argsData, _b, debugPort, debugIde, debuggerPath, debugArgs, tunnelService, session, httpTrigger, tmpDir, localInvoke;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, this.handlerInputs(inputs)];
                    case 1:
                        _a = _c.sent(), serviceConfig = _a.serviceConfig, functionConfig = _a.functionConfig, triggerConfigList = _a.triggerConfigList, customDomainConfigList = _a.customDomainConfigList, region = _a.region, devsPath = _a.devsPath, nasBaseDir = _a.nasBaseDir, baseDir = _a.baseDir, creds = _a.creds, isHelp = _a.isHelp, access = _a.access, appName = _a.appName, curPath = _a.curPath;
                        if (isHelp) {
                            // TODO: help info
                            return [2 /*return*/];
                        }
                        parsedArgs = core.commandParse(inputs, {
                            boolean: ['debug'],
                            alias: {
                                help: 'h',
                                'debug-port': 'd',
                            },
                        });
                        argsData = (parsedArgs === null || parsedArgs === void 0 ? void 0 : parsedArgs.data) || {};
                        _b = debug_1.getDebugOptions(argsData), debugPort = _b.debugPort, debugIde = _b.debugIde, debuggerPath = _b.debuggerPath, debugArgs = _b.debugArgs;
                        if (debugIde && !FcTunnelInvokeComponent.supportedDebugIde.includes(lodash_1.default.toLower(debugIde))) {
                            logger_1.default.error("Unsupported ide: " + debugIde + " for debugging.Only " + FcTunnelInvokeComponent.supportedDebugIde + " are supported");
                            return [2 /*return*/];
                        }
                        tunnelService = new tunnel_service_1.default(creds, serviceConfig, functionConfig, region, access, appName, curPath, triggerConfigList, customDomainConfigList, debugPort, debugIde);
                        return [4 /*yield*/, tunnelService.setup()];
                    case 2:
                        _c.sent();
                        session = tunnelService.getSession();
                        httpTrigger = definition_1.getHttpTrigger(triggerConfigList);
                        return [4 /*yield*/, path_2.ensureTmpDir(argsData['tmp-dir'], devsPath, serviceConfig === null || serviceConfig === void 0 ? void 0 : serviceConfig.name, functionConfig === null || functionConfig === void 0 ? void 0 : functionConfig.name)];
                    case 3:
                        tmpDir = _c.sent();
                        localInvoke = new local_invoke_1.default(tunnelService, session === null || session === void 0 ? void 0 : session.sessionId, creds, region, baseDir, serviceConfig, functionConfig, httpTrigger, debugPort, debugIde, tmpDir, debuggerPath, debugArgs, nasBaseDir);
                        return [4 /*yield*/, localInvoke.setup()];
                    case 4:
                        _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * invoke
     * @param inputs
     * @returns
     */
    FcTunnelInvokeComponent.prototype.invoke = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, serviceConfig, functionConfig, region, creds, isHelp, access, appName, curPath, args, tunnelService;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.handlerInputs(inputs)];
                    case 1:
                        _a = _b.sent(), serviceConfig = _a.serviceConfig, functionConfig = _a.functionConfig, region = _a.region, creds = _a.creds, isHelp = _a.isHelp, access = _a.access, appName = _a.appName, curPath = _a.curPath, args = _a.args;
                        if (isHelp) {
                            // TODO: help info
                            return [2 /*return*/];
                        }
                        tunnelService = new tunnel_service_1.default(creds, serviceConfig, functionConfig, region, access, appName, curPath);
                        return [4 /*yield*/, tunnelService.invokeHelperFunction(args)];
                    case 2:
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * clean
     * @param inputs
     * @returns
     */
    FcTunnelInvokeComponent.prototype.clean = function (inputs) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, serviceConfig, functionConfig, region, baseDir, creds, isHelp, access, appName, curPath, tunnelService, localInvoke;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.handlerInputs(inputs)];
                    case 1:
                        _a = _b.sent(), serviceConfig = _a.serviceConfig, functionConfig = _a.functionConfig, region = _a.region, baseDir = _a.baseDir, creds = _a.creds, isHelp = _a.isHelp, access = _a.access, appName = _a.appName, curPath = _a.curPath;
                        if (isHelp) {
                            // TODO: help info
                            return [2 /*return*/];
                        }
                        tunnelService = new tunnel_service_1.default(creds, serviceConfig, functionConfig, region, access, appName, curPath);
                        return [4 /*yield*/, tunnelService.clean()];
                    case 2:
                        _b.sent();
                        localInvoke = new local_invoke_1.default(tunnelService, null, creds, region, baseDir, serviceConfig, functionConfig);
                        return [4 /*yield*/, localInvoke.clean()];
                    case 3:
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    FcTunnelInvokeComponent.supportedDebugIde = ['vscode', 'intellij'];
    return FcTunnelInvokeComponent;
}(base_1.default));
exports.default = FcTunnelInvokeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLHVEQUEwQztBQUMxQywyREFBcUM7QUFFckMsa0RBQXVCO0FBQ3ZCLDBEQUE4QztBQUM5QyxzRkFBK0Q7QUFNL0QsOENBQXdCO0FBQ3hCLG1DQUEwRTtBQUMxRSx3RUFBaUQ7QUFDakQsaUZBQTBEO0FBQzFELDJDQUFxRDtBQUNyRCwrQ0FBa0Q7QUFDbEQsa0RBQTJEO0FBQzNELHlDQUFnRDtBQUdoRDtJQUFxRCwyQ0FBYTtJQUNoRSxpQ0FBWSxLQUFVO2VBQ3BCLGtCQUFNLEtBQUssQ0FBQztJQUNkLENBQUM7SUFJSyx3Q0FBTSxHQUFaLFVBQWEsYUFBcUIsRUFBRSxPQUFlLEVBQUUsU0FBa0IsRUFBRSxNQUFlOzs7Ozs7d0JBQ2xGLEdBQUcsR0FBVyxTQUFTLENBQUM7NkJBQ3hCLGdCQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFwQix3QkFBb0I7d0JBQ1kscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTVELFdBQVcsR0FBaUIsU0FBZ0M7d0JBQ2xFLEdBQUcsR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDOzs7d0JBRTlCLElBQUk7NEJBQ0YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEVBQUU7Z0NBQ2xDLE9BQU8sU0FBQTtnQ0FDUCxHQUFHLEtBQUE7NkJBQ0osQ0FBQyxDQUFDO3lCQUNKO3dCQUFDLE9BQU8sQ0FBQyxFQUFFOzRCQUNWLGdCQUFNLENBQUMsT0FBTyxDQUFDLDBCQUFlLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxxQkFBbUIsYUFBYSxrQkFBYSxPQUFTLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7eUJBQzdJOzs7OztLQUNGO0lBRUssK0NBQWEsR0FBbkIsVUFBb0IsTUFBa0I7Ozs7Ozs0QkFDcEMscUJBQU0sMEJBQWUsQ0FBQyxVQUFVLEVBQUUsRUFBQTs7d0JBQWxDLFNBQWtDLENBQUM7d0JBQzdCLE9BQU8sR0FBRyxNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsT0FBTyxDQUFDO3dCQUMxQixNQUFNLEdBQVcsT0FBTyxhQUFQLE9BQU8sdUJBQVAsT0FBTyxDQUFFLE1BQU0sQ0FBQzt3QkFDWCxxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBdEQsS0FBSyxHQUFpQixTQUFnQzt3QkFDNUQsOEJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzNCLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLE9BQU8sRUFBRSxLQUFLLGFBQUwsS0FBSyx1QkFBTCxLQUFLLENBQUUsU0FBUyxRQUFFLE1BQU0sYUFBTixNQUFNLHVCQUFOLE1BQU0sQ0FBRSxPQUFPLDBDQUFFLE1BQU0sQ0FBQyxFQUFBOzt3QkFBakcsU0FBaUcsQ0FBQzt3QkFFNUYsVUFBVSxHQUFnQixNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsS0FBSyxDQUFDO3dCQUV4QyxPQUFPLEdBQVcsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLE9BQU8sQ0FBQzt3QkFFbEMsSUFBSSxHQUFXLE1BQU0sYUFBTixNQUFNLHVCQUFOLE1BQU0sQ0FBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxDQUFDO3dCQUMxRCxPQUFPLEdBQVEsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLElBQUksQ0FBQzt3QkFFNUIsUUFBUSxHQUFXLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxVQUFVLENBQUM7d0JBQ3ZDLFVBQVUsR0FBVyx1QkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDaEQsT0FBTyxHQUFXLGNBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBRXpDLFdBQVcsR0FBVyxPQUFPLGFBQVAsT0FBTyx1QkFBUCxPQUFPLENBQUUsV0FBVyxDQUFDO3dCQUN6QyxNQUFNLEdBQUssVUFBVSxPQUFmLENBQWdCO3dCQUN4QixVQUFVLEdBQTJCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFOzRCQUNuRSxPQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUM7NEJBQ2pCLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUU7eUJBQ3JCLENBQUMsQ0FBQzt3QkFDRyxRQUFRLEdBQVEsQ0FBQSxVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsSUFBSSxLQUFJLEVBQUUsQ0FBQzt3QkFDN0MsSUFBSSxRQUFRLGFBQVIsUUFBUSx1QkFBUixRQUFRLENBQUUsSUFBSSxFQUFFOzRCQUNsQixzQkFBTztvQ0FDTCxNQUFNLFFBQUE7b0NBQ04sS0FBSyxPQUFBO29DQUNMLElBQUksZ0JBQUE7b0NBQ0osSUFBSSxNQUFBO29DQUNKLE1BQU0sUUFBQTtvQ0FDTixNQUFNLEVBQUUsSUFBSTtpQ0FDYixFQUFDO3lCQUNIO3dCQUVLLGFBQWEsR0FBa0IsVUFBVSxhQUFWLFVBQVUsdUJBQVYsVUFBVSxDQUFFLE9BQU8sQ0FBQzt3QkFDbkQsaUJBQWlCLEdBQW9CLFVBQVUsYUFBVixVQUFVLHVCQUFWLFVBQVUsQ0FBRSxRQUFRLENBQUM7d0JBQzFELHNCQUFzQixHQUF5QixVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsYUFBYSxDQUFDO3dCQUN6RSxjQUFjLEdBQW1CLGlDQUEwQixDQUFDLE9BQU8sRUFBRSxVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsUUFBUSxFQUFFLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFFckgsc0JBQU87Z0NBQ0wsYUFBYSxlQUFBO2dDQUNiLGNBQWMsZ0JBQUE7Z0NBQ2QsaUJBQWlCLG1CQUFBO2dDQUNqQixzQkFBc0Isd0JBQUE7Z0NBQ3RCLE1BQU0sUUFBQTtnQ0FDTixLQUFLLE9BQUE7Z0NBQ0wsT0FBTyxTQUFBO2dDQUNQLElBQUksTUFBQTtnQ0FDSixPQUFPLFNBQUE7Z0NBQ1AsV0FBVyxhQUFBO2dDQUNYLFFBQVEsVUFBQTtnQ0FDUixVQUFVLFlBQUE7Z0NBQ1YsT0FBTyxTQUFBO2dDQUNQLE1BQU0sUUFBQTs2QkFDUCxFQUFDOzs7O0tBQ0g7SUFFRDs7OztPQUlHO0lBQ1UsdUNBQUssR0FBbEIsVUFBbUIsTUFBa0I7Ozs7OzRCQWUvQixxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFkOUIsS0FjRixTQUFnQyxFQWJsQyxhQUFhLG1CQUFBLEVBQ2IsY0FBYyxvQkFBQSxFQUNkLGlCQUFpQix1QkFBQSxFQUNqQixzQkFBc0IsNEJBQUEsRUFDdEIsTUFBTSxZQUFBLEVBQ04sUUFBUSxjQUFBLEVBQ1IsVUFBVSxnQkFBQSxFQUNWLE9BQU8sYUFBQSxFQUNQLEtBQUssV0FBQSxFQUNMLE1BQU0sWUFBQSxFQUNOLE1BQU0sWUFBQSxFQUNOLE9BQU8sYUFBQSxFQUNQLE9BQU8sYUFBQTt3QkFHVCxJQUFJLE1BQU0sRUFBRTs0QkFDVixrQkFBa0I7NEJBQ2xCLHNCQUFPO3lCQUNSO3dCQUVLLFVBQVUsR0FBMkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7NEJBQ25FLE9BQU8sRUFBRSxDQUFDLE9BQU8sQ0FBQzs0QkFDbEIsS0FBSyxFQUFFO2dDQUNMLElBQUksRUFBRSxHQUFHO2dDQUNULFlBQVksRUFBRSxHQUFHOzZCQUNsQjt5QkFDRixDQUFDLENBQUM7d0JBQ0csUUFBUSxHQUFRLENBQUEsVUFBVSxhQUFWLFVBQVUsdUJBQVYsVUFBVSxDQUFFLElBQUksS0FBSSxFQUFFLENBQUM7d0JBQ3ZDLEtBQW1ELHVCQUFlLENBQUMsUUFBUSxDQUFDLEVBQTFFLFNBQVMsZUFBQSxFQUFFLFFBQVEsY0FBQSxFQUFFLFlBQVksa0JBQUEsRUFBRSxTQUFTLGVBQUEsQ0FBK0I7d0JBQ25GLElBQUksUUFBUSxJQUFJLENBQUMsdUJBQXVCLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLGdCQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUU7NEJBQ3hGLGdCQUFNLENBQUMsS0FBSyxDQUFDLHNCQUFvQixRQUFRLDRCQUF1Qix1QkFBdUIsQ0FBQyxpQkFBaUIsbUJBQWdCLENBQUMsQ0FBQzs0QkFDM0gsc0JBQU87eUJBQ1I7d0JBQ0ssYUFBYSxHQUFrQixJQUFJLHdCQUFhLENBQ3BELEtBQUssRUFDTCxhQUFhLEVBQ2IsY0FBYyxFQUNkLE1BQU0sRUFDTixNQUFNLEVBQ04sT0FBTyxFQUNQLE9BQU8sRUFDUCxpQkFBaUIsRUFDakIsc0JBQXNCLEVBQ3RCLFNBQVMsRUFDVCxRQUFRLENBQ1QsQ0FBQzt3QkFDRixxQkFBTSxhQUFhLENBQUMsS0FBSyxFQUFFLEVBQUE7O3dCQUEzQixTQUEyQixDQUFDO3dCQUN0QixPQUFPLEdBQVksYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO3dCQUM5QyxXQUFXLEdBQWtCLDJCQUFjLENBQUMsaUJBQWlCLENBQUMsQ0FBQzt3QkFFdEQscUJBQU0sbUJBQVksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEVBQUUsUUFBUSxFQUFFLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxJQUFJLEVBQUUsY0FBYyxhQUFkLGNBQWMsdUJBQWQsY0FBYyxDQUFFLElBQUksQ0FBQyxFQUFBOzt3QkFBckcsTUFBTSxHQUFHLFNBQTRGO3dCQUVyRyxXQUFXLEdBQWdCLElBQUksc0JBQVcsQ0FDOUMsYUFBYSxFQUNiLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxTQUFTLEVBQ2xCLEtBQUssRUFDTCxNQUFNLEVBQ04sT0FBTyxFQUNQLGFBQWEsRUFDYixjQUFjLEVBQ2QsV0FBVyxFQUNYLFNBQVMsRUFDVCxRQUFRLEVBQ1IsTUFBTSxFQUNOLFlBQVksRUFDWixTQUFTLEVBQ1QsVUFBVSxDQUNYLENBQUM7d0JBQ0YscUJBQU0sV0FBVyxDQUFDLEtBQUssRUFBRSxFQUFBOzt3QkFBekIsU0FBeUIsQ0FBQzs7Ozs7S0FDM0I7SUFFRDs7OztPQUlHO0lBQ1Usd0NBQU0sR0FBbkIsVUFBb0IsTUFBa0I7Ozs7OzRCQUM2RCxxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBM0gsS0FBMkYsU0FBZ0MsRUFBekgsYUFBYSxtQkFBQSxFQUFFLGNBQWMsb0JBQUEsRUFBRSxNQUFNLFlBQUEsRUFBRSxLQUFLLFdBQUEsRUFBRSxNQUFNLFlBQUEsRUFBRSxNQUFNLFlBQUEsRUFBRSxPQUFPLGFBQUEsRUFBRSxPQUFPLGFBQUEsRUFBRSxJQUFJLFVBQUE7d0JBQzVGLElBQUksTUFBTSxFQUFFOzRCQUNWLGtCQUFrQjs0QkFDbEIsc0JBQU87eUJBQ1I7d0JBR0ssYUFBYSxHQUFrQixJQUFJLHdCQUFhLENBQUMsS0FBSyxFQUFFLGFBQWEsRUFBRSxjQUFjLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQy9ILHFCQUFNLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsRUFBQTs7d0JBQTlDLFNBQThDLENBQUM7Ozs7O0tBQ2hEO0lBRUQ7Ozs7T0FJRztJQUNVLHVDQUFLLEdBQWxCLFVBQW1CLE1BQWtCOzs7Ozs0QkFDaUUscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTlILEtBQThGLFNBQWdDLEVBQTVILGFBQWEsbUJBQUEsRUFBRSxjQUFjLG9CQUFBLEVBQUUsTUFBTSxZQUFBLEVBQUUsT0FBTyxhQUFBLEVBQUUsS0FBSyxXQUFBLEVBQUUsTUFBTSxZQUFBLEVBQUUsTUFBTSxZQUFBLEVBQUUsT0FBTyxhQUFBLEVBQUUsT0FBTyxhQUFBO3dCQUMvRixJQUFJLE1BQU0sRUFBRTs0QkFDVixrQkFBa0I7NEJBQ2xCLHNCQUFPO3lCQUNSO3dCQUVLLGFBQWEsR0FBa0IsSUFBSSx3QkFBYSxDQUFDLEtBQUssRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLE9BQU8sQ0FBQyxDQUFDO3dCQUMvSCxxQkFBTSxhQUFhLENBQUMsS0FBSyxFQUFFLEVBQUE7O3dCQUEzQixTQUEyQixDQUFDO3dCQUV0QixXQUFXLEdBQWdCLElBQUksc0JBQVcsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBRSxjQUFjLENBQUMsQ0FBQzt3QkFDN0gscUJBQU0sV0FBVyxDQUFDLEtBQUssRUFBRSxFQUFBOzt3QkFBekIsU0FBeUIsQ0FBQzs7Ozs7S0FDM0I7SUE5TGUseUNBQWlCLEdBQWEsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7SUErTHZFLDhCQUFDO0NBQUEsQUFwTUQsQ0FBcUQsY0FBYSxHQW9NakU7a0JBcE1vQix1QkFBdUIifQ==