"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolveRuntimeToDockerImage = exports.resolveDockerEnv = exports.resolveDockerRegistry = exports.resolveMockScript = exports.generateLocalStartOpts = exports.generateContainerName = exports.generateContainerNameFilter = exports.transformPathForVirtualBox = exports.transformMountsForToolbox = exports.generateInstallOpts = exports.resolveDockerUser = exports.generateSboxOpts = exports.resolveImageNameForPull = exports.DOCKER_REGISTRIES = void 0;
var httpx = __importStar(require("httpx"));
var _ = __importStar(require("lodash"));
var env_1 = require("../local-invoke/env");
var logger_1 = __importDefault(require("../../common/logger"));
var definition_1 = require("../definition");
var nested_object_assign_1 = __importDefault(require("nested-object-assign"));
var runtime_1 = require("../utils/runtime");
var utils_1 = require("../utils/utils");
var REGISTRY_DEFAULT = 'registry.cn-beijing.aliyuncs.com';
var DEFAULT_REGISTRY = REGISTRY_DEFAULT || 'registry.hub.docker.com';
exports.DOCKER_REGISTRIES = [
    "registry.cn-beijing.aliyuncs.com",
    "registry.hub.docker.com"
];
var IMAGE_VERSION = process.env.FC_DOCKER_VERSION || '1.9.18';
var DOCKER_REGISTRY_CACHE;
var NAS_UID = 10003;
var NAS_GID = 10003;
var runtimeImageMap = {
    'nodejs6': 'nodejs6',
    'nodejs8': 'nodejs8',
    'nodejs10': 'nodejs10',
    'nodejs12': 'nodejs12',
    'python2.7': 'python2.7',
    'python3': 'python3.6',
    'java8': 'java8',
    'java11': 'java11',
    'php7.2': 'php7.2',
    'dotnetcore2.1': 'dotnetcore2.1',
    'custom': 'custom'
};
function resolveImageNameForPull(imageName) {
    return __awaiter(this, void 0, void 0, function () {
        var dockerImageRegistry;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, resolveDockerRegistry()];
                case 1:
                    dockerImageRegistry = _a.sent();
                    if (dockerImageRegistry) {
                        imageName = dockerImageRegistry + "/" + imageName;
                    }
                    return [2 /*return*/, imageName];
            }
        });
    });
}
exports.resolveImageNameForPull = resolveImageNameForPull;
function generateSboxOpts(_a) {
    var imageName = _a.imageName, hostname = _a.hostname, mounts = _a.mounts, envs = _a.envs, _b = _a.cmd, cmd = _b === void 0 ? [] : _b, isTty = _a.isTty, isInteractive = _a.isInteractive;
    return {
        Image: imageName,
        Hostname: hostname,
        AttachStdin: isInteractive,
        AttachStdout: true,
        AttachStderr: true,
        // @ts-ignore
        User: resolveDockerUser({ stage: 'sbox' }),
        Tty: isTty,
        OpenStdin: isInteractive,
        StdinOnce: true,
        Env: resolveDockerEnv(envs),
        Cmd: cmd.length ? cmd : ['/bin/bash'],
        HostConfig: {
            AutoRemove: true,
            Mounts: mounts
        }
    };
}
exports.generateSboxOpts = generateSboxOpts;
// Not Run stage:
//  for linux platform, it will always use process.uid and process.gid
//  for mac and windows platform, it will always use 0
// Run stage:
//  for linux platform, it will always use process.uid and process.gid
//  for mac and windows platform, it will use 10003 if no nasConfig, otherwise it will use nasConfig userId
function resolveDockerUser(_a) {
    var nasConfig = _a.nasConfig, _b = _a.stage, stage = _b === void 0 ? 'run' : _b;
    var _c = definition_1.getUserIdAndGroupId(nasConfig), userId = _c.userId, groupId = _c.groupId;
    if (process.platform === 'linux') {
        logger_1.default.debug('For linux platform, Fc will use host userId and groupId to build or run your functions');
        userId = process.getuid();
        groupId = process.getgid();
    }
    else {
        if (stage === 'run') {
            if (userId === -1 || userId === undefined) {
                userId = NAS_UID;
            }
            if (groupId === -1 || groupId === undefined) {
                groupId = NAS_GID;
            }
        }
        else {
            userId = 0;
            groupId = 0;
        }
    }
    return userId + ":" + groupId;
}
exports.resolveDockerUser = resolveDockerUser;
function generateInstallOpts(imageName, mounts, envs) {
    return {
        Image: imageName,
        Tty: true,
        Env: resolveDockerEnv(envs),
        Cmd: ['/bin/bash'],
        HostConfig: {
            AutoRemove: true,
            Mounts: mounts
        }
    };
}
exports.generateInstallOpts = generateInstallOpts;
function transformMountsForToolbox(mounts) {
    console.warn("We detected that you are using docker toolbox. For a better experience, please upgrade 'docker for windows'.\nYou can refer to Chinese doc https://github.com/alibaba/funcraft/blob/master/docs/usage/installation-zh.md#windows-%E5%AE%89%E8%A3%85-docker or English doc https://github.com/alibaba/funcraft/blob/master/docs/usage/installation.md.\n");
    if (Array.isArray(mounts)) {
        return mounts.map(function (m) {
            return transformSourcePathOfMount(m);
        });
    }
    return transformSourcePathOfMount(mounts);
}
exports.transformMountsForToolbox = transformMountsForToolbox;
function transformSourcePathOfMount(mountsObj) {
    if (!_.isEmpty(mountsObj)) {
        var replaceMounts = Object.assign({}, mountsObj);
        replaceMounts.Source = transformPathForVirtualBox(mountsObj.Source);
        return replaceMounts;
    }
    return {};
}
function transformPathForVirtualBox(source) {
    // C:\\Users\\image_crawler\\code -> /c/Users/image_crawler/code
    var sourcePath = source.split(':').join('');
    var lowerFirstAndReplace = _.lowerFirst(sourcePath.split('\\').join('/'));
    return '/' + lowerFirstAndReplace;
}
exports.transformPathForVirtualBox = transformPathForVirtualBox;
function generateContainerNameFilter(containerName, inited) {
    if (inited) {
        return "{\"name\": [\"" + containerName + "-inited\"]}";
    }
    return "{\"name\": [\"" + containerName + "\"]}";
}
exports.generateContainerNameFilter = generateContainerNameFilter;
function generateContainerName(serviceName, functionName, debugPort) {
    return ("fc-local-" + serviceName + "-" + functionName).replace(/ /g, '')
        + (debugPort ? '-debug' : '-run');
}
exports.generateContainerName = generateContainerName;
function generateLocalStartOpts(proxyContainerName, runtime, name, mounts, cmd, envs, _a) {
    var debugPort = _a.debugPort, dockerUser = _a.dockerUser, _b = _a.debugIde, debugIde = _b === void 0 ? null : _b, imageName = _a.imageName;
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    if (runtime_1.isCustomContainerRuntime(runtime)) {
                        return [2 /*return*/, genCustomContainerLocalStartOpts(proxyContainerName, name, mounts, cmd, envs, imageName)];
                    }
                    return [4 /*yield*/, genNonCustomContainerLocalStartOpts(proxyContainerName, runtime, name, mounts, cmd, debugPort, envs, dockerUser, debugIde)];
                case 1: return [2 /*return*/, _c.sent()];
            }
        });
    });
}
exports.generateLocalStartOpts = generateLocalStartOpts;
function genNonCustomContainerLocalStartOpts(proxyContainerName, runtime, name, mounts, cmd, debugPort, envs, dockerUser, debugIde) {
    return __awaiter(this, void 0, void 0, function () {
        var hostOpts, imageName, opts, encryptedOpts;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    hostOpts = {
                        HostConfig: {
                            AutoRemove: true,
                            Mounts: mounts
                        }
                    };
                    if (!_.isEmpty(proxyContainerName)) {
                        hostOpts.HostConfig.NetworkMode = "container:" + proxyContainerName;
                    }
                    return [4 /*yield*/, resolveRuntimeToDockerImage(runtime)];
                case 1:
                    imageName = _a.sent();
                    supportCustomBootstrapFile(runtime, envs);
                    opts = nested_object_assign_1.default({
                        Env: resolveDockerEnv(envs),
                        Image: imageName,
                        name: name,
                        Cmd: cmd,
                        User: dockerUser,
                        Entrypoint: [resolveMockScript(runtime)]
                    }, hostOpts);
                    encryptedOpts = encryptDockerOpts(opts);
                    logger_1.default.debug("docker options: " + JSON.stringify(encryptedOpts, null, '  '));
                    return [2 /*return*/, opts];
            }
        });
    });
}
// /**
//  * 支持通过 BOOTSTRAP_FILE 环境变量改变 bootstrap 文件名。
// **/
function supportCustomBootstrapFile(runtime, envs) {
    if (runtime === 'custom') {
        if (envs['BOOTSTRAP_FILE']) {
            envs['AGENT_SCRIPT'] = envs['BOOTSTRAP_FILE'];
        }
    }
}
function resolveMockScript(runtime) {
    return "/var/fc/runtime/" + runtime + "/mock";
}
exports.resolveMockScript = resolveMockScript;
function encryptDockerOpts(dockerOpts) {
    var encryptedOpts = _.cloneDeep(dockerOpts);
    if (encryptedOpts === null || encryptedOpts === void 0 ? void 0 : encryptedOpts.Env) {
        var encryptedEnv = encryptedOpts.Env.map(function (e) {
            if (e.startsWith("FC_ACCESS_KEY_ID") || e.startsWith("FC_ACCESS_KEY_SECRET") || e.startsWith("FC_ACCOUNT_ID")) {
                var keyValueList = e.split('=');
                var encrptedVal = utils_1.mark(keyValueList[1]);
                return keyValueList[0] + "=" + encrptedVal;
            }
            else {
                return e;
            }
        });
        encryptedOpts.Env = encryptedEnv;
    }
    return encryptedOpts;
}
function genCustomContainerLocalStartOpts(proxyContainerName, name, mounts, cmd, envs, imageName) {
    var hostOpts = {
        HostConfig: {
            AutoRemove: true,
            Mounts: mounts,
        }
    };
    if (!_.isEmpty(proxyContainerName)) {
        hostOpts.HostConfig.NetworkMode = "container:" + proxyContainerName;
    }
    var opts = {
        Env: resolveDockerEnv(envs, true),
        Image: imageName,
        name: name
    };
    if (cmd !== []) {
        opts.Cmd = cmd;
    }
    var ioOpts = {
        OpenStdin: true,
        Tty: false,
        StdinOnce: true,
        AttachStdin: true,
        AttachStdout: true,
        AttachStderr: true
    };
    var dockerOpts = nested_object_assign_1.default(opts, hostOpts, ioOpts);
    var encryptedOpts = encryptDockerOpts(dockerOpts);
    logger_1.default.debug("docker options for custom container: " + JSON.stringify(encryptedOpts, null, '  '));
    return dockerOpts;
}
function resolveDockerRegistry() {
    return __awaiter(this, void 0, void 0, function () {
        var promises, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    // await doImageRegisterEventTag('start');
                    if (DOCKER_REGISTRY_CACHE) {
                        return [2 /*return*/, DOCKER_REGISTRY_CACHE];
                    }
                    promises = exports.DOCKER_REGISTRIES.map(function (r) { return httpx.request("https://" + r + "/v2/aliyunfc/runtime-nodejs8/tags/list", { timeout: 3000 }).then(function () { return r; }); });
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, Promise.race(promises)];
                case 2:
                    DOCKER_REGISTRY_CACHE = _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    error_1 = _a.sent();
                    DOCKER_REGISTRY_CACHE = DEFAULT_REGISTRY;
                    return [3 /*break*/, 4];
                case 4: 
                // await doImageRegisterEventTag(DOCKER_REGISTRY_CACHE);
                return [2 /*return*/, DOCKER_REGISTRY_CACHE];
            }
        });
    });
}
exports.resolveDockerRegistry = resolveDockerRegistry;
function resolveDockerEnv(envs, isCustomContainer) {
    if (envs === void 0) { envs = {}; }
    if (isCustomContainer === void 0) { isCustomContainer = false; }
    if (isCustomContainer) {
        return _.map(envs || {}, function (v, k) { return k + "=" + v; });
    }
    return _.map(env_1.addEnv(envs || {}), function (v, k) { return k + "=" + v; });
}
exports.resolveDockerEnv = resolveDockerEnv;
function resolveRuntimeToDockerImage(runtime, isBuild) {
    return __awaiter(this, void 0, void 0, function () {
        var name_1, imageName;
        return __generator(this, function (_a) {
            if (runtimeImageMap[runtime]) {
                name_1 = runtimeImageMap[runtime];
                if (isBuild) {
                    imageName = "aliyunfc/runtime-" + name_1 + ":build-" + IMAGE_VERSION;
                }
                else {
                    imageName = "aliyunfc/runtime-" + name_1 + ":" + IMAGE_VERSION;
                }
                logger_1.default.debug('imageName: ' + imageName);
                return [2 /*return*/, imageName];
            }
            throw new Error("invalid runtime name " + runtime);
        });
    });
}
exports.resolveRuntimeToDockerImage = resolveRuntimeToDockerImage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9ja2VyLW9wdHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2RvY2tlci9kb2NrZXItb3B0cy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsMkNBQStCO0FBQy9CLHdDQUE0QjtBQUM1QiwyQ0FBNkM7QUFDN0MsK0RBQXlDO0FBQ3pDLDRDQUFvRDtBQUNwRCw4RUFBc0Q7QUFDdEQsNENBQTREO0FBQzVELHdDQUFzQztBQUV0QyxJQUFNLGdCQUFnQixHQUFXLGtDQUFrQyxDQUFDO0FBR3BFLElBQU0sZ0JBQWdCLEdBQVcsZ0JBQWdCLElBQUkseUJBQXlCLENBQUM7QUFDbEUsUUFBQSxpQkFBaUIsR0FBYTtJQUN6QyxrQ0FBa0M7SUFDbEMseUJBQXlCO0NBQzFCLENBQUM7QUFDRixJQUFNLGFBQWEsR0FBVyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixJQUFJLFFBQVEsQ0FBQztBQUd4RSxJQUFJLHFCQUFxQixDQUFDO0FBRzFCLElBQU0sT0FBTyxHQUFXLEtBQUssQ0FBQztBQUM5QixJQUFNLE9BQU8sR0FBVyxLQUFLLENBQUM7QUFFOUIsSUFBTSxlQUFlLEdBQTRCO0lBQy9DLFNBQVMsRUFBRSxTQUFTO0lBQ3BCLFNBQVMsRUFBRSxTQUFTO0lBQ3BCLFVBQVUsRUFBRSxVQUFVO0lBQ3RCLFVBQVUsRUFBRSxVQUFVO0lBQ3RCLFdBQVcsRUFBRSxXQUFXO0lBQ3hCLFNBQVMsRUFBRSxXQUFXO0lBQ3RCLE9BQU8sRUFBRSxPQUFPO0lBQ2hCLFFBQVEsRUFBRSxRQUFRO0lBQ2xCLFFBQVEsRUFBRSxRQUFRO0lBQ2xCLGVBQWUsRUFBRSxlQUFlO0lBQ2hDLFFBQVEsRUFBRSxRQUFRO0NBQ25CLENBQUM7QUFHRixTQUFzQix1QkFBdUIsQ0FBQyxTQUFpQjs7Ozs7d0JBRWpDLHFCQUFNLHFCQUFxQixFQUFFLEVBQUE7O29CQUFuRCxtQkFBbUIsR0FBRyxTQUE2QjtvQkFFekQsSUFBSSxtQkFBbUIsRUFBRTt3QkFDdkIsU0FBUyxHQUFNLG1CQUFtQixTQUFJLFNBQVcsQ0FBQztxQkFDbkQ7b0JBQ0Qsc0JBQU8sU0FBUyxFQUFDOzs7O0NBQ2xCO0FBUkQsMERBUUM7QUFFRCxTQUFnQixnQkFBZ0IsQ0FBQyxFQUFtRTtRQUFsRSxTQUFTLGVBQUEsRUFBRSxRQUFRLGNBQUEsRUFBRSxNQUFNLFlBQUEsRUFBRSxJQUFJLFVBQUEsRUFBRSxXQUFRLEVBQVIsR0FBRyxtQkFBRyxFQUFFLEtBQUEsRUFBRSxLQUFLLFdBQUEsRUFBRSxhQUFhLG1CQUFBO0lBQ2pHLE9BQU87UUFDTCxLQUFLLEVBQUUsU0FBUztRQUNoQixRQUFRLEVBQUUsUUFBUTtRQUNsQixXQUFXLEVBQUUsYUFBYTtRQUMxQixZQUFZLEVBQUUsSUFBSTtRQUNsQixZQUFZLEVBQUUsSUFBSTtRQUNsQixhQUFhO1FBQ2IsSUFBSSxFQUFFLGlCQUFpQixDQUFDLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxDQUFDO1FBQzFDLEdBQUcsRUFBRSxLQUFLO1FBQ1YsU0FBUyxFQUFFLGFBQWE7UUFDeEIsU0FBUyxFQUFFLElBQUk7UUFDZixHQUFHLEVBQUUsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO1FBQzNCLEdBQUcsRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1FBQ3JDLFVBQVUsRUFBRTtZQUNWLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLE1BQU0sRUFBRSxNQUFNO1NBQ2Y7S0FDRixDQUFDO0FBQ0osQ0FBQztBQW5CRCw0Q0FtQkM7QUFFRCxpQkFBaUI7QUFDakIsc0VBQXNFO0FBQ3RFLHNEQUFzRDtBQUN0RCxhQUFhO0FBQ2Isc0VBQXNFO0FBQ3RFLDJHQUEyRztBQUMzRyxTQUFnQixpQkFBaUIsQ0FBQyxFQUEwQjtRQUF6QixTQUFTLGVBQUEsRUFBRSxhQUFhLEVBQWIsS0FBSyxtQkFBRyxLQUFLLEtBQUE7SUFDckQsSUFBQSxLQUFzQixnQ0FBbUIsQ0FBQyxTQUFTLENBQUMsRUFBbEQsTUFBTSxZQUFBLEVBQUUsT0FBTyxhQUFtQyxDQUFDO0lBRXpELElBQUksT0FBTyxDQUFDLFFBQVEsS0FBSyxPQUFPLEVBQUU7UUFDaEMsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsd0ZBQXdGLENBQUMsQ0FBQztRQUN2RyxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzFCLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7S0FDNUI7U0FBTTtRQUNMLElBQUksS0FBSyxLQUFLLEtBQUssRUFBRTtZQUNuQixJQUFJLE1BQU0sS0FBSyxDQUFDLENBQUMsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO2dCQUN6QyxNQUFNLEdBQUcsT0FBTyxDQUFDO2FBQ2xCO1lBQ0QsSUFBSSxPQUFPLEtBQUssQ0FBQyxDQUFDLElBQUksT0FBTyxLQUFLLFNBQVMsRUFBRTtnQkFDM0MsT0FBTyxHQUFHLE9BQU8sQ0FBQzthQUNuQjtTQUNGO2FBQU07WUFDTCxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ1gsT0FBTyxHQUFHLENBQUMsQ0FBQztTQUNiO0tBQ0Y7SUFFRCxPQUFVLE1BQU0sU0FBSSxPQUFTLENBQUM7QUFDaEMsQ0FBQztBQXRCRCw4Q0FzQkM7QUFHRCxTQUFnQixtQkFBbUIsQ0FBQyxTQUFpQixFQUFFLE1BQVcsRUFBRSxJQUFTO0lBQzNFLE9BQU87UUFDTCxLQUFLLEVBQUUsU0FBUztRQUNoQixHQUFHLEVBQUUsSUFBSTtRQUNULEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7UUFDM0IsR0FBRyxFQUFFLENBQUMsV0FBVyxDQUFDO1FBQ2xCLFVBQVUsRUFBRTtZQUNWLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLE1BQU0sRUFBRSxNQUFNO1NBQ2Y7S0FDRixDQUFDO0FBQ0osQ0FBQztBQVhELGtEQVdDO0FBRUQsU0FBZ0IseUJBQXlCLENBQUMsTUFBTTtJQUU5QyxPQUFPLENBQUMsSUFBSSxDQUFDLHlWQUF5VixDQUFDLENBQUM7SUFFeFcsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1FBQ3pCLE9BQU8sTUFBTSxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUM7WUFFakIsT0FBTywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUMsQ0FBQztLQUNKO0lBQ0QsT0FBTywwQkFBMEIsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM1QyxDQUFDO0FBWEQsOERBV0M7QUFFRCxTQUFTLDBCQUEwQixDQUFDLFNBQVM7SUFFM0MsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7UUFFekIsSUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDbkQsYUFBYSxDQUFDLE1BQU0sR0FBRywwQkFBMEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEUsT0FBTyxhQUFhLENBQUM7S0FDdEI7SUFDRCxPQUFPLEVBQUUsQ0FBQztBQUNaLENBQUM7QUFFRCxTQUFnQiwwQkFBMEIsQ0FBQyxNQUFNO0lBQy9DLGdFQUFnRTtJQUNoRSxJQUFNLFVBQVUsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM5QyxJQUFNLG9CQUFvQixHQUFHLENBQUMsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUM1RSxPQUFPLEdBQUcsR0FBRyxvQkFBb0IsQ0FBQztBQUNwQyxDQUFDO0FBTEQsZ0VBS0M7QUFFRCxTQUFnQiwyQkFBMkIsQ0FBQyxhQUFxQixFQUFFLE1BQWdCO0lBQ2pGLElBQUksTUFBTSxFQUFFO1FBQ1YsT0FBTyxtQkFBYyxhQUFhLGdCQUFZLENBQUM7S0FDaEQ7SUFDRCxPQUFPLG1CQUFjLGFBQWEsU0FBSyxDQUFDO0FBQzFDLENBQUM7QUFMRCxrRUFLQztBQUdELFNBQWdCLHFCQUFxQixDQUFDLFdBQW1CLEVBQUUsWUFBb0IsRUFBRSxTQUFrQjtJQUNqRyxPQUFPLENBQUEsY0FBWSxXQUFXLFNBQUksWUFBYyxDQUFBLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUM7VUFDOUQsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7QUFDdEMsQ0FBQztBQUhELHNEQUdDO0FBRUQsU0FBc0Isc0JBQXNCLENBQUMsa0JBQTBCLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFxRDtRQUFuRCxTQUFTLGVBQUEsRUFBRSxVQUFVLGdCQUFBLEVBQUUsZ0JBQWUsRUFBZixRQUFRLG1CQUFHLElBQUksS0FBQSxFQUFFLFNBQVMsZUFBQTs7Ozs7b0JBQzVKLElBQUksa0NBQXdCLENBQUMsT0FBTyxDQUFDLEVBQUU7d0JBQ3JDLHNCQUFPLGdDQUFnQyxDQUFDLGtCQUFrQixFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxTQUFTLENBQUMsRUFBQztxQkFDakc7b0JBQ00scUJBQU0sbUNBQW1DLENBQUMsa0JBQWtCLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxFQUFBO3dCQUF2SSxzQkFBTyxTQUFnSSxFQUFDOzs7O0NBQ3pJO0FBTEQsd0RBS0M7QUFFRCxTQUFlLG1DQUFtQyxDQUFDLGtCQUFrQixFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxRQUFROzs7Ozs7b0JBQ2hJLFFBQVEsR0FBUTt3QkFDcEIsVUFBVSxFQUFFOzRCQUNWLFVBQVUsRUFBRSxJQUFJOzRCQUNoQixNQUFNLEVBQUUsTUFBTTt5QkFDZjtxQkFDRixDQUFDO29CQUNGLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLEVBQUU7d0JBQ2xDLFFBQVEsQ0FBQyxVQUFVLENBQUMsV0FBVyxHQUFHLGVBQWEsa0JBQW9CLENBQUM7cUJBQ3JFO29CQUdpQixxQkFBTSwyQkFBMkIsQ0FBQyxPQUFPLENBQUMsRUFBQTs7b0JBQXRELFNBQVMsR0FBRyxTQUEwQztvQkFFNUQsMEJBQTBCLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUVwQyxJQUFJLEdBQUcsOEJBQWtCLENBQzdCO3dCQUNFLEdBQUcsRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7d0JBQzNCLEtBQUssRUFBRSxTQUFTO3dCQUNoQixJQUFJLE1BQUE7d0JBQ0osR0FBRyxFQUFFLEdBQUc7d0JBQ1IsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLFVBQVUsRUFBRSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUN6QyxFQUNELFFBQVEsQ0FBQyxDQUFDO29CQUNOLGFBQWEsR0FBUSxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFFbkQsZ0JBQU0sQ0FBQyxLQUFLLENBQUMscUJBQW1CLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUcsQ0FBQyxDQUFDO29CQUM3RSxzQkFBTyxJQUFJLEVBQUM7Ozs7Q0FDYjtBQUVELE1BQU07QUFDTiwrQ0FBK0M7QUFDL0MsTUFBTTtBQUNOLFNBQVMsMEJBQTBCLENBQUMsT0FBTyxFQUFFLElBQUk7SUFDL0MsSUFBSSxPQUFPLEtBQUssUUFBUSxFQUFFO1FBQ3hCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7WUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQy9DO0tBQ0Y7QUFDSCxDQUFDO0FBRUQsU0FBZ0IsaUJBQWlCLENBQUMsT0FBZTtJQUMvQyxPQUFPLHFCQUFtQixPQUFPLFVBQU8sQ0FBQztBQUMzQyxDQUFDO0FBRkQsOENBRUM7QUFFRCxTQUFTLGlCQUFpQixDQUFDLFVBQWU7SUFDeEMsSUFBTSxhQUFhLEdBQVEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNuRCxJQUFJLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxHQUFHLEVBQUU7UUFDdEIsSUFBTSxZQUFZLEdBQVEsYUFBYSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFTO1lBQ3hELElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxFQUFFO2dCQUM3RyxJQUFNLFlBQVksR0FBYSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1QyxJQUFNLFdBQVcsR0FBVyxZQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELE9BQVUsWUFBWSxDQUFDLENBQUMsQ0FBQyxTQUFJLFdBQWEsQ0FBQzthQUM1QztpQkFBTTtnQkFDTCxPQUFPLENBQUMsQ0FBQzthQUNWO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxhQUFhLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQztLQUNsQztJQUNELE9BQU8sYUFBYSxDQUFDO0FBQ3ZCLENBQUM7QUFFRCxTQUFTLGdDQUFnQyxDQUFDLGtCQUFrQixFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxTQUFTO0lBQzlGLElBQU0sUUFBUSxHQUFRO1FBQ3BCLFVBQVUsRUFBRTtZQUNWLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLE1BQU0sRUFBRSxNQUFNO1NBQ2Y7S0FDRixDQUFDO0lBQ0YsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsRUFBRTtRQUNsQyxRQUFRLENBQUMsVUFBVSxDQUFDLFdBQVcsR0FBRyxlQUFhLGtCQUFvQixDQUFDO0tBQ3JFO0lBRUQsSUFBTSxJQUFJLEdBQVE7UUFDaEIsR0FBRyxFQUFFLGdCQUFnQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUM7UUFDakMsS0FBSyxFQUFFLFNBQVM7UUFDaEIsSUFBSSxNQUFBO0tBQ0wsQ0FBQztJQUNGLElBQUksR0FBRyxLQUFLLEVBQUUsRUFBRTtRQUNkLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO0tBQ2hCO0lBQ0QsSUFBTSxNQUFNLEdBQUc7UUFDYixTQUFTLEVBQUUsSUFBSTtRQUNmLEdBQUcsRUFBRSxLQUFLO1FBQ1YsU0FBUyxFQUFFLElBQUk7UUFDZixXQUFXLEVBQUUsSUFBSTtRQUNqQixZQUFZLEVBQUUsSUFBSTtRQUNsQixZQUFZLEVBQUUsSUFBSTtLQUNuQixDQUFDO0lBQ0YsSUFBTSxVQUFVLEdBQUcsOEJBQWtCLENBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUM5RCxJQUFNLGFBQWEsR0FBUSxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUV6RCxnQkFBTSxDQUFDLEtBQUssQ0FBQywwQ0FBd0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBRyxDQUFDLENBQUM7SUFDbEcsT0FBTyxVQUFVLENBQUM7QUFDcEIsQ0FBQztBQUVELFNBQXNCLHFCQUFxQjs7Ozs7O29CQUN6QywwQ0FBMEM7b0JBQzFDLElBQUkscUJBQXFCLEVBQUU7d0JBQ3pCLHNCQUFPLHFCQUFxQixFQUFDO3FCQUM5QjtvQkFDSyxRQUFRLEdBQUcseUJBQWlCLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFXLENBQUMsMkNBQXdDLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBTSxPQUFBLENBQUMsRUFBRCxDQUFDLENBQUMsRUFBcEcsQ0FBb0csQ0FBQyxDQUFDOzs7O29CQUV4SCxxQkFBTSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFBOztvQkFBcEQscUJBQXFCLEdBQUcsU0FBNEIsQ0FBQzs7OztvQkFFckQscUJBQXFCLEdBQUcsZ0JBQWdCLENBQUM7OztnQkFFM0Msd0RBQXdEO2dCQUN4RCxzQkFBTyxxQkFBcUIsRUFBQzs7OztDQUM5QjtBQWJELHNEQWFDO0FBR0QsU0FBZ0IsZ0JBQWdCLENBQUMsSUFBUyxFQUFFLGlCQUF5QjtJQUFwQyxxQkFBQSxFQUFBLFNBQVM7SUFBRSxrQ0FBQSxFQUFBLHlCQUF5QjtJQUNuRSxJQUFJLGlCQUFpQixFQUFFO1FBQ3JCLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFHLENBQUMsU0FBSSxDQUFHLEVBQVgsQ0FBVyxDQUFDLENBQUM7S0FDakQ7SUFDRCxPQUFPLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBTSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsRUFBRSxVQUFDLENBQUMsRUFBRSxDQUFDLElBQUssT0FBRyxDQUFDLFNBQUksQ0FBRyxFQUFYLENBQVcsQ0FBQyxDQUFDO0FBQzFELENBQUM7QUFMRCw0Q0FLQztBQUVELFNBQXNCLDJCQUEyQixDQUFDLE9BQWUsRUFBRSxPQUFpQjs7OztZQUNsRixJQUFJLGVBQWUsQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDdEIsU0FBTyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBRXRDLElBQUksT0FBTyxFQUFFO29CQUNYLFNBQVMsR0FBRyxzQkFBb0IsTUFBSSxlQUFVLGFBQWUsQ0FBQztpQkFDL0Q7cUJBQU07b0JBQ0wsU0FBUyxHQUFHLHNCQUFvQixNQUFJLFNBQUksYUFBZSxDQUFDO2lCQUN6RDtnQkFFRCxnQkFBTSxDQUFDLEtBQUssQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDLENBQUM7Z0JBQ3hDLHNCQUFPLFNBQVMsRUFBQzthQUNsQjtZQUNELE1BQU0sSUFBSSxLQUFLLENBQUMsMEJBQXdCLE9BQVMsQ0FBQyxDQUFDOzs7Q0FDcEQ7QUFkRCxrRUFjQyJ9