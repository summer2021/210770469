"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.stopContainer = exports.run = exports.startContainer = exports.showDebugIdeTipsForPycharm = exports.writeDebugIdeConfigForVscode = exports.showDebugIdeTipsForVscode = exports.createAndRunContainer = exports.resolvePasswdMount = exports.resolveDebuggerPathToMount = exports.resolveTmpDirToMount = exports.resolveNasConfigToMounts = exports.detectDockerVersion = exports.isDockerToolBoxAndEnsureDockerVersion = exports.resolveCodeUriToMount = exports.runContainer = exports.pullImage = exports.pullFcImageIfNeed = exports.generateFunctionEnvs = exports.generateDockerEnvs = exports.generateDockerCmd = exports.pullImageIfNeed = exports.generateRamdomContainerName = exports.imageExist = void 0;
var dockerode_1 = __importDefault(require("dockerode"));
var logger_1 = __importDefault(require("../../common/logger"));
var draftlog = __importStar(require("draftlog"));
var docker_support_1 = require("./docker-support");
var error_processor_1 = require("../error-processor");
var lodash_1 = __importDefault(require("lodash"));
var nas = __importStar(require("../local-invoke/nas"));
var path_1 = __importDefault(require("path"));
var devs_1 = require("../devs");
var fs = __importStar(require("fs-extra"));
var passwd_1 = require("../utils/passwd");
var dockerOpts = __importStar(require("./docker-opts"));
var debug_1 = require("../local-invoke/debug");
var ip = __importStar(require("ip"));
var env_1 = require("../local-invoke/env");
var runtime_1 = require("../utils/runtime");
var definition_1 = require("../definition");
var dev_null_1 = __importDefault(require("dev-null"));
var core = __importStar(require("@serverless-devs/core"));
var isWin = process.platform === 'win32';
var docker = new dockerode_1.default();
draftlog.into(console);
var containers = new Set();
var streams = new Set();
// todo: add options for pull latest image
var skipPullImage = true;
function imageExist(imageUrl) {
    return __awaiter(this, void 0, void 0, function () {
        var images;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, docker.listImages({
                        filters: {
                            reference: [imageUrl]
                        }
                    })];
                case 1:
                    images = _a.sent();
                    return [2 /*return*/, images.length > 0];
            }
        });
    });
}
exports.imageExist = imageExist;
function generateRamdomContainerName() {
    return "fc_local_" + new Date().getTime() + "_" + Math.random().toString(36).substr(2, 7);
}
exports.generateRamdomContainerName = generateRamdomContainerName;
function pullImageIfNeed(imageRegistry, imageRepo, imageName, imageTag) {
    return __awaiter(this, void 0, void 0, function () {
        var imageUrl, stream;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    imageUrl = imageRegistry + "/" + imageRepo + "/" + imageName + ":" + imageTag;
                    logger_1.default.debug("Proxy image url: " + imageUrl);
                    return [4 /*yield*/, imageExist(imageUrl)];
                case 1:
                    if (!!(_a.sent())) return [3 /*break*/, 3];
                    logger_1.default.debug("Starting docker pull " + imageUrl);
                    return [4 /*yield*/, docker.pull(imageUrl)];
                case 2:
                    stream = _a.sent();
                    _a.label = 3;
                case 3: return [4 /*yield*/, new Promise(function (resolve, reject) {
                        if (stream) {
                            var onFinished = function (err) {
                                if (err) {
                                    reject(err);
                                }
                                resolve(imageUrl);
                            };
                            var barLines_1 = {};
                            var onProgress = function (event) {
                                var status = event.status;
                                if (event.progress) {
                                    status = event.status + " " + event.progress;
                                }
                                if (event.id) {
                                    var id = event.id;
                                    if (!barLines_1[id]) {
                                        barLines_1[id] = console.draft();
                                    }
                                    barLines_1[id](id + ': ' + status);
                                }
                                else {
                                    if (lodash_1.default.has(event, 'aux.ID')) {
                                        event.stream = event.aux.ID + '\n';
                                    }
                                    // If there is no id, the line should be wrapped manually.
                                    var out = event.status ? event.status + '\n' : event.stream;
                                    process.stdout.write(out);
                                }
                            };
                            docker.modem.followProgress(stream, onFinished, onProgress);
                        }
                        else {
                            resolve(imageUrl);
                        }
                    })];
                case 4: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.pullImageIfNeed = pullImageIfNeed;
function generateDockerCmd(runtime, isLocalStartInit, functionConfig, httpMode, invokeInitializer, event) {
    if (invokeInitializer === void 0) { invokeInitializer = true; }
    if (event === void 0) { event = null; }
    if (runtime_1.isCustomContainerRuntime(runtime)) {
        return genDockerCmdOfCustomContainer(functionConfig);
    }
    else if (isLocalStartInit) {
        return ['--server'];
    }
    return genDockerCmdOfNonCustomContainer(functionConfig, httpMode, invokeInitializer, event);
}
exports.generateDockerCmd = generateDockerCmd;
function genDockerCmdOfNonCustomContainer(functionConfig, httpMode, invokeInitializer, event) {
    if (invokeInitializer === void 0) { invokeInitializer = true; }
    if (event === void 0) { event = null; }
    var cmd = ['-h', functionConfig.handler];
    // 如果提供了 event
    if (event !== null) {
        cmd.push('--event', Buffer.from(event).toString('base64'));
        cmd.push('--event-decode');
    }
    else {
        // always pass event using stdin mode
        cmd.push('--stdin');
    }
    if (httpMode) {
        cmd.push('--http');
    }
    var initializer = functionConfig.initializer;
    if (initializer && invokeInitializer) {
        cmd.push('-i', initializer);
    }
    var initializationTimeout = functionConfig.initializationTimeout;
    // initializationTimeout is defined as integer, see lib/validate/schema/function.js
    if (initializationTimeout) {
        cmd.push('--initializationTimeout', initializationTimeout.toString());
    }
    logger_1.default.debug("docker cmd: " + cmd);
    return cmd;
}
function genDockerCmdOfCustomContainer(functionConfig) {
    var command = functionConfig.customContainerConfig.command ? JSON.parse(functionConfig.customContainerConfig.command) : undefined;
    var args = functionConfig.customContainerConfig.args ? JSON.parse(functionConfig.customContainerConfig.args) : undefined;
    if (command && args) {
        return __spreadArrays(command, args);
    }
    else if (command) {
        return command;
    }
    else if (args) {
        return args;
    }
    return [];
}
function generateDockerEnvs(creds, region, baseDir, serviceName, serviceProps, functionName, functionProps, debugPort, httpParams, nasConfig, debugIde, debugArgs) {
    return __awaiter(this, void 0, void 0, function () {
        var envs, confEnv, runtime, debugEnv, logConfigInEnv;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    envs = {};
                    if (httpParams) {
                        Object.assign(envs, {
                            'FC_HTTP_PARAMS': httpParams
                        });
                    }
                    return [4 /*yield*/, env_1.resolveLibPathsFromLdConf(baseDir, functionProps.codeUri)];
                case 1:
                    confEnv = _a.sent();
                    Object.assign(envs, confEnv);
                    runtime = functionProps.runtime;
                    if (debugPort && !debugArgs) {
                        debugEnv = debug_1.generateDebugEnv(runtime, debugPort, debugIde);
                        Object.assign(envs, debugEnv);
                    }
                    else if (debugArgs) {
                        Object.assign(envs, {
                            DEBUG_OPTIONS: debugArgs
                        });
                    }
                    Object.assign(envs, generateFunctionEnvs(functionProps));
                    if (definition_1.isAutoConfig(serviceProps === null || serviceProps === void 0 ? void 0 : serviceProps.logConfig)) {
                        logConfigInEnv = definition_1.resolveAutoLogConfig(creds === null || creds === void 0 ? void 0 : creds.AccountID, region, serviceName);
                    }
                    else {
                        // @ts-ignore
                        logConfigInEnv = serviceProps === null || serviceProps === void 0 ? void 0 : serviceProps.logConfig;
                    }
                    if (functionProps === null || functionProps === void 0 ? void 0 : functionProps.runtime.includes('java')) {
                        Object.assign(envs, { 'fc_enable_new_java_ca': true });
                    }
                    Object.assign(envs, {
                        'local': true,
                        'FC_ACCESS_KEY_ID': creds === null || creds === void 0 ? void 0 : creds.AccessKeyID,
                        'FC_ACCESS_KEY_SECRET': creds === null || creds === void 0 ? void 0 : creds.AccessKeySecret,
                        'FC_SECURITY_TOKEN': creds === null || creds === void 0 ? void 0 : creds.SecurityToken,
                        'FC_ACCOUNT_ID': creds === null || creds === void 0 ? void 0 : creds.AccountID,
                        'FC_REGION': region,
                        'FC_FUNCTION_NAME': functionName,
                        'FC_HANDLER': functionProps.handler,
                        'FC_MEMORY_SIZE': functionProps.memorySize || 128,
                        'FC_TIMEOUT': functionProps.timeout || 3,
                        'FC_INITIALIZER': functionProps.initializer,
                        'FC_INITIALIZATION_TIMEOUT': functionProps.initializationTimeout || 3,
                        'FC_SERVICE_NAME': serviceName,
                        'FC_SERVICE_LOG_PROJECT': logConfigInEnv === null || logConfigInEnv === void 0 ? void 0 : logConfigInEnv.project,
                        'FC_SERVICE_LOG_STORE': logConfigInEnv === null || logConfigInEnv === void 0 ? void 0 : logConfigInEnv.logstore
                    });
                    if (runtime_1.isCustomContainerRuntime(functionProps.runtime)) {
                        return [2 /*return*/, envs];
                    }
                    return [2 /*return*/, env_1.addEnv(envs, nasConfig)];
            }
        });
    });
}
exports.generateDockerEnvs = generateDockerEnvs;
function generateFunctionEnvs(functionConfig) {
    var environmentVariables = functionConfig.environmentVariables;
    if (!environmentVariables) {
        return {};
    }
    return Object.assign({}, environmentVariables);
}
exports.generateFunctionEnvs = generateFunctionEnvs;
function pullFcImageIfNeed(imageName) {
    return __awaiter(this, void 0, void 0, function () {
        var exist;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, imageExist(imageName)];
                case 1:
                    exist = _a.sent();
                    if (!(!exist || !skipPullImage)) return [3 /*break*/, 3];
                    return [4 /*yield*/, pullImage(imageName)];
                case 2:
                    _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    logger_1.default.debug("skip pulling image " + imageName + "...");
                    logger_1.default.info("Skip pulling image " + imageName + "...");
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    });
}
exports.pullFcImageIfNeed = pullFcImageIfNeed;
function pullImage(imageName) {
    return __awaiter(this, void 0, void 0, function () {
        var resolveImageName, stream;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, dockerOpts.resolveImageNameForPull(imageName)];
                case 1:
                    resolveImageName = _a.sent();
                    return [4 /*yield*/, docker.pull(resolveImageName)];
                case 2:
                    stream = _a.sent();
                    return [4 /*yield*/, new Promise(function (resolve, reject) {
                            logger_1.default.info("Pulling image " + resolveImageName + ", you can also use " + ("'docker pull " + resolveImageName + "'") + ' to pull image by yourself.');
                            var onFinished = function (err) { return __awaiter(_this, void 0, void 0, function () {
                                var _i, _a, r, image, newImageName, repoTag;
                                return __generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0:
                                            if (err) {
                                                reject(err);
                                            }
                                            containers.delete(stream);
                                            _i = 0, _a = dockerOpts.DOCKER_REGISTRIES;
                                            _b.label = 1;
                                        case 1:
                                            if (!(_i < _a.length)) return [3 /*break*/, 5];
                                            r = _a[_i];
                                            if (!(resolveImageName.indexOf(r) === 0)) return [3 /*break*/, 4];
                                            return [4 /*yield*/, docker.getImage(resolveImageName)];
                                        case 2:
                                            image = _b.sent();
                                            newImageName = resolveImageName.slice(r.length + 1);
                                            repoTag = newImageName.split(':');
                                            // rename
                                            return [4 /*yield*/, image.tag({
                                                    name: resolveImageName,
                                                    repo: lodash_1.default.first(repoTag),
                                                    tag: lodash_1.default.last(repoTag)
                                                })];
                                        case 3:
                                            // rename
                                            _b.sent();
                                            return [3 /*break*/, 5];
                                        case 4:
                                            _i++;
                                            return [3 /*break*/, 1];
                                        case 5:
                                            resolve(resolveImageName);
                                            return [2 /*return*/];
                                    }
                                });
                            }); };
                            containers.add(stream);
                            // pull image progress
                            followProgress(stream, onFinished);
                        })];
                case 3: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.pullImage = pullImage;
function followProgress(stream, onFinished) {
    var barLines = {};
    var onProgress = function (event) {
        var status = event.status;
        if (event.progress) {
            status = event.status + " " + event.progress;
        }
        if (event.id) {
            var id = event.id;
            if (!barLines[id]) {
                barLines[id] = console.draft();
            }
            barLines[id](id + ': ' + status);
        }
        else {
            if (lodash_1.default.has(event, 'aux.ID')) {
                event.stream = event.aux.ID + '\n';
            }
            // If there is no id, the line should be wrapped manually.
            var out = event.status ? event.status + '\n' : event.stream;
            process.stdout.write(out);
        }
    };
    docker.modem.followProgress(stream, onFinished, onProgress);
}
function runContainer(opts, outputStream, errorStream, context) {
    return __awaiter(this, void 0, void 0, function () {
        var container, attachOpts, stream, errorTransform, logStream;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, createContainer(opts)];
                case 1:
                    container = _a.sent();
                    attachOpts = {
                        hijack: true,
                        stream: true,
                        stdin: true,
                        stdout: true,
                        stderr: true
                    };
                    return [4 /*yield*/, container.attach(attachOpts)];
                case 2:
                    stream = _a.sent();
                    if (!outputStream) {
                        outputStream = process.stdout;
                    }
                    if (!errorStream) {
                        errorStream = process.stderr;
                    }
                    errorTransform = error_processor_1.processorTransformFactory({
                        serviceName: context === null || context === void 0 ? void 0 : context.serviceName,
                        functionName: context === null || context === void 0 ? void 0 : context.functionName,
                        errorStream: errorStream
                    });
                    if (!isWin) {
                        container.modem.demuxStream(stream, outputStream, errorTransform);
                    }
                    return [4 /*yield*/, container.start()];
                case 3:
                    _a.sent();
                    if (!isWin) return [3 /*break*/, 5];
                    return [4 /*yield*/, container.logs({
                            stdout: true,
                            stderr: true,
                            follow: true
                        })];
                case 4:
                    logStream = _a.sent();
                    container.modem.demuxStream(logStream, outputStream, errorTransform);
                    _a.label = 5;
                case 5:
                    containers.add(container.id);
                    streams.add(stream);
                    return [2 /*return*/, {
                            container: container,
                            stream: stream
                        }];
            }
        });
    });
}
exports.runContainer = runContainer;
// todo: 当前只支持目录以及 jar。code uri 还可能是 oss 地址、目录、jar、zip?
function resolveCodeUriToMount(absCodeUri, readOnly) {
    if (readOnly === void 0) { readOnly = true; }
    return __awaiter(this, void 0, void 0, function () {
        var target, stats;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!absCodeUri) {
                        return [2 /*return*/, null];
                    }
                    target = null;
                    return [4 /*yield*/, fs.lstat(absCodeUri)];
                case 1:
                    stats = _a.sent();
                    if (stats.isDirectory()) {
                        target = '/code';
                    }
                    else {
                        // could not use path.join('/code', xxx)
                        // in windows, it will be translate to \code\xxx, and will not be recorgnized as a valid path in linux container
                        target = path_1.default.posix.join('/code', path_1.default.basename(absCodeUri));
                    }
                    // Mount the code directory as read only
                    return [2 /*return*/, {
                            Type: 'bind',
                            Source: absCodeUri,
                            Target: target,
                            ReadOnly: readOnly
                        }];
            }
        });
    });
}
exports.resolveCodeUriToMount = resolveCodeUriToMount;
function isDockerToolBoxAndEnsureDockerVersion() {
    return __awaiter(this, void 0, void 0, function () {
        var dockerInfo, obj;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, docker.info()];
                case 1:
                    dockerInfo = _a.sent();
                    return [4 /*yield*/, detectDockerVersion(dockerInfo.ServerVersion || '')];
                case 2:
                    _a.sent();
                    obj = (dockerInfo.Labels || []).map(function (e) { return lodash_1.default.split(e, '=', 2); })
                        .filter(function (e) { return e.length === 2; })
                        .reduce(function (acc, cur) { return (acc[cur[0]] = cur[1], acc); }, {});
                    return [2 /*return*/, process.platform === 'win32' && obj.provider === 'virtualbox'];
            }
        });
    });
}
exports.isDockerToolBoxAndEnsureDockerVersion = isDockerToolBoxAndEnsureDockerVersion;
function detectDockerVersion(serverVersion) {
    return __awaiter(this, void 0, void 0, function () {
        var cur;
        return __generator(this, function (_a) {
            cur = serverVersion.split('.');
            // 1.13.1
            if (Number.parseInt(cur[0]) === 1 && Number.parseInt(cur[1]) <= 13) {
                throw new Error("\nWe detected that your docker version is " + serverVersion + ", for a better experience, please upgrade the docker version.");
            }
            return [2 /*return*/];
        });
    });
}
exports.detectDockerVersion = detectDockerVersion;
function createContainer(opts) {
    var _a;
    return __awaiter(this, void 0, void 0, function () {
        var isWin, isMac, pathsOutofSharedPaths, dockerToolBox, container, ex_1;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    isWin = process.platform === 'win32';
                    isMac = process.platform === 'darwin';
                    if (!(opts && isMac)) return [3 /*break*/, 2];
                    if (!opts.HostConfig) return [3 /*break*/, 2];
                    return [4 /*yield*/, docker_support_1.findPathsOutofSharedPaths((_a = opts.HostConfig) === null || _a === void 0 ? void 0 : _a.Mounts)];
                case 1:
                    pathsOutofSharedPaths = _b.sent();
                    if (isMac && pathsOutofSharedPaths.length > 0) {
                        throw new Error("Please add directory '" + pathsOutofSharedPaths + "' to Docker File sharing list, more information please refer to https://github.com/alibaba/funcraft/blob/master/docs/usage/faq-zh.md");
                    }
                    _b.label = 2;
                case 2: return [4 /*yield*/, isDockerToolBoxAndEnsureDockerVersion()];
                case 3:
                    dockerToolBox = _b.sent();
                    _b.label = 4;
                case 4:
                    _b.trys.push([4, 6, , 7]);
                    return [4 /*yield*/, docker.createContainer(opts)];
                case 5:
                    // see https://github.com/apocas/dockerode/pull/38
                    container = _b.sent();
                    return [3 /*break*/, 7];
                case 6:
                    ex_1 = _b.sent();
                    if (ex_1.message.indexOf('invalid mount config for type') !== -1 && dockerToolBox) {
                        throw new Error("The default host machine path for docker toolbox is under 'C:\\Users', Please make sure your project is in this directory. If you want to mount other disk paths, please refer to https://github.com/alibaba/funcraft/blob/master/docs/usage/faq-zh.md .");
                    }
                    if (ex_1.message.indexOf('drive is not shared') !== -1 && isWin) {
                        throw new Error(ex_1.message + "More information please refer to https://docs.docker.com/docker-for-windows/#shared-drives");
                    }
                    throw ex_1;
                case 7: return [2 /*return*/, container];
            }
        });
    });
}
function resolveNasConfigToMounts(baseDir, serviceName, nasConfig, nasBaseDir) {
    return __awaiter(this, void 0, void 0, function () {
        var nasMappings;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, nas.convertNasConfigToNasMappings(nasBaseDir, nasConfig, serviceName)];
                case 1:
                    nasMappings = _a.sent();
                    return [2 /*return*/, convertNasMappingsToMounts(devs_1.getRootBaseDir(baseDir), nasMappings)];
            }
        });
    });
}
exports.resolveNasConfigToMounts = resolveNasConfigToMounts;
function convertNasMappingsToMounts(baseDir, nasMappings) {
    return nasMappings.map(function (nasMapping) {
        // console.log('mounting local nas mock dir %s into container %s\n', nasMapping.localNasDir, nasMapping.remoteNasDir);
        return {
            Type: 'bind',
            Source: path_1.default.resolve(baseDir, nasMapping.localNasDir),
            Target: nasMapping.remoteNasDir,
            ReadOnly: false
        };
    });
}
function resolveTmpDirToMount(absTmpDir) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            if (!absTmpDir) {
                return [2 /*return*/, {}];
            }
            return [2 /*return*/, {
                    Type: 'bind',
                    Source: absTmpDir,
                    Target: '/tmp',
                    ReadOnly: false
                }];
        });
    });
}
exports.resolveTmpDirToMount = resolveTmpDirToMount;
function resolveDebuggerPathToMount(debuggerPath) {
    return __awaiter(this, void 0, void 0, function () {
        var absDebuggerPath;
        return __generator(this, function (_a) {
            if (!debuggerPath) {
                return [2 /*return*/, {}];
            }
            absDebuggerPath = path_1.default.resolve(debuggerPath);
            return [2 /*return*/, {
                    Type: 'bind',
                    Source: absDebuggerPath,
                    Target: '/tmp/debugger_files',
                    ReadOnly: false
                }];
        });
    });
}
exports.resolveDebuggerPathToMount = resolveDebuggerPathToMount;
function resolvePasswdMount() {
    return __awaiter(this, void 0, void 0, function () {
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    if (!(process.platform === 'linux')) return [3 /*break*/, 2];
                    _a = {
                        Type: 'bind'
                    };
                    return [4 /*yield*/, passwd_1.generatePwdFile()];
                case 1: return [2 /*return*/, (_a.Source = _b.sent(),
                        _a.Target = '/etc/passwd',
                        _a.ReadOnly = true,
                        _a)];
                case 2: return [2 /*return*/, null];
            }
        });
    });
}
exports.resolvePasswdMount = resolvePasswdMount;
function createAndRunContainer(opts) {
    return __awaiter(this, void 0, void 0, function () {
        var container;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, createContainer(opts)];
                case 1:
                    container = _a.sent();
                    containers.add(container.id);
                    return [4 /*yield*/, container.start({})];
                case 2:
                    _a.sent();
                    return [2 /*return*/, container];
            }
        });
    });
}
exports.createAndRunContainer = createAndRunContainer;
function showDebugIdeTipsForVscode(serviceName, functionName, runtime, codeSource, debugPort) {
    return __awaiter(this, void 0, void 0, function () {
        var vscodeDebugConfig;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, debug_1.generateVscodeDebugConfig(serviceName, functionName, runtime, codeSource, debugPort)];
                case 1:
                    vscodeDebugConfig = _a.sent();
                    // todo: auto detect .vscode/launch.json in codeuri path.
                    logger_1.default.log('You can paste these config to .vscode/launch.json, and then attach to your running function', 'yellow');
                    logger_1.default.log('///////////////// config begin /////////////////');
                    logger_1.default.log(JSON.stringify(vscodeDebugConfig, null, 4));
                    logger_1.default.log('///////////////// config end /////////////////');
                    return [2 /*return*/];
            }
        });
    });
}
exports.showDebugIdeTipsForVscode = showDebugIdeTipsForVscode;
function writeDebugIdeConfigForVscode(baseDir, serviceName, functionName, runtime, codeSource, debugPort) {
    return __awaiter(this, void 0, void 0, function () {
        var configJsonFolder, configJsonFilePath, e_1, vscodeDebugConfig, configInJsonFile, _a, _b, e_2;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    configJsonFolder = path_1.default.join(baseDir, '.vscode');
                    configJsonFilePath = path_1.default.join(configJsonFolder, 'launch.json');
                    _c.label = 1;
                case 1:
                    _c.trys.push([1, 3, , 5]);
                    return [4 /*yield*/, fs.ensureDir(path_1.default.dirname(configJsonFilePath))];
                case 2:
                    _c.sent();
                    return [3 /*break*/, 5];
                case 3:
                    e_1 = _c.sent();
                    logger_1.default.warning("Ensure directory: " + configJsonFolder + " failed.");
                    return [4 /*yield*/, showDebugIdeTipsForVscode(serviceName, functionName, runtime, codeSource, debugPort)];
                case 4:
                    _c.sent();
                    logger_1.default.debug("Ensure directory: " + configJsonFolder + " failed, error: " + e_1);
                    return [2 /*return*/];
                case 5: return [4 /*yield*/, debug_1.generateVscodeDebugConfig(serviceName, functionName, runtime, codeSource, debugPort)];
                case 6:
                    vscodeDebugConfig = _c.sent();
                    if (!(fs.pathExistsSync(configJsonFilePath) && fs.lstatSync(configJsonFilePath).isFile())) return [3 /*break*/, 9];
                    _b = (_a = JSON).parse;
                    return [4 /*yield*/, fs.readFile(configJsonFilePath, { encoding: 'utf8' })];
                case 7:
                    configInJsonFile = _b.apply(_a, [_c.sent()]);
                    if (lodash_1.default.isEqual(configInJsonFile, vscodeDebugConfig)) {
                        return [2 /*return*/];
                    }
                    logger_1.default.warning("File: " + configJsonFilePath + " already exists, please overwrite it with the following config.");
                    return [4 /*yield*/, showDebugIdeTipsForVscode(serviceName, functionName, runtime, codeSource, debugPort)];
                case 8:
                    _c.sent();
                    return [2 /*return*/];
                case 9:
                    _c.trys.push([9, 11, , 13]);
                    return [4 /*yield*/, fs.writeFile(configJsonFilePath, JSON.stringify(vscodeDebugConfig, null, '  '), { encoding: 'utf8', flag: 'w' })];
                case 10:
                    _c.sent();
                    return [3 /*break*/, 13];
                case 11:
                    e_2 = _c.sent();
                    logger_1.default.warning("Write " + configJsonFilePath + " failed.");
                    return [4 /*yield*/, showDebugIdeTipsForVscode(serviceName, functionName, runtime, codeSource, debugPort)];
                case 12:
                    _c.sent();
                    logger_1.default.debug("Write " + configJsonFilePath + " failed, error: " + e_2);
                    return [3 /*break*/, 13];
                case 13: return [2 /*return*/];
            }
        });
    });
}
exports.writeDebugIdeConfigForVscode = writeDebugIdeConfigForVscode;
function showDebugIdeTipsForPycharm(codeSource, debugPort) {
    return __awaiter(this, void 0, void 0, function () {
        var stats;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, fs.lstat(codeSource)];
                case 1:
                    stats = _a.sent();
                    if (!stats.isDirectory()) {
                        codeSource = path_1.default.dirname(codeSource);
                    }
                    logger_1.default.log("\n========= Tips for PyCharm remote debug =========\nLocal host name: " + ip.address() + "\nPort           : " + debugPort + "\nPath mappings  : " + codeSource + "=/code\n\nDebug Code needed to:\n 1. Install pydevd-pycharm:\n \n pip install pydevd-pycharm~=203.5981.165\n \n 2. copy to your function code:\n\nimport pydevd_pycharm\npydevd_pycharm.settrace('" + ip.address() + "', port=" + debugPort + ", stdoutToServer=True, stderrToServer=True)\n\n=========================================================================\n", 'yellow');
                    return [2 /*return*/];
            }
        });
    });
}
exports.showDebugIdeTipsForPycharm = showDebugIdeTipsForPycharm;
function writeEventToStreamAndClose(stream, event) {
    if (event) {
        stream.write(event);
    }
    stream.end();
}
// outputStream, errorStream used for http invoke
// because agent is started when container running and exec could not receive related logs
function startContainer(opts, outputStream, errorStream, context) {
    return __awaiter(this, void 0, void 0, function () {
        var container, err_1, logs, logStream;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, createContainer(opts)];
                case 1:
                    container = _a.sent();
                    containers.add(container.id);
                    _a.label = 2;
                case 2:
                    _a.trys.push([2, 4, , 5]);
                    return [4 /*yield*/, container.start({})];
                case 3:
                    _a.sent();
                    return [3 /*break*/, 5];
                case 4:
                    err_1 = _a.sent();
                    logger_1.default.error(err_1);
                    return [3 /*break*/, 5];
                case 5:
                    logs = outputStream || errorStream;
                    if (!logs) return [3 /*break*/, 7];
                    if (!outputStream) {
                        outputStream = dev_null_1.default();
                    }
                    if (!errorStream) {
                        errorStream = dev_null_1.default();
                    }
                    return [4 /*yield*/, container.logs({
                            stdout: true,
                            stderr: true,
                            follow: true
                        })];
                case 6:
                    logStream = _a.sent();
                    container.modem.demuxStream(logStream, outputStream, error_processor_1.processorTransformFactory({
                        serviceName: context.serviceName,
                        functionName: context.functionName,
                        errorStream: errorStream
                    }));
                    _a.label = 7;
                case 7: return [2 /*return*/, {
                        containerId: container === null || container === void 0 ? void 0 : container.id,
                        stop: function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        logger_1.default.debug("Stopping container: " + container.id);
                                        return [4 /*yield*/, container.stop()];
                                    case 1:
                                        _a.sent();
                                        containers.delete(container.id);
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                        kill: function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        logger_1.default.debug("Killing container: " + container.id);
                                        return [4 /*yield*/, container.kill()];
                                    case 1:
                                        _a.sent();
                                        containers.delete(container.id);
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                        exec: function (cmd, _a) {
                            var _b = _a === void 0 ? {} : _a, _c = _b.cwd, cwd = _c === void 0 ? '' : _c, _d = _b.env, env = _d === void 0 ? {} : _d, _e = _b.outputStream, outputStream = _e === void 0 ? process.stdout : _e, _f = _b.errorStream, errorStream = _f === void 0 ? process.stderr : _f, _g = _b.verbose, verbose = _g === void 0 ? false : _g, _h = _b.context, context = _h === void 0 ? {} : _h, _j = _b.event, event = _j === void 0 ? null : _j;
                            return __awaiter(_this, void 0, void 0, function () {
                                var stdin, options, exec, stream;
                                return __generator(this, function (_k) {
                                    switch (_k.label) {
                                        case 0:
                                            stdin = event ? true : false;
                                            options = {
                                                Env: dockerOpts.resolveDockerEnv(env),
                                                Tty: false,
                                                AttachStdin: stdin,
                                                AttachStdout: true,
                                                AttachStderr: true,
                                                WorkingDir: cwd
                                            };
                                            if (cmd !== []) {
                                                options.Cmd = cmd;
                                            }
                                            // docker exec
                                            logger_1.default.debug("docker exec opts: " + JSON.stringify(options, null, 4));
                                            return [4 /*yield*/, container.exec(options)];
                                        case 1:
                                            exec = _k.sent();
                                            return [4 /*yield*/, exec.start({ hijack: true, stdin: stdin })];
                                        case 2:
                                            stream = _k.sent();
                                            // todo: have to wait, otherwise stdin may not be readable
                                            return [4 /*yield*/, new Promise(function (resolve) { return setTimeout(resolve, 30); })];
                                        case 3:
                                            // todo: have to wait, otherwise stdin may not be readable
                                            _k.sent();
                                            if (event !== null) {
                                                writeEventToStreamAndClose(stream, event);
                                            }
                                            if (!outputStream) {
                                                outputStream = process.stdout;
                                            }
                                            if (!errorStream) {
                                                errorStream = process.stderr;
                                            }
                                            if (verbose) {
                                                container.modem.demuxStream(stream, outputStream, errorStream);
                                            }
                                            else {
                                                container.modem.demuxStream(stream, dev_null_1.default(), errorStream);
                                            }
                                            return [4 /*yield*/, waitForExec(exec)];
                                        case 4: return [2 /*return*/, _k.sent()];
                                    }
                                });
                            });
                        }
                    }];
            }
        });
    });
}
exports.startContainer = startContainer;
function waitForExec(exec) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, new Promise(function (resolve, reject) {
                        // stream.on('end') could not receive end event on windows.
                        // so use inspect to check exec exit
                        function waitContainerExec() {
                            exec.inspect(function (err, data) {
                                if (data === null || data === void 0 ? void 0 : data.Running) {
                                    setTimeout(waitContainerExec, 100);
                                    return;
                                }
                                if (err) {
                                    reject(err);
                                }
                                else if (data.ExitCode !== 0) {
                                    reject(data.ProcessConfig.entrypoint + " exited with code " + data.ExitCode);
                                }
                                else {
                                    resolve(data.ExitCode);
                                }
                            });
                        }
                        waitContainerExec();
                    })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
function run(opts, event, outputStream, errorStream, context) {
    if (context === void 0) { context = {}; }
    return __awaiter(this, void 0, void 0, function () {
        var _a, container, stream, exitRs;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, runContainer(opts, outputStream, errorStream, context)];
                case 1:
                    _a = _b.sent(), container = _a.container, stream = _a.stream;
                    writeEventToStreamAndClose(stream, event);
                    return [4 /*yield*/, container.wait()];
                case 2:
                    exitRs = _b.sent();
                    containers.delete(container.id);
                    streams.delete(stream);
                    return [2 /*return*/, exitRs];
            }
        });
    });
}
exports.run = run;
function stopContainer(container) {
    return __awaiter(this, void 0, void 0, function () {
        var stopVm, e_3, e_4;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    stopVm = core.spinner("Stopping the container: " + container.id);
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 8]);
                    return [4 /*yield*/, container.stop()];
                case 2:
                    _a.sent();
                    stopVm.succeed("Stop container succeed.");
                    return [3 /*break*/, 8];
                case 3:
                    e_3 = _a.sent();
                    stopVm.fail("Failed to stop the container.");
                    logger_1.default.debug("Stop the container: " + container.id + " failed, error: " + e_3);
                    stopVm = core.spinner("Killing the container: " + container.id);
                    _a.label = 4;
                case 4:
                    _a.trys.push([4, 6, , 7]);
                    return [4 /*yield*/, container.kill()];
                case 5:
                    _a.sent();
                    stopVm.succeed("Kill container succeed");
                    return [3 /*break*/, 7];
                case 6:
                    e_4 = _a.sent();
                    stopVm.fail("Failed to kill the container.Please stop it manually.");
                    logger_1.default.debug("Kill proxy container: " + container.id + " failed, error: " + e_4);
                    return [3 /*break*/, 7];
                case 7: return [3 /*break*/, 8];
                case 8: return [2 /*return*/];
            }
        });
    });
}
exports.stopContainer = stopContainer;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9ja2VyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9kb2NrZXIvZG9ja2VyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsd0RBQStCO0FBRS9CLCtEQUF5QztBQUN6QyxpREFBcUM7QUFDckMsbURBQTJEO0FBQzNELHNEQUE2RDtBQUM3RCxrREFBdUI7QUFFdkIsdURBQTJDO0FBQzNDLDhDQUF3QjtBQUN4QixnQ0FBdUM7QUFDdkMsMkNBQStCO0FBQy9CLDBDQUFnRDtBQUNoRCx3REFBNEM7QUFDNUMsK0NBQWtGO0FBQ2xGLHFDQUF5QjtBQUV6QiwyQ0FBc0U7QUFFdEUsNENBQTBEO0FBRTFELDRDQUFpRTtBQUVqRSxzREFBK0I7QUFDL0IsMERBQThDO0FBRTlDLElBQU0sS0FBSyxHQUFZLE9BQU8sQ0FBQyxRQUFRLEtBQUssT0FBTyxDQUFDO0FBQ3BELElBQU0sTUFBTSxHQUFRLElBQUksbUJBQU0sRUFBRSxDQUFDO0FBQ2pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDdkIsSUFBSSxVQUFVLEdBQVEsSUFBSSxHQUFHLEVBQUUsQ0FBQztBQUNoQyxJQUFJLE9BQU8sR0FBUSxJQUFJLEdBQUcsRUFBRSxDQUFDO0FBRTdCLDBDQUEwQztBQUMxQyxJQUFNLGFBQWEsR0FBWSxJQUFJLENBQUM7QUFHcEMsU0FBc0IsVUFBVSxDQUFDLFFBQWdCOzs7Ozt3QkFDbEIscUJBQU0sTUFBTSxDQUFDLFVBQVUsQ0FBQzt3QkFDbkQsT0FBTyxFQUFFOzRCQUNMLFNBQVMsRUFBRSxDQUFDLFFBQVEsQ0FBQzt5QkFDeEI7cUJBQ0osQ0FBQyxFQUFBOztvQkFKUSxNQUFNLEdBQWUsU0FJN0I7b0JBR0Ysc0JBQU8sTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUM7Ozs7Q0FDeEI7QUFURCxnQ0FTQztBQUVELFNBQWdCLDJCQUEyQjtJQUN2QyxPQUFPLGNBQVksSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsU0FBSSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFHLENBQUM7QUFDekYsQ0FBQztBQUZELGtFQUVDO0FBRUQsU0FBc0IsZUFBZSxDQUFDLGFBQXFCLEVBQUUsU0FBaUIsRUFBRSxTQUFpQixFQUFFLFFBQWdCOzs7Ozs7b0JBQ3pHLFFBQVEsR0FBTSxhQUFhLFNBQUksU0FBUyxTQUFJLFNBQVMsU0FBSSxRQUFVLENBQUM7b0JBQzFFLGdCQUFNLENBQUMsS0FBSyxDQUFDLHNCQUFvQixRQUFVLENBQUMsQ0FBQztvQkFFeEMscUJBQU0sVUFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFBOzt5QkFBM0IsQ0FBQyxDQUFBLFNBQTBCLENBQUEsRUFBM0Isd0JBQTJCO29CQUMzQixnQkFBTSxDQUFDLEtBQUssQ0FBQywwQkFBd0IsUUFBVSxDQUFDLENBQUM7b0JBQ3hDLHFCQUFNLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUE7O29CQUFwQyxNQUFNLEdBQUcsU0FBMkIsQ0FBQzs7d0JBR2xDLHFCQUFNLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07d0JBQ3JDLElBQUksTUFBTSxFQUFFOzRCQUNSLElBQU0sVUFBVSxHQUFHLFVBQUMsR0FBRztnQ0FDbkIsSUFBSSxHQUFHLEVBQUU7b0NBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lDQUFFO2dDQUN6QixPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQ3RCLENBQUMsQ0FBQTs0QkFDRCxJQUFNLFVBQVEsR0FBUSxFQUFFLENBQUM7NEJBQ3pCLElBQU0sVUFBVSxHQUFhLFVBQUMsS0FBSztnQ0FDL0IsSUFBSSxNQUFNLEdBQVEsS0FBSyxDQUFDLE1BQU0sQ0FBQztnQ0FFL0IsSUFBSSxLQUFLLENBQUMsUUFBUSxFQUFFO29DQUNoQixNQUFNLEdBQU0sS0FBSyxDQUFDLE1BQU0sU0FBSSxLQUFLLENBQUMsUUFBVSxDQUFDO2lDQUNoRDtnQ0FFRCxJQUFJLEtBQUssQ0FBQyxFQUFFLEVBQUU7b0NBQ1YsSUFBTSxFQUFFLEdBQVcsS0FBSyxDQUFDLEVBQUUsQ0FBQztvQ0FFNUIsSUFBSSxDQUFDLFVBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRTt3Q0FDZixVQUFRLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO3FDQUNsQztvQ0FDRCxVQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBRSxHQUFHLElBQUksR0FBRyxNQUFNLENBQUMsQ0FBQztpQ0FDcEM7cUNBQU07b0NBQ0gsSUFBSSxnQkFBQyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsUUFBUSxDQUFDLEVBQUU7d0NBQ3hCLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDO3FDQUN0QztvQ0FDRCwwREFBMEQ7b0NBQzFELElBQU0sR0FBRyxHQUFRLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO29DQUNuRSxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztpQ0FDN0I7NEJBQ0wsQ0FBQyxDQUFDOzRCQUNGLE1BQU0sQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7eUJBQy9EOzZCQUFNOzRCQUNILE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzt5QkFDckI7b0JBRUwsQ0FBQyxDQUFDLEVBQUE7d0JBbkNGLHNCQUFPLFNBbUNMLEVBQUM7Ozs7Q0FDTjtBQTdDRCwwQ0E2Q0M7QUFFRCxTQUFnQixpQkFBaUIsQ0FBQyxPQUFlLEVBQUUsZ0JBQXlCLEVBQUUsY0FBK0IsRUFBRSxRQUFrQixFQUFFLGlCQUF3QixFQUFFLEtBQVk7SUFBdEMsa0NBQUEsRUFBQSx3QkFBd0I7SUFBRSxzQkFBQSxFQUFBLFlBQVk7SUFDckssSUFBSSxrQ0FBd0IsQ0FBQyxPQUFPLENBQUMsRUFBRTtRQUNuQyxPQUFPLDZCQUE2QixDQUFDLGNBQWMsQ0FBQyxDQUFDO0tBQ3hEO1NBQU0sSUFBSSxnQkFBZ0IsRUFBRTtRQUN6QixPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7S0FDdkI7SUFDRCxPQUFPLGdDQUFnQyxDQUFDLGNBQWMsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLENBQUM7QUFDaEcsQ0FBQztBQVBELDhDQU9DO0FBRUQsU0FBUyxnQ0FBZ0MsQ0FBQyxjQUE4QixFQUFFLFFBQWlCLEVBQUUsaUJBQXdCLEVBQUUsS0FBWTtJQUF0QyxrQ0FBQSxFQUFBLHdCQUF3QjtJQUFFLHNCQUFBLEVBQUEsWUFBWTtJQUMvSCxJQUFNLEdBQUcsR0FBYSxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7SUFFckQsY0FBYztJQUNkLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTtRQUNoQixHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQzNELEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztLQUM5QjtTQUFNO1FBQ0gscUNBQXFDO1FBQ3JDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7S0FDdkI7SUFFRCxJQUFJLFFBQVEsRUFBRTtRQUNWLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDdEI7SUFFRCxJQUFNLFdBQVcsR0FBRyxjQUFjLENBQUMsV0FBVyxDQUFDO0lBRS9DLElBQUksV0FBVyxJQUFJLGlCQUFpQixFQUFFO1FBQ2xDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO0tBQy9CO0lBRUQsSUFBTSxxQkFBcUIsR0FBRyxjQUFjLENBQUMscUJBQXFCLENBQUM7SUFFbkUsbUZBQW1GO0lBQ25GLElBQUkscUJBQXFCLEVBQUU7UUFDdkIsR0FBRyxDQUFDLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxxQkFBcUIsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0tBQ3pFO0lBRUQsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWUsR0FBSyxDQUFDLENBQUM7SUFFbkMsT0FBTyxHQUFHLENBQUM7QUFDZixDQUFDO0FBR0QsU0FBUyw2QkFBNkIsQ0FBQyxjQUE4QjtJQUNqRSxJQUFNLE9BQU8sR0FBUSxjQUFjLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO0lBQ3pJLElBQU0sSUFBSSxHQUFRLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7SUFFaEksSUFBSSxPQUFPLElBQUksSUFBSSxFQUFFO1FBQ2pCLHNCQUFXLE9BQU8sRUFBSyxJQUFJLEVBQUU7S0FDaEM7U0FBTSxJQUFJLE9BQU8sRUFBRTtRQUNoQixPQUFPLE9BQU8sQ0FBQztLQUNsQjtTQUFNLElBQUksSUFBSSxFQUFFO1FBQ2IsT0FBTyxJQUFJLENBQUM7S0FDZjtJQUNELE9BQU8sRUFBRSxDQUFDO0FBQ2QsQ0FBQztBQUlELFNBQXNCLGtCQUFrQixDQUFDLEtBQW1CLEVBQUUsTUFBYyxFQUFFLE9BQWUsRUFBRSxXQUFtQixFQUFFLFlBQTJCLEVBQUUsWUFBb0IsRUFBRSxhQUE2QixFQUFFLFNBQWlCLEVBQUUsVUFBZSxFQUFFLFNBQTZCLEVBQUUsUUFBYSxFQUFFLFNBQWU7Ozs7OztvQkFDN1IsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFFaEIsSUFBSSxVQUFVLEVBQUU7d0JBQ1osTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUU7NEJBQ2hCLGdCQUFnQixFQUFFLFVBQVU7eUJBQy9CLENBQUMsQ0FBQztxQkFDTjtvQkFFZSxxQkFBTSwrQkFBeUIsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLE9BQU8sQ0FBQyxFQUFBOztvQkFBekUsT0FBTyxHQUFHLFNBQStEO29CQUUvRSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFFdkIsT0FBTyxHQUFXLGFBQWEsQ0FBQyxPQUFPLENBQUM7b0JBRTlDLElBQUksU0FBUyxJQUFJLENBQUMsU0FBUyxFQUFFO3dCQUNuQixRQUFRLEdBQUcsd0JBQWdCLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQzt3QkFFaEUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7cUJBQ2pDO3lCQUFNLElBQUksU0FBUyxFQUFFO3dCQUNsQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTs0QkFDaEIsYUFBYSxFQUFFLFNBQVM7eUJBQzNCLENBQUMsQ0FBQztxQkFDTjtvQkFFRCxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxvQkFBb0IsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO29CQUd6RCxJQUFJLHlCQUFZLENBQUMsWUFBWSxhQUFaLFlBQVksdUJBQVosWUFBWSxDQUFFLFNBQVMsQ0FBQyxFQUFFO3dCQUN2QyxjQUFjLEdBQUcsaUNBQW9CLENBQUMsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUM7cUJBQ2hGO3lCQUFNO3dCQUNILGFBQWE7d0JBQ2IsY0FBYyxHQUFHLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxTQUFTLENBQUM7cUJBQzVDO29CQUNELElBQUksYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE9BQU8sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHO3dCQUN6QyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxFQUFFLHVCQUF1QixFQUFFLElBQUksRUFBRSxDQUFDLENBQUE7cUJBQ3pEO29CQUNELE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFO3dCQUNoQixPQUFPLEVBQUUsSUFBSTt3QkFDYixrQkFBa0IsRUFBRSxLQUFLLGFBQUwsS0FBSyx1QkFBTCxLQUFLLENBQUUsV0FBVzt3QkFDdEMsc0JBQXNCLEVBQUUsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLGVBQWU7d0JBQzlDLG1CQUFtQixFQUFFLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxhQUFhO3dCQUN6QyxlQUFlLEVBQUUsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLFNBQVM7d0JBQ2pDLFdBQVcsRUFBRSxNQUFNO3dCQUNuQixrQkFBa0IsRUFBRSxZQUFZO3dCQUNoQyxZQUFZLEVBQUUsYUFBYSxDQUFDLE9BQU87d0JBQ25DLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxVQUFVLElBQUksR0FBRzt3QkFDakQsWUFBWSxFQUFFLGFBQWEsQ0FBQyxPQUFPLElBQUksQ0FBQzt3QkFDeEMsZ0JBQWdCLEVBQUUsYUFBYSxDQUFDLFdBQVc7d0JBQzNDLDJCQUEyQixFQUFFLGFBQWEsQ0FBQyxxQkFBcUIsSUFBSSxDQUFDO3dCQUNyRSxpQkFBaUIsRUFBRSxXQUFXO3dCQUM5Qix3QkFBd0IsRUFBRSxjQUFjLGFBQWQsY0FBYyx1QkFBZCxjQUFjLENBQUUsT0FBTzt3QkFDakQsc0JBQXNCLEVBQUUsY0FBYyxhQUFkLGNBQWMsdUJBQWQsY0FBYyxDQUFFLFFBQVE7cUJBQ25ELENBQUMsQ0FBQztvQkFFSCxJQUFJLGtDQUF3QixDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsRUFBRTt3QkFDakQsc0JBQU8sSUFBSSxFQUFDO3FCQUNmO29CQUNELHNCQUFPLFlBQU0sQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLEVBQUM7Ozs7Q0FDbEM7QUEzREQsZ0RBMkRDO0FBRUQsU0FBZ0Isb0JBQW9CLENBQUMsY0FBOEI7SUFDL0QsSUFBTSxvQkFBb0IsR0FBRyxjQUFjLENBQUMsb0JBQW9CLENBQUM7SUFFakUsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1FBQUUsT0FBTyxFQUFFLENBQUM7S0FBRTtJQUV6QyxPQUFPLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLG9CQUFvQixDQUFDLENBQUM7QUFDbkQsQ0FBQztBQU5ELG9EQU1DO0FBSUQsU0FBc0IsaUJBQWlCLENBQUMsU0FBUzs7Ozs7d0JBQ3RCLHFCQUFNLFVBQVUsQ0FBQyxTQUFTLENBQUMsRUFBQTs7b0JBQTVDLEtBQUssR0FBWSxTQUEyQjt5QkFFOUMsQ0FBQSxDQUFDLEtBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQSxFQUF4Qix3QkFBd0I7b0JBRXhCLHFCQUFNLFNBQVMsQ0FBQyxTQUFTLENBQUMsRUFBQTs7b0JBQTFCLFNBQTBCLENBQUM7OztvQkFFM0IsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsd0JBQXNCLFNBQVMsUUFBSyxDQUFDLENBQUM7b0JBQ25ELGdCQUFNLENBQUMsSUFBSSxDQUFDLHdCQUFzQixTQUFTLFFBQUssQ0FBQyxDQUFDOzs7Ozs7Q0FFekQ7QUFWRCw4Q0FVQztBQUVELFNBQXNCLFNBQVMsQ0FBQyxTQUFpQjs7Ozs7O3dCQUVaLHFCQUFNLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsRUFBQTs7b0JBQTlFLGdCQUFnQixHQUFXLFNBQW1EO29CQUdoRSxxQkFBTSxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUE7O29CQUFqRCxNQUFNLEdBQVEsU0FBbUM7b0JBR2hELHFCQUFNLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07NEJBRXJDLGdCQUFNLENBQUMsSUFBSSxDQUFDLG1CQUFpQixnQkFBZ0Isd0JBQXFCLElBQUcsa0JBQWdCLGdCQUFnQixNQUFHLENBQUEsR0FBRyw2QkFBNkIsQ0FBQyxDQUFDOzRCQUUxSSxJQUFNLFVBQVUsR0FBRyxVQUFPLEdBQUc7Ozs7OzRDQUN6QixJQUFJLEdBQUcsRUFBRTtnREFDTCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7NkNBQ2Y7NENBQ0QsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztrREFFa0IsRUFBNUIsS0FBQSxVQUFVLENBQUMsaUJBQWlCOzs7aURBQTVCLENBQUEsY0FBNEIsQ0FBQTs0Q0FBakMsQ0FBQztpREFDSixDQUFBLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUEsRUFBakMsd0JBQWlDOzRDQUNkLHFCQUFNLE1BQU0sQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsRUFBQTs7NENBQXBELEtBQUssR0FBUSxTQUF1Qzs0Q0FFcEQsWUFBWSxHQUFXLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDOzRDQUM1RCxPQUFPLEdBQWEsWUFBWSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzs0Q0FFbEQsU0FBUzs0Q0FDVCxxQkFBTSxLQUFLLENBQUMsR0FBRyxDQUFDO29EQUNaLElBQUksRUFBRSxnQkFBZ0I7b0RBQ3RCLElBQUksRUFBRSxnQkFBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7b0RBQ3RCLEdBQUcsRUFBRSxnQkFBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7aURBQ3ZCLENBQUMsRUFBQTs7NENBTEYsU0FBUzs0Q0FDVCxTQUlFLENBQUM7NENBQ0gsd0JBQU07OzRDQWJFLElBQTRCLENBQUE7Ozs0Q0FnQjVDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzs7O2lDQUM3QixDQUFDOzRCQUVGLFVBQVUsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQ3ZCLHNCQUFzQjs0QkFDdEIsY0FBYyxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDdkMsQ0FBQyxDQUFDLEVBQUE7d0JBaENGLHNCQUFPLFNBZ0NMLEVBQUM7Ozs7Q0FDTjtBQXpDRCw4QkF5Q0M7QUFDRCxTQUFTLGNBQWMsQ0FBQyxNQUFXLEVBQUUsVUFBZTtJQUVoRCxJQUFNLFFBQVEsR0FBUSxFQUFFLENBQUM7SUFFekIsSUFBTSxVQUFVLEdBQWEsVUFBQyxLQUFLO1FBQy9CLElBQUksTUFBTSxHQUFRLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFFL0IsSUFBSSxLQUFLLENBQUMsUUFBUSxFQUFFO1lBQ2hCLE1BQU0sR0FBTSxLQUFLLENBQUMsTUFBTSxTQUFJLEtBQUssQ0FBQyxRQUFVLENBQUM7U0FDaEQ7UUFFRCxJQUFJLEtBQUssQ0FBQyxFQUFFLEVBQUU7WUFDVixJQUFNLEVBQUUsR0FBVyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBRTVCLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLEVBQUU7Z0JBQ2YsUUFBUSxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUNsQztZQUNELFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLEdBQUcsSUFBSSxHQUFHLE1BQU0sQ0FBQyxDQUFDO1NBQ3BDO2FBQU07WUFDSCxJQUFJLGdCQUFDLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDeEIsS0FBSyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUM7YUFDdEM7WUFDRCwwREFBMEQ7WUFDMUQsSUFBTSxHQUFHLEdBQVEsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDbkUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDN0I7SUFDTCxDQUFDLENBQUM7SUFFRixNQUFNLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0FBQ2hFLENBQUM7QUFJRCxTQUFzQixZQUFZLENBQUMsSUFBSSxFQUFFLFlBQWtCLEVBQUUsV0FBaUIsRUFBRSxPQUFhOzs7Ozt3QkFDdkUscUJBQU0sZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFBOztvQkFBdkMsU0FBUyxHQUFHLFNBQTJCO29CQUN2QyxVQUFVLEdBQUc7d0JBQ2YsTUFBTSxFQUFFLElBQUk7d0JBQ1osTUFBTSxFQUFFLElBQUk7d0JBQ1osS0FBSyxFQUFFLElBQUk7d0JBQ1gsTUFBTSxFQUFFLElBQUk7d0JBQ1osTUFBTSxFQUFFLElBQUk7cUJBQ2YsQ0FBQztvQkFFYSxxQkFBTSxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxFQUFBOztvQkFBM0MsTUFBTSxHQUFHLFNBQWtDO29CQUVqRCxJQUFJLENBQUMsWUFBWSxFQUFFO3dCQUNmLFlBQVksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO3FCQUNqQztvQkFFRCxJQUFJLENBQUMsV0FBVyxFQUFFO3dCQUNkLFdBQVcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO3FCQUNoQztvQkFFSyxjQUFjLEdBQUcsMkNBQXlCLENBQUM7d0JBQzdDLFdBQVcsRUFBRSxPQUFPLGFBQVAsT0FBTyx1QkFBUCxPQUFPLENBQUUsV0FBVzt3QkFDakMsWUFBWSxFQUFFLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxZQUFZO3dCQUNuQyxXQUFXLEVBQUUsV0FBVztxQkFDM0IsQ0FBQyxDQUFDO29CQUVILElBQUksQ0FBQyxLQUFLLEVBQUU7d0JBQ1IsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLFlBQVksRUFBRSxjQUFjLENBQUMsQ0FBQztxQkFDckU7b0JBRUQscUJBQU0sU0FBUyxDQUFDLEtBQUssRUFBRSxFQUFBOztvQkFBdkIsU0FBdUIsQ0FBQzt5QkFFcEIsS0FBSyxFQUFMLHdCQUFLO29CQUNhLHFCQUFNLFNBQVMsQ0FBQyxJQUFJLENBQUM7NEJBQ25DLE1BQU0sRUFBRSxJQUFJOzRCQUNaLE1BQU0sRUFBRSxJQUFJOzRCQUNaLE1BQU0sRUFBRSxJQUFJO3lCQUNmLENBQUMsRUFBQTs7b0JBSkksU0FBUyxHQUFHLFNBSWhCO29CQUVGLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxZQUFZLEVBQUUsY0FBYyxDQUFDLENBQUM7OztvQkFHekUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3BCLHNCQUFPOzRCQUNILFNBQVMsV0FBQTs0QkFDVCxNQUFNLFFBQUE7eUJBQ1QsRUFBQzs7OztDQUNMO0FBaERELG9DQWdEQztBQUdELHVEQUF1RDtBQUN2RCxTQUFzQixxQkFBcUIsQ0FBQyxVQUFrQixFQUFFLFFBQWU7SUFBZix5QkFBQSxFQUFBLGVBQWU7Ozs7OztvQkFDM0UsSUFBSSxDQUFDLFVBQVUsRUFBRTt3QkFDYixzQkFBTyxJQUFJLEVBQUM7cUJBQ2Y7b0JBQ0csTUFBTSxHQUFXLElBQUksQ0FBQztvQkFFUCxxQkFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxFQUFBOztvQkFBdkMsS0FBSyxHQUFRLFNBQTBCO29CQUU3QyxJQUFJLEtBQUssQ0FBQyxXQUFXLEVBQUUsRUFBRTt3QkFDckIsTUFBTSxHQUFHLE9BQU8sQ0FBQztxQkFDcEI7eUJBQU07d0JBQ0gsd0NBQXdDO3dCQUN4QyxnSEFBZ0g7d0JBQ2hILE1BQU0sR0FBRyxjQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsY0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO3FCQUNoRTtvQkFFRCx3Q0FBd0M7b0JBQ3hDLHNCQUFPOzRCQUNILElBQUksRUFBRSxNQUFNOzRCQUNaLE1BQU0sRUFBRSxVQUFVOzRCQUNsQixNQUFNLEVBQUUsTUFBTTs0QkFDZCxRQUFRLEVBQUUsUUFBUTt5QkFDckIsRUFBQzs7OztDQUNMO0FBdkJELHNEQXVCQztBQUdELFNBQXNCLHFDQUFxQzs7Ozs7d0JBRS9CLHFCQUFNLE1BQU0sQ0FBQyxJQUFJLEVBQUUsRUFBQTs7b0JBQXJDLFVBQVUsR0FBUSxTQUFtQjtvQkFFM0MscUJBQU0sbUJBQW1CLENBQUMsVUFBVSxDQUFDLGFBQWEsSUFBSSxFQUFFLENBQUMsRUFBQTs7b0JBQXpELFNBQXlELENBQUM7b0JBRXBELEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsZ0JBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQzt5QkFDN0QsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQWQsQ0FBYyxDQUFDO3lCQUMzQixNQUFNLENBQUMsVUFBQyxHQUFHLEVBQUUsR0FBRyxJQUFLLE9BQUEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUEzQixDQUEyQixFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQUUzRCxzQkFBTyxPQUFPLENBQUMsUUFBUSxLQUFLLE9BQU8sSUFBSSxHQUFHLENBQUMsUUFBUSxLQUFLLFlBQVksRUFBQzs7OztDQUN4RTtBQVhELHNGQVdDO0FBRUQsU0FBc0IsbUJBQW1CLENBQUMsYUFBcUI7Ozs7WUFDdkQsR0FBRyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkMsU0FBUztZQUNULElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQ2hFLE1BQU0sSUFBSSxLQUFLLENBQUMsK0NBQTZDLGFBQWEsa0VBQStELENBQUMsQ0FBQzthQUM5STs7OztDQUNKO0FBTkQsa0RBTUM7QUFHRCxTQUFlLGVBQWUsQ0FBQyxJQUFTOzs7Ozs7O29CQUM5QixLQUFLLEdBQVksT0FBTyxDQUFDLFFBQVEsS0FBSyxPQUFPLENBQUM7b0JBQzlDLEtBQUssR0FBWSxPQUFPLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQzt5QkFFakQsQ0FBQSxJQUFJLElBQUksS0FBSyxDQUFBLEVBQWIsd0JBQWE7eUJBQ1QsSUFBSSxDQUFDLFVBQVUsRUFBZix3QkFBZTtvQkFDZSxxQkFBTSwwQ0FBeUIsT0FBQyxJQUFJLENBQUMsVUFBVSwwQ0FBRSxNQUFNLENBQUMsRUFBQTs7b0JBQWhGLHFCQUFxQixHQUFHLFNBQXdEO29CQUN0RixJQUFJLEtBQUssSUFBSSxxQkFBcUIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUMzQyxNQUFNLElBQUksS0FBSyxDQUFDLDJCQUF5QixxQkFBcUIseUlBQXNJLENBQUMsQ0FBQztxQkFDek07O3dCQUdhLHFCQUFNLHFDQUFxQyxFQUFFLEVBQUE7O29CQUE3RCxhQUFhLEdBQUcsU0FBNkM7Ozs7b0JBS25ELHFCQUFNLE1BQU0sQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUE7O29CQUQ5QyxrREFBa0Q7b0JBQ2xELFNBQVMsR0FBRyxTQUFrQyxDQUFDOzs7O29CQUcvQyxJQUFJLElBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLCtCQUErQixDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksYUFBYSxFQUFFO3dCQUM3RSxNQUFNLElBQUksS0FBSyxDQUFDLDBQQUEwUCxDQUFDLENBQUM7cUJBQy9RO29CQUNELElBQUksSUFBRSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxLQUFLLEVBQUU7d0JBQzNELE1BQU0sSUFBSSxLQUFLLENBQUksSUFBRSxDQUFDLE9BQU8sK0ZBQTRGLENBQUMsQ0FBQztxQkFDOUg7b0JBQ0QsTUFBTSxJQUFFLENBQUM7d0JBRWIsc0JBQU8sU0FBUyxFQUFDOzs7O0NBQ3BCO0FBRUQsU0FBc0Isd0JBQXdCLENBQUMsT0FBZSxFQUFFLFdBQW1CLEVBQUUsU0FBNkIsRUFBRSxVQUFrQjs7Ozs7d0JBQ3pHLHFCQUFNLEdBQUcsQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLFdBQVcsQ0FBQyxFQUFBOztvQkFBOUYsV0FBVyxHQUFRLFNBQTJFO29CQUNwRyxzQkFBTywwQkFBMEIsQ0FBQyxxQkFBYyxDQUFDLE9BQU8sQ0FBQyxFQUFFLFdBQVcsQ0FBQyxFQUFDOzs7O0NBQzNFO0FBSEQsNERBR0M7QUFFRCxTQUFTLDBCQUEwQixDQUFDLE9BQWUsRUFBRSxXQUFnQjtJQUNqRSxPQUFPLFdBQVcsQ0FBQyxHQUFHLENBQUMsVUFBQSxVQUFVO1FBQzdCLHNIQUFzSDtRQUN0SCxPQUFPO1lBQ0gsSUFBSSxFQUFFLE1BQU07WUFDWixNQUFNLEVBQUUsY0FBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLFdBQVcsQ0FBQztZQUNyRCxNQUFNLEVBQUUsVUFBVSxDQUFDLFlBQVk7WUFDL0IsUUFBUSxFQUFFLEtBQUs7U0FDbEIsQ0FBQztJQUNOLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUVELFNBQXNCLG9CQUFvQixDQUFDLFNBQWlCOzs7WUFDeEQsSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFBRSxzQkFBTyxFQUFFLEVBQUM7YUFBRTtZQUM5QixzQkFBTztvQkFDSCxJQUFJLEVBQUUsTUFBTTtvQkFDWixNQUFNLEVBQUUsU0FBUztvQkFDakIsTUFBTSxFQUFFLE1BQU07b0JBQ2QsUUFBUSxFQUFFLEtBQUs7aUJBQ2xCLEVBQUM7OztDQUNMO0FBUkQsb0RBUUM7QUFFRCxTQUFzQiwwQkFBMEIsQ0FBQyxZQUFvQjs7OztZQUNqRSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUFFLHNCQUFPLEVBQUUsRUFBQzthQUFFO1lBQzNCLGVBQWUsR0FBVyxjQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzNELHNCQUFPO29CQUNILElBQUksRUFBRSxNQUFNO29CQUNaLE1BQU0sRUFBRSxlQUFlO29CQUN2QixNQUFNLEVBQUUscUJBQXFCO29CQUM3QixRQUFRLEVBQUUsS0FBSztpQkFDbEIsRUFBQzs7O0NBQ0w7QUFURCxnRUFTQztBQUVELFNBQXNCLGtCQUFrQjs7Ozs7O3lCQUNoQyxDQUFBLE9BQU8sQ0FBQyxRQUFRLEtBQUssT0FBTyxDQUFBLEVBQTVCLHdCQUE0Qjs7d0JBRXhCLElBQUksRUFBRSxNQUFNOztvQkFDSixxQkFBTSx3QkFBZSxFQUFFLEVBQUE7d0JBRm5DLHVCQUVJLFNBQU0sR0FBRSxTQUF1Qjt3QkFDL0IsU0FBTSxHQUFFLGFBQWE7d0JBQ3JCLFdBQVEsR0FBRSxJQUFJOzZCQUNoQjt3QkFHTixzQkFBTyxJQUFJLEVBQUM7Ozs7Q0FDZjtBQVhELGdEQVdDO0FBRUQsU0FBc0IscUJBQXFCLENBQUMsSUFBSTs7Ozs7d0JBQzFCLHFCQUFNLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQTs7b0JBQXZDLFNBQVMsR0FBRyxTQUEyQjtvQkFDN0MsVUFBVSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzdCLHFCQUFNLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUE7O29CQUF6QixTQUF5QixDQUFDO29CQUMxQixzQkFBTyxTQUFTLEVBQUM7Ozs7Q0FDcEI7QUFMRCxzREFLQztBQUVELFNBQXNCLHlCQUF5QixDQUFDLFdBQW1CLEVBQUUsWUFBb0IsRUFBRSxPQUFlLEVBQUUsVUFBa0IsRUFBRSxTQUFrQjs7Ozs7d0JBQ3BILHFCQUFNLGlDQUF5QixDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUMsRUFBQTs7b0JBQTlHLGlCQUFpQixHQUFHLFNBQTBGO29CQUVwSCx5REFBeUQ7b0JBQ3pELGdCQUFNLENBQUMsR0FBRyxDQUFDLDZGQUE2RixFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUNwSCxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxrREFBa0QsQ0FBQyxDQUFDO29CQUMvRCxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN2RCxnQkFBTSxDQUFDLEdBQUcsQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDOzs7OztDQUNoRTtBQVJELDhEQVFDO0FBRUQsU0FBc0IsNEJBQTRCLENBQUMsT0FBZSxFQUFFLFdBQW1CLEVBQUUsWUFBb0IsRUFBRSxPQUFlLEVBQUUsVUFBa0IsRUFBRSxTQUFrQjs7Ozs7O29CQUM1SixnQkFBZ0IsR0FBVyxjQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztvQkFDekQsa0JBQWtCLEdBQVcsY0FBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxhQUFhLENBQUMsQ0FBQzs7OztvQkFFMUUscUJBQU0sRUFBRSxDQUFDLFNBQVMsQ0FBQyxjQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsRUFBQTs7b0JBQXBELFNBQW9ELENBQUM7Ozs7b0JBRXJELGdCQUFNLENBQUMsT0FBTyxDQUFDLHVCQUFxQixnQkFBZ0IsYUFBVSxDQUFDLENBQUM7b0JBQ2hFLHFCQUFNLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUMsRUFBQTs7b0JBQTFGLFNBQTBGLENBQUM7b0JBQzNGLGdCQUFNLENBQUMsS0FBSyxDQUFDLHVCQUFxQixnQkFBZ0Isd0JBQW1CLEdBQUcsQ0FBQyxDQUFDO29CQUMxRSxzQkFBTzt3QkFFZSxxQkFBTSxpQ0FBeUIsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLEVBQUE7O29CQUE5RyxpQkFBaUIsR0FBRyxTQUEwRjt5QkFDaEgsQ0FBQSxFQUFFLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFBLEVBQWxGLHdCQUFrRjtvQkFFekQsS0FBQSxDQUFBLEtBQUEsSUFBSSxDQUFBLENBQUMsS0FBSyxDQUFBO29CQUFDLHFCQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMsa0JBQWtCLEVBQUUsRUFBQyxRQUFRLEVBQUUsTUFBTSxFQUFDLENBQUMsRUFBQTs7b0JBQXZGLGdCQUFnQixHQUFHLGNBQVcsU0FBeUQsRUFBQztvQkFDOUYsSUFBSSxnQkFBQyxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxpQkFBaUIsQ0FBQyxFQUFFO3dCQUFFLHNCQUFPO3FCQUFFO29CQUMvRCxnQkFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFTLGtCQUFrQixvRUFBaUUsQ0FBQyxDQUFDO29CQUM3RyxxQkFBTSx5QkFBeUIsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLEVBQUE7O29CQUExRixTQUEwRixDQUFDO29CQUMzRixzQkFBTzs7O29CQUdQLHFCQUFNLEVBQUUsQ0FBQyxTQUFTLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUMsQ0FBQyxFQUFBOztvQkFBcEgsU0FBb0gsQ0FBQzs7OztvQkFFckgsZ0JBQU0sQ0FBQyxPQUFPLENBQUMsV0FBUyxrQkFBa0IsYUFBVSxDQUFDLENBQUM7b0JBQ3RELHFCQUFNLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLENBQUMsRUFBQTs7b0JBQTFGLFNBQTBGLENBQUM7b0JBQzNGLGdCQUFNLENBQUMsS0FBSyxDQUFDLFdBQVMsa0JBQWtCLHdCQUFtQixHQUFHLENBQUMsQ0FBQzs7Ozs7O0NBRXZFO0FBM0JELG9FQTJCQztBQUVELFNBQXNCLDBCQUEwQixDQUFDLFVBQWtCLEVBQUUsU0FBaUI7Ozs7O3dCQUVwRSxxQkFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxFQUFBOztvQkFBbEMsS0FBSyxHQUFHLFNBQTBCO29CQUV4QyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxFQUFFO3dCQUN0QixVQUFVLEdBQUcsY0FBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDekM7b0JBRUQsZ0JBQU0sQ0FBQyxHQUFHLENBQUMsMkVBQ0ksRUFBRSxDQUFDLE9BQU8sRUFBRSwyQkFDWixTQUFTLDJCQUNULFVBQVUsME1BVUYsRUFBRSxDQUFDLE9BQU8sRUFBRSxnQkFBVyxTQUFTLCtIQUVpQixFQUFFLFFBQVEsQ0FBQyxDQUFDOzs7OztDQUN2RjtBQXhCRCxnRUF3QkM7QUFFRCxTQUFTLDBCQUEwQixDQUFDLE1BQU0sRUFBRSxLQUFLO0lBRTdDLElBQUksS0FBSyxFQUFFO1FBQ1AsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUN2QjtJQUVELE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUNqQixDQUFDO0FBR0QsaURBQWlEO0FBQ2pELDBGQUEwRjtBQUMxRixTQUFzQixjQUFjLENBQUMsSUFBUyxFQUFFLFlBQWtCLEVBQUUsV0FBaUIsRUFBRSxPQUFhOzs7Ozs7d0JBRTlFLHFCQUFNLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBQTs7b0JBQXZDLFNBQVMsR0FBRyxTQUEyQjtvQkFFN0MsVUFBVSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7Ozs7b0JBR3pCLHFCQUFNLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQUE7O29CQUF6QixTQUF5QixDQUFDOzs7O29CQUUxQixnQkFBTSxDQUFDLEtBQUssQ0FBQyxLQUFHLENBQUMsQ0FBQzs7O29CQUdoQixJQUFJLEdBQVEsWUFBWSxJQUFJLFdBQVcsQ0FBQzt5QkFFMUMsSUFBSSxFQUFKLHdCQUFJO29CQUNKLElBQUksQ0FBQyxZQUFZLEVBQUU7d0JBQ2YsWUFBWSxHQUFHLGtCQUFPLEVBQUUsQ0FBQztxQkFDNUI7b0JBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRTt3QkFDZCxXQUFXLEdBQUcsa0JBQU8sRUFBRSxDQUFDO3FCQUMzQjtvQkFHaUIscUJBQU0sU0FBUyxDQUFDLElBQUksQ0FBQzs0QkFDbkMsTUFBTSxFQUFFLElBQUk7NEJBQ1osTUFBTSxFQUFFLElBQUk7NEJBQ1osTUFBTSxFQUFFLElBQUk7eUJBQ2YsQ0FBQyxFQUFBOztvQkFKSSxTQUFTLEdBQUcsU0FJaEI7b0JBRUYsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLFlBQVksRUFBRSwyQ0FBeUIsQ0FBQzt3QkFDM0UsV0FBVyxFQUFFLE9BQU8sQ0FBQyxXQUFXO3dCQUNoQyxZQUFZLEVBQUUsT0FBTyxDQUFDLFlBQVk7d0JBQ2xDLFdBQVcsYUFBQTtxQkFDZCxDQUFDLENBQUMsQ0FBQzs7d0JBR1Isc0JBQU87d0JBQ0gsV0FBVyxFQUFFLFNBQVMsYUFBVCxTQUFTLHVCQUFULFNBQVMsQ0FBRSxFQUFFO3dCQUMxQixJQUFJLEVBQUU7Ozs7d0NBQ0YsZ0JBQU0sQ0FBQyxLQUFLLENBQUMseUJBQXVCLFNBQVMsQ0FBQyxFQUFJLENBQUMsQ0FBQzt3Q0FDcEQscUJBQU0sU0FBUyxDQUFDLElBQUksRUFBRSxFQUFBOzt3Q0FBdEIsU0FBc0IsQ0FBQzt3Q0FDdkIsVUFBVSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7Ozs7NkJBQ25DO3dCQUVELElBQUksRUFBRTs7Ozt3Q0FDRixnQkFBTSxDQUFDLEtBQUssQ0FBQyx3QkFBc0IsU0FBUyxDQUFDLEVBQUksQ0FBQyxDQUFDO3dDQUNuRCxxQkFBTSxTQUFTLENBQUMsSUFBSSxFQUFFLEVBQUE7O3dDQUF0QixTQUFzQixDQUFDO3dDQUN2QixVQUFVLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Ozs2QkFDbkM7d0JBRUQsSUFBSSxFQUFFLFVBQU8sR0FBRyxFQUFFLEVBQXFJO2dDQUFySSxxQkFBbUksRUFBRSxLQUFBLEVBQW5JLFdBQVEsRUFBUixHQUFHLG1CQUFHLEVBQUUsS0FBQSxFQUFFLFdBQVEsRUFBUixHQUFHLG1CQUFHLEVBQUUsS0FBQSxFQUFFLG9CQUE2QixFQUE3QixZQUFZLG1CQUFHLE9BQU8sQ0FBQyxNQUFNLEtBQUEsRUFBRSxtQkFBNEIsRUFBNUIsV0FBVyxtQkFBRyxPQUFPLENBQUMsTUFBTSxLQUFBLEVBQUUsZUFBZSxFQUFmLE9BQU8sbUJBQUcsS0FBSyxLQUFBLEVBQUUsZUFBWSxFQUFaLE9BQU8sbUJBQUcsRUFBRSxLQUFBLEVBQUUsYUFBWSxFQUFaLEtBQUssbUJBQUcsSUFBSSxLQUFBOzs7Ozs7NENBQ3RJLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDOzRDQUU3QixPQUFPLEdBQVE7Z0RBQ2pCLEdBQUcsRUFBRSxVQUFVLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDO2dEQUNyQyxHQUFHLEVBQUUsS0FBSztnREFDVixXQUFXLEVBQUUsS0FBSztnREFDbEIsWUFBWSxFQUFFLElBQUk7Z0RBQ2xCLFlBQVksRUFBRSxJQUFJO2dEQUNsQixVQUFVLEVBQUUsR0FBRzs2Q0FDbEIsQ0FBQzs0Q0FDRixJQUFJLEdBQUcsS0FBSyxFQUFFLEVBQUU7Z0RBQ1osT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7NkNBQ3JCOzRDQUVELGNBQWM7NENBQ2QsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsdUJBQXFCLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUcsQ0FBQyxDQUFDOzRDQUV6RCxxQkFBTSxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFBOzs0Q0FBcEMsSUFBSSxHQUFHLFNBQTZCOzRDQUUzQixxQkFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLE9BQUEsRUFBRSxDQUFDLEVBQUE7OzRDQUFsRCxNQUFNLEdBQUcsU0FBeUM7NENBRXhELDBEQUEwRDs0Q0FDMUQscUJBQU0sSUFBSSxPQUFPLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxFQUF2QixDQUF1QixDQUFDLEVBQUE7OzRDQURyRCwwREFBMEQ7NENBQzFELFNBQXFELENBQUM7NENBRXRELElBQUksS0FBSyxLQUFLLElBQUksRUFBRTtnREFDaEIsMEJBQTBCLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDOzZDQUM3Qzs0Q0FFRCxJQUFJLENBQUMsWUFBWSxFQUFFO2dEQUNmLFlBQVksR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDOzZDQUNqQzs0Q0FFRCxJQUFJLENBQUMsV0FBVyxFQUFFO2dEQUNkLFdBQVcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDOzZDQUNoQzs0Q0FFRCxJQUFJLE9BQU8sRUFBRTtnREFDVCxTQUFTLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsWUFBWSxFQUFFLFdBQVcsQ0FBQyxDQUFDOzZDQUNsRTtpREFBTTtnREFDSCxTQUFTLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsa0JBQU8sRUFBRSxFQUFFLFdBQVcsQ0FBQyxDQUFDOzZDQUMvRDs0Q0FFTSxxQkFBTSxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUE7Z0RBQTlCLHNCQUFPLFNBQXVCLEVBQUM7Ozs7eUJBQ2xDO3FCQUNKLEVBQUM7Ozs7Q0FDTDtBQWpHRCx3Q0FpR0M7QUFFRCxTQUFlLFdBQVcsQ0FBQyxJQUFJOzs7O3dCQUNwQixxQkFBTSxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO3dCQUNyQywyREFBMkQ7d0JBQzNELG9DQUFvQzt3QkFDcEMsU0FBUyxpQkFBaUI7NEJBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHLEVBQUUsSUFBSTtnQ0FDbkIsSUFBSSxJQUFJLGFBQUosSUFBSSx1QkFBSixJQUFJLENBQUUsT0FBTyxFQUFFO29DQUNmLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxHQUFHLENBQUMsQ0FBQztvQ0FDbkMsT0FBTztpQ0FDVjtnQ0FDRCxJQUFJLEdBQUcsRUFBRTtvQ0FDTCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7aUNBQ2Y7cUNBQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLENBQUMsRUFBRTtvQ0FDNUIsTUFBTSxDQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSwwQkFBcUIsSUFBSSxDQUFDLFFBQVUsQ0FBQyxDQUFDO2lDQUNoRjtxQ0FBTTtvQ0FDSCxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2lDQUMxQjs0QkFDTCxDQUFDLENBQUMsQ0FBQzt3QkFDUCxDQUFDO3dCQUNELGlCQUFpQixFQUFFLENBQUM7b0JBQ3hCLENBQUMsQ0FBQyxFQUFBO3dCQW5CRixzQkFBTyxTQW1CTCxFQUFDOzs7O0NBQ047QUFFRCxTQUFzQixHQUFHLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLE9BQVk7SUFBWix3QkFBQSxFQUFBLFlBQVk7Ozs7O3dCQUU1QyxxQkFBTSxZQUFZLENBQUMsSUFBSSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsT0FBTyxDQUFDLEVBQUE7O29CQUFwRixLQUF3QixTQUE0RCxFQUFsRixTQUFTLGVBQUEsRUFBRSxNQUFNLFlBQUE7b0JBRXpCLDBCQUEwQixDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFJM0IscUJBQU0sU0FBUyxDQUFDLElBQUksRUFBRSxFQUFBOztvQkFBL0IsTUFBTSxHQUFHLFNBQXNCO29CQUVyQyxVQUFVLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDaEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDdkIsc0JBQU8sTUFBTSxFQUFDOzs7O0NBQ2pCO0FBYkQsa0JBYUM7QUFFRCxTQUFzQixhQUFhLENBQUMsU0FBb0I7Ozs7OztvQkFDaEQsTUFBTSxHQUFRLElBQUksQ0FBQyxPQUFPLENBQUMsNkJBQTJCLFNBQVMsQ0FBQyxFQUFJLENBQUMsQ0FBQzs7OztvQkFFdEUscUJBQU0sU0FBUyxDQUFDLElBQUksRUFBRSxFQUFBOztvQkFBdEIsU0FBc0IsQ0FBQztvQkFDdkIsTUFBTSxDQUFDLE9BQU8sQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDOzs7O29CQUUxQyxNQUFNLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7b0JBQzdDLGdCQUFNLENBQUMsS0FBSyxDQUFDLHlCQUF1QixTQUFTLENBQUMsRUFBRSx3QkFBbUIsR0FBRyxDQUFDLENBQUM7b0JBQ3hFLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLDRCQUEwQixTQUFTLENBQUMsRUFBSSxDQUFDLENBQUM7Ozs7b0JBRTVELHFCQUFNLFNBQVMsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7b0JBQXRCLFNBQXNCLENBQUM7b0JBQ3ZCLE1BQU0sQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQzs7OztvQkFFekMsTUFBTSxDQUFDLElBQUksQ0FBQyx1REFBdUQsQ0FBQyxDQUFDO29CQUNyRSxnQkFBTSxDQUFDLEtBQUssQ0FBQywyQkFBeUIsU0FBUyxDQUFDLEVBQUUsd0JBQW1CLEdBQUcsQ0FBQyxDQUFDOzs7Ozs7O0NBR3JGO0FBakJELHNDQWlCQyJ9