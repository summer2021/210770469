"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processMakeHelperFunctionErr = void 0;
function processMakeHelperFunctionErr(e) {
    if ((e === null || e === void 0 ? void 0 : e.code) === 'OSSInvalidRequest' && (e === null || e === void 0 ? void 0 : e.message.includes("event source 'oss' returned error: Cannot specify overlapping prefix and suffix with same event type"))) {
        throw new Error("Oss trigger can not be deployed under helper function because it already exists online.Please remove it and exec 's clean' to remove the deployed helper resource. Then try 's setup' again.");
    }
    throw e;
}
exports.processMakeHelperFunctionErr = processMakeHelperFunctionErr;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3JQcm9jZXNzb3IuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbGliL2Vycm9yUHJvY2Vzc29yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUNBLFNBQWdCLDRCQUE0QixDQUFDLENBQU07SUFDL0MsSUFBSSxDQUFBLENBQUMsYUFBRCxDQUFDLHVCQUFELENBQUMsQ0FBRSxJQUFJLE1BQUssbUJBQW1CLEtBQUksQ0FBQyxhQUFELENBQUMsdUJBQUQsQ0FBQyxDQUFFLE9BQU8sQ0FBQyxRQUFRLENBQUMsc0dBQXNHLEVBQUMsRUFBRTtRQUNoSyxNQUFNLElBQUksS0FBSyxDQUFDLDhMQUE4TCxDQUFDLENBQUM7S0FDbk47SUFDRCxNQUFNLENBQUMsQ0FBQztBQUNaLENBQUM7QUFMRCxvRUFLQyJ9