"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateVscodeDebugConfig = exports.generateDebugEnv = exports.generateDockerDebugOpts = exports.getDebugOptions = void 0;
var ip = __importStar(require("ip"));
var fs = __importStar(require("fs-extra"));
var path = __importStar(require("path"));
var logger_1 = __importDefault(require("../../common/logger"));
var IDE_PYCHARM = 'pycharm';
function getDebugOptions(argsData) {
    var debugPort = argsData['debug-port'];
    logger_1.default.debug("debugPort: " + debugPort);
    var debugIde = argsData['config'];
    logger_1.default.debug("debugIde: " + debugIde);
    var debuggerPath = argsData['debugger-path'];
    logger_1.default.debug("debuggerPath: " + debuggerPath);
    var debugArgs = argsData['debug-args'];
    logger_1.default.debug("debugArgs: " + JSON.stringify(debugArgs));
    return {
        debugPort: debugPort,
        debugIde: debugIde,
        debuggerPath: debuggerPath,
        debugArgs: debugArgs
    };
}
exports.getDebugOptions = getDebugOptions;
function generateDockerDebugOpts(runtime, debugPort, debugIde) {
    var _a, _b;
    var exposedPort = debugPort + "/tcp";
    if (debugIde === IDE_PYCHARM) {
        if (runtime !== 'python2.7' && runtime !== 'python3') {
            throw new Error(IDE_PYCHARM + " debug config only support for runtime [python2.7, python3]");
        }
        else {
            return {};
        }
    }
    else if (runtime === 'php7.2') {
        return {};
    }
    else {
        return {
            ExposedPorts: (_a = {},
                _a[exposedPort] = {},
                _a),
            HostConfig: {
                PortBindings: (_b = {},
                    _b[exposedPort] = [
                        {
                            'HostIp': '',
                            'HostPort': "" + debugPort
                        }
                    ],
                    _b)
            }
        };
    }
}
exports.generateDockerDebugOpts = generateDockerDebugOpts;
function generateDebugEnv(runtime, debugPort, debugIde) {
    var remoteIp = ip.address();
    switch (runtime) {
        case 'nodejs12':
        case 'nodejs10':
        case 'nodejs8':
            return { 'DEBUG_OPTIONS': "--inspect=0.0.0.0:" + debugPort };
        case 'nodejs6':
            return { 'DEBUG_OPTIONS': "--debug-brk=" + debugPort };
        case 'python2.7':
        case 'python3':
            if (debugIde === IDE_PYCHARM) {
                return {};
            }
            return { 'DEBUG_OPTIONS': "-m debugpy --listen 0.0.0.0:" + debugPort };
        case 'java8':
        case 'java11':
            return { 'DEBUG_OPTIONS': "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,quiet=y,address=" + debugPort };
        case 'php7.2':
            console.log("using remote_ip " + remoteIp);
            return { 'XDEBUG_CONFIG': "remote_enable=1 remote_autostart=1 remote_port=" + debugPort + " remote_host=" + remoteIp };
        case 'dotnetcore2.1':
            return { 'DEBUG_OPTIONS': 'true' };
        default:
            throw new Error('could not found runtime.');
    }
}
exports.generateDebugEnv = generateDebugEnv;
function generateVscodeDebugConfig(serviceName, functionName, runtime, codePath, debugPort) {
    return __awaiter(this, void 0, void 0, function () {
        var stats;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, fs.lstat(codePath)];
                case 1:
                    stats = _a.sent();
                    if (!stats.isDirectory()) {
                        codePath = path.dirname(codePath);
                    }
                    switch (runtime) {
                        case 'nodejs6':
                            return [2 /*return*/, {
                                    'version': '0.2.0',
                                    'configurations': [
                                        {
                                            'name': "fc/" + serviceName + "/" + functionName,
                                            'type': 'node',
                                            'request': 'attach',
                                            'address': 'localhost',
                                            'port': debugPort,
                                            'localRoot': "" + codePath,
                                            'remoteRoot': '/code',
                                            'protocol': 'legacy',
                                            'stopOnEntry': false
                                        }
                                    ]
                                }];
                        case 'nodejs12':
                        case 'nodejs10':
                        case 'nodejs8':
                            return [2 /*return*/, {
                                    'version': '0.2.0',
                                    'configurations': [
                                        {
                                            'name': "fc/" + serviceName + "/" + functionName,
                                            'type': 'node',
                                            'request': 'attach',
                                            'address': 'localhost',
                                            'port': debugPort,
                                            'localRoot': "" + codePath,
                                            'remoteRoot': '/code',
                                            'protocol': 'inspector',
                                            'stopOnEntry': false
                                        }
                                    ]
                                }];
                        case 'python3':
                        case 'python2.7':
                            return [2 /*return*/, {
                                    'version': '0.2.0',
                                    'configurations': [
                                        {
                                            'name': "Python: fc/" + serviceName + "/" + functionName,
                                            'type': 'python',
                                            'request': 'attach',
                                            'connect': {
                                                'host': 'localhost',
                                                'port': debugPort
                                            },
                                            'pathMappings': [
                                                {
                                                    'localRoot': "" + codePath,
                                                    'remoteRoot': '/code'
                                                }
                                            ]
                                        }
                                    ]
                                }];
                        case 'java8':
                        case 'java11':
                            return [2 /*return*/, {
                                    'version': '0.2.0',
                                    'configurations': [
                                        {
                                            'name': "fc/" + serviceName + "/" + functionName,
                                            'type': 'java',
                                            'request': 'attach',
                                            'hostName': 'localhost',
                                            'port': debugPort
                                        }
                                    ]
                                }];
                        case 'php7.2':
                            return [2 /*return*/, {
                                    'version': '0.2.0',
                                    'configurations': [
                                        {
                                            'name': "fc/" + serviceName + "/" + functionName,
                                            'type': 'php',
                                            'request': 'launch',
                                            'port': debugPort,
                                            'stopOnEntry': false,
                                            'pathMappings': {
                                                '/code': "" + codePath
                                            },
                                            'ignore': [
                                                '/var/fc/runtime/**'
                                            ]
                                        }
                                    ]
                                }];
                        case 'dotnetcore2.1':
                            return [2 /*return*/, {
                                    'version': '0.2.0',
                                    'configurations': [
                                        {
                                            'name': "fc/" + serviceName + "/" + functionName,
                                            'type': 'coreclr',
                                            'request': 'attach',
                                            'processName': 'dotnet',
                                            'pipeTransport': {
                                                'pipeProgram': 'sh',
                                                'pipeArgs': [
                                                    '-c',
                                                    "docker exec -i $(docker ps -q -f publish=" + debugPort + ") ${debuggerCommand}"
                                                ],
                                                'debuggerPath': '/vsdbg/vsdbg',
                                                'pipeCwd': '${workspaceFolder}'
                                            },
                                            'windows': {
                                                'pipeTransport': {
                                                    'pipeProgram': 'powershell',
                                                    'pipeArgs': [
                                                        '-c',
                                                        "docker exec -i $(docker ps -q -f publish=" + debugPort + ") ${debuggerCommand}"
                                                    ],
                                                    'debuggerPath': '/vsdbg/vsdbg',
                                                    'pipeCwd': '${workspaceFolder}'
                                                }
                                            },
                                            'sourceFileMap': {
                                                '/code': codePath
                                            }
                                        }
                                    ]
                                }];
                        default:
                            break;
                    }
                    logger_1.default.debug('CodePath: ' + codePath);
                    return [2 /*return*/];
            }
        });
    });
}
exports.generateVscodeDebugConfig = generateVscodeDebugConfig;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVidWcuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi9zcmMvbGliL2xvY2FsLWludm9rZS9kZWJ1Zy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEscUNBQXlCO0FBQ3pCLDJDQUErQjtBQUMvQix5Q0FBNkI7QUFDN0IsK0RBQXlDO0FBQ3pDLElBQU0sV0FBVyxHQUFXLFNBQVMsQ0FBQztBQUV0QyxTQUFnQixlQUFlLENBQUMsUUFBYTtJQUMzQyxJQUFNLFNBQVMsR0FBVyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDakQsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsZ0JBQWMsU0FBVyxDQUFDLENBQUM7SUFDeEMsSUFBTSxRQUFRLEdBQVEsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3pDLGdCQUFNLENBQUMsS0FBSyxDQUFDLGVBQWEsUUFBVSxDQUFDLENBQUM7SUFDdEMsSUFBTSxZQUFZLEdBQVcsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3ZELGdCQUFNLENBQUMsS0FBSyxDQUFDLG1CQUFpQixZQUFjLENBQUMsQ0FBQztJQUM5QyxJQUFNLFNBQVMsR0FBUSxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDOUMsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsZ0JBQWMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUcsQ0FBQyxDQUFDO0lBRXhELE9BQU87UUFDTCxTQUFTLFdBQUE7UUFDVCxRQUFRLFVBQUE7UUFDUixZQUFZLGNBQUE7UUFDWixTQUFTLFdBQUE7S0FDVixDQUFBO0FBQ0gsQ0FBQztBQWhCRCwwQ0FnQkM7QUFFRCxTQUFnQix1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVE7O0lBQ2xFLElBQU0sV0FBVyxHQUFNLFNBQVMsU0FBTSxDQUFDO0lBRXZDLElBQUksUUFBUSxLQUFLLFdBQVcsRUFBRTtRQUM1QixJQUFJLE9BQU8sS0FBSyxXQUFXLElBQUksT0FBTyxLQUFLLFNBQVMsRUFBRTtZQUNwRCxNQUFNLElBQUksS0FBSyxDQUFJLFdBQVcsZ0VBQTZELENBQUMsQ0FBQztTQUM5RjthQUFNO1lBQ0wsT0FBTyxFQUFFLENBQUM7U0FDWDtLQUNGO1NBQU0sSUFBSSxPQUFPLEtBQUssUUFBUSxFQUFFO1FBQy9CLE9BQU8sRUFBRSxDQUFDO0tBQ1g7U0FBTTtRQUNMLE9BQU87WUFDTCxZQUFZO2dCQUNWLEdBQUMsV0FBVyxJQUFHLEVBQUU7bUJBQ2xCO1lBQ0QsVUFBVSxFQUFFO2dCQUNWLFlBQVk7b0JBQ1YsR0FBQyxXQUFXLElBQUc7d0JBQ2I7NEJBQ0UsUUFBUSxFQUFFLEVBQUU7NEJBQ1osVUFBVSxFQUFFLEtBQUcsU0FBVzt5QkFDM0I7cUJBQ0Y7dUJBQ0Y7YUFDRjtTQUNGLENBQUM7S0FDSDtBQUNILENBQUM7QUE1QkQsMERBNEJDO0FBQ0QsU0FBZ0IsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxRQUFRO0lBQzNELElBQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUU5QixRQUFRLE9BQU8sRUFBRTtRQUNqQixLQUFLLFVBQVUsQ0FBQztRQUNoQixLQUFLLFVBQVUsQ0FBQztRQUNoQixLQUFLLFNBQVM7WUFDWixPQUFPLEVBQUUsZUFBZSxFQUFFLHVCQUFxQixTQUFXLEVBQUUsQ0FBQztRQUMvRCxLQUFLLFNBQVM7WUFDWixPQUFPLEVBQUUsZUFBZSxFQUFFLGlCQUFlLFNBQVcsRUFBRSxDQUFDO1FBQ3pELEtBQUssV0FBVyxDQUFDO1FBQ2pCLEtBQUssU0FBUztZQUNaLElBQUksUUFBUSxLQUFLLFdBQVcsRUFBRTtnQkFDNUIsT0FBTyxFQUFFLENBQUM7YUFDWDtZQUNELE9BQU8sRUFBRSxlQUFlLEVBQUcsaUNBQStCLFNBQVcsRUFBQyxDQUFBO1FBQ3hFLEtBQUssT0FBTyxDQUFDO1FBQ2IsS0FBSyxRQUFRO1lBQ1gsT0FBTyxFQUFFLGVBQWUsRUFBRSwyRUFBeUUsU0FBVyxFQUFFLENBQUM7UUFDbkgsS0FBSyxRQUFRO1lBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBbUIsUUFBVSxDQUFDLENBQUM7WUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxvREFBa0QsU0FBUyxxQkFBZ0IsUUFBVSxFQUFFLENBQUM7UUFDcEgsS0FBSyxlQUFlO1lBQ2xCLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxFQUFFLENBQUM7UUFDckM7WUFDRSxNQUFNLElBQUksS0FBSyxDQUFDLDBCQUEwQixDQUFDLENBQUM7S0FDN0M7QUFDSCxDQUFDO0FBM0JELDRDQTJCQztBQUdELFNBQXNCLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxTQUFTOzs7Ozt3QkFFdkYscUJBQU0sRUFBRSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsRUFBQTs7b0JBQWhDLEtBQUssR0FBRyxTQUF3QjtvQkFFdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsRUFBRTt3QkFDeEIsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7cUJBQ25DO29CQUVELFFBQVEsT0FBTyxFQUFFO3dCQUNmLEtBQUssU0FBUzs0QkFDWixzQkFBTztvQ0FDTCxTQUFTLEVBQUUsT0FBTztvQ0FDbEIsZ0JBQWdCLEVBQUU7d0NBQ2hCOzRDQUNFLE1BQU0sRUFBRSxRQUFNLFdBQVcsU0FBSSxZQUFjOzRDQUMzQyxNQUFNLEVBQUUsTUFBTTs0Q0FDZCxTQUFTLEVBQUUsUUFBUTs0Q0FDbkIsU0FBUyxFQUFFLFdBQVc7NENBQ3RCLE1BQU0sRUFBRSxTQUFTOzRDQUNqQixXQUFXLEVBQUUsS0FBRyxRQUFVOzRDQUMxQixZQUFZLEVBQUUsT0FBTzs0Q0FDckIsVUFBVSxFQUFFLFFBQVE7NENBQ3BCLGFBQWEsRUFBRSxLQUFLO3lDQUNyQjtxQ0FDRjtpQ0FDRixFQUFDO3dCQUNKLEtBQUssVUFBVSxDQUFDO3dCQUNoQixLQUFLLFVBQVUsQ0FBQzt3QkFDaEIsS0FBSyxTQUFTOzRCQUNaLHNCQUFPO29DQUNMLFNBQVMsRUFBRSxPQUFPO29DQUNsQixnQkFBZ0IsRUFBRTt3Q0FDaEI7NENBQ0UsTUFBTSxFQUFFLFFBQU0sV0FBVyxTQUFJLFlBQWM7NENBQzNDLE1BQU0sRUFBRSxNQUFNOzRDQUNkLFNBQVMsRUFBRSxRQUFROzRDQUNuQixTQUFTLEVBQUUsV0FBVzs0Q0FDdEIsTUFBTSxFQUFFLFNBQVM7NENBQ2pCLFdBQVcsRUFBRSxLQUFHLFFBQVU7NENBQzFCLFlBQVksRUFBRSxPQUFPOzRDQUNyQixVQUFVLEVBQUUsV0FBVzs0Q0FDdkIsYUFBYSxFQUFFLEtBQUs7eUNBQ3JCO3FDQUNGO2lDQUNGLEVBQUM7d0JBQ0osS0FBSyxTQUFTLENBQUM7d0JBQ2YsS0FBSyxXQUFXOzRCQUNkLHNCQUFPO29DQUNMLFNBQVMsRUFBRSxPQUFPO29DQUNsQixnQkFBZ0IsRUFBRTt3Q0FDaEI7NENBQ0UsTUFBTSxFQUFFLGdCQUFjLFdBQVcsU0FBSSxZQUFjOzRDQUNuRCxNQUFNLEVBQUUsUUFBUTs0Q0FDaEIsU0FBUyxFQUFFLFFBQVE7NENBQ25CLFNBQVMsRUFBRTtnREFDVCxNQUFNLEVBQUUsV0FBVztnREFDbkIsTUFBTSxFQUFFLFNBQVM7NkNBQ2xCOzRDQUNELGNBQWMsRUFBRTtnREFDZDtvREFDRSxXQUFXLEVBQUUsS0FBRyxRQUFVO29EQUMxQixZQUFZLEVBQUUsT0FBTztpREFDdEI7NkNBQ0Y7eUNBQ0Y7cUNBQ0Y7aUNBQ0YsRUFBQzt3QkFDSixLQUFLLE9BQU8sQ0FBQzt3QkFDYixLQUFLLFFBQVE7NEJBQ1gsc0JBQU87b0NBQ0wsU0FBUyxFQUFFLE9BQU87b0NBQ2xCLGdCQUFnQixFQUFFO3dDQUNoQjs0Q0FDRSxNQUFNLEVBQUUsUUFBTSxXQUFXLFNBQUksWUFBYzs0Q0FDM0MsTUFBTSxFQUFFLE1BQU07NENBQ2QsU0FBUyxFQUFFLFFBQVE7NENBQ25CLFVBQVUsRUFBRSxXQUFXOzRDQUN2QixNQUFNLEVBQUUsU0FBUzt5Q0FDbEI7cUNBQ0Y7aUNBQ0YsRUFBQzt3QkFDSixLQUFLLFFBQVE7NEJBQ1gsc0JBQU87b0NBQ0wsU0FBUyxFQUFFLE9BQU87b0NBQ2xCLGdCQUFnQixFQUFFO3dDQUNoQjs0Q0FDRSxNQUFNLEVBQUUsUUFBTSxXQUFXLFNBQUksWUFBYzs0Q0FDM0MsTUFBTSxFQUFFLEtBQUs7NENBQ2IsU0FBUyxFQUFFLFFBQVE7NENBQ25CLE1BQU0sRUFBRSxTQUFTOzRDQUNqQixhQUFhLEVBQUUsS0FBSzs0Q0FDcEIsY0FBYyxFQUFFO2dEQUNkLE9BQU8sRUFBRSxLQUFHLFFBQVU7NkNBQ3ZCOzRDQUNELFFBQVEsRUFBRTtnREFDUixvQkFBb0I7NkNBQ3JCO3lDQUNGO3FDQUNGO2lDQUNGLEVBQUM7d0JBQ0osS0FBSyxlQUFlOzRCQUNsQixzQkFBTztvQ0FDTCxTQUFTLEVBQUUsT0FBTztvQ0FDbEIsZ0JBQWdCLEVBQUU7d0NBQ2hCOzRDQUNFLE1BQU0sRUFBRSxRQUFNLFdBQVcsU0FBSSxZQUFjOzRDQUMzQyxNQUFNLEVBQUUsU0FBUzs0Q0FDakIsU0FBUyxFQUFFLFFBQVE7NENBQ25CLGFBQWEsRUFBRSxRQUFROzRDQUN2QixlQUFlLEVBQUU7Z0RBQ2YsYUFBYSxFQUFFLElBQUk7Z0RBQ25CLFVBQVUsRUFBRTtvREFDVixJQUFJO29EQUNKLDhDQUE0QyxTQUFTLHlCQUF1QjtpREFDN0U7Z0RBQ0QsY0FBYyxFQUFFLGNBQWM7Z0RBQzlCLFNBQVMsRUFBRSxvQkFBb0I7NkNBQ2hDOzRDQUNELFNBQVMsRUFBRTtnREFDVCxlQUFlLEVBQUU7b0RBQ2YsYUFBYSxFQUFFLFlBQVk7b0RBQzNCLFVBQVUsRUFBRTt3REFDVixJQUFJO3dEQUNKLDhDQUE0QyxTQUFTLHlCQUF1QjtxREFDN0U7b0RBQ0QsY0FBYyxFQUFFLGNBQWM7b0RBQzlCLFNBQVMsRUFBRSxvQkFBb0I7aURBQ2hDOzZDQUNGOzRDQUNELGVBQWUsRUFBRTtnREFDZixPQUFPLEVBQUUsUUFBUTs2Q0FDbEI7eUNBQ0Y7cUNBRUY7aUNBQ0YsRUFBQzt3QkFDSjs0QkFDRSxNQUFNO3FCQUNUO29CQUVELGdCQUFNLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsQ0FBQzs7Ozs7Q0FDdkM7QUE3SUQsOERBNklDIn0=