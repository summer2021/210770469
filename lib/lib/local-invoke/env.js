"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addEnv = exports.resolveLibPathsFromLdConf = exports.addInstallTargetEnv = void 0;
var _ = __importStar(require("lodash"));
var definition_1 = require("../definition");
var nas_1 = require("./nas");
var path = __importStar(require("path"));
var fs = __importStar(require("fs-extra"));
var file_1 = require("../utils/file");
var sysLibs = [
    '/usr/local/lib',
    '/usr/lib',
    '/usr/lib/x86_64-linux-gnu',
    '/usr/lib64',
    '/lib',
    '/lib/x86_64-linux-gnu',
    '/python/lib/python2.7/site-packages',
    '/python/lib/python3.6/site-packages'
];
var fcLibs = [
    '/code',
    '/code/lib',
    '/usr/local/lib'
];
var sysPaths = [
    '/usr/local/bin',
    '/usr/local/sbin',
    '/usr/bin',
    '/usr/sbin',
    '/sbin',
    '/bin'
];
var fcPaths = [
    '/code',
    '/code/node_modules/.bin'
];
var pythonPaths = [
    '/python/lib/python2.7/site-packages',
    '/python/lib/python3.6/site-packages'
];
var funPaths = [
    '/python/bin',
    '/node_modules/.bin'
];
// This method is only used for fun install target attribue.
//
// In order to be able to use the dependencies installed in the previous step,
// such as the model serving example, fun need to configure the corresponding environment variables
// so that the install process can go through.
//
// However, if the target specifies a directory other than nas, code,
// it will not be successful by deploy, so this is an implicit rule.
//
// For fun-install, don't need to care about this rule because it has Context information for nas.
// Fun will set all environment variables before fun-install is executed.
function addInstallTargetEnv(envVars, targets) {
    var envs = Object.assign({}, envVars);
    if (!targets) {
        return envs;
    }
    _.forEach(targets, function (target) {
        var containerPath = target.containerPath;
        var prefix = containerPath;
        var targetPathonPath = pythonPaths.map(function (p) { return "" + prefix + p; }).join(':');
        if (envs['PYTHONPATH']) {
            envs['PYTHONPATH'] = envs['PYTHONPATH'] + ":" + targetPathonPath;
        }
        else {
            envs['PYTHONPATH'] = targetPathonPath;
        }
    });
    return envs;
}
exports.addInstallTargetEnv = addInstallTargetEnv;
function resolveLibPathsFromLdConf(baseDir, codeUri) {
    return __awaiter(this, void 0, void 0, function () {
        var envs, confdPath, stats, libPaths;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    envs = {};
                    if (!codeUri) {
                        return [2 /*return*/, envs];
                    }
                    confdPath = path.resolve(baseDir, codeUri, '.fun/root/etc/ld.so.conf.d');
                    return [4 /*yield*/, fs.pathExists(confdPath)];
                case 1:
                    if (!(_a.sent())) {
                        return [2 /*return*/, envs];
                    }
                    return [4 /*yield*/, fs.lstat(confdPath)];
                case 2:
                    stats = _a.sent();
                    if (stats.isFile()) {
                        return [2 /*return*/, envs];
                    }
                    return [4 /*yield*/, resolveLibPaths(confdPath)];
                case 3:
                    libPaths = _a.sent();
                    if (!_.isEmpty(libPaths)) {
                        envs['LD_LIBRARY_PATH'] = libPaths.map(function (path) { return "/code/.fun/root" + path; }).join(':');
                    }
                    return [2 /*return*/, envs];
            }
        });
    });
}
exports.resolveLibPathsFromLdConf = resolveLibPathsFromLdConf;
function resolveLibPaths(confdPath) {
    return __awaiter(this, void 0, void 0, function () {
        var confLines;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!fs.existsSync(confdPath)) {
                        return [2 /*return*/, []];
                    }
                    return [4 /*yield*/, Promise.all(fs.readdirSync(confdPath, 'utf-8')
                            .filter(function (f) { return f.endsWith('.conf'); })
                            .map(function (f) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, file_1.readLines(path.join(confdPath, f))];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        }); }); }))];
                case 1:
                    confLines = _a.sent();
                    return [2 /*return*/, _.flatten(confLines)
                            .reduce(function (lines, line) {
                            // remove the first and last blanks and leave only the middle
                            var found = line.match(/^\s*(\/.*)\s*$/);
                            if (found && found[1].startsWith('/')) {
                                lines.push(found[1]);
                            }
                            return lines;
                        }, [])];
            }
        });
    });
}
function addEnv(envVars, nasConfig) {
    var envs = Object.assign({}, envVars);
    var prefix = '/code/.s';
    envs['LD_LIBRARY_PATH'] = generateLibPath(envs, prefix);
    envs['PATH'] = generatePath(envs, prefix);
    envs['NODE_PATH'] = generateNodePaths(envs, '/code');
    var defaultPythonPath = prefix + "/python";
    if (!envs['PYTHONUSERBASE']) {
        envs['PYTHONUSERBASE'] = defaultPythonPath;
    }
    if (nasConfig) {
        return appendNasEnvs(envs, nasConfig);
    }
    return envs;
}
exports.addEnv = addEnv;
function appendNasEnvs(envs, nasConfig) {
    var isNasAuto = definition_1.isAutoConfig(nasConfig);
    var nasEnvs;
    if (isNasAuto) {
        var mountDir = '/mnt/auto';
        nasEnvs = appendNasMountPointEnv(envs, mountDir);
    }
    else {
        if (typeof nasConfig !== 'string') {
            var mountPoints = nasConfig.mountPoints;
            _.forEach(mountPoints, function (mountPoint) {
                var mountDir = nas_1.resolveMountPoint(mountPoint).mountDir;
                nasEnvs = appendNasMountPointEnv(envs, mountDir);
            });
        }
    }
    return nasEnvs;
}
function appendNasMountPointEnv(envs, mountDir) {
    envs['LD_LIBRARY_PATH'] = generateLibPath(envs, mountDir);
    envs['PATH'] = generatePath(envs, mountDir);
    envs['NODE_PATH'] = generateNodePaths(envs, mountDir);
    var nasPythonPaths = generatePythonPaths(mountDir);
    if (envs['PYTHONPATH']) {
        envs['PYTHONPATH'] = envs['PYTHONPATH'] + ":" + nasPythonPaths;
    }
    else {
        envs['PYTHONPATH'] = nasPythonPaths;
    }
    // TODO: add other runtime envs
    return envs;
}
function generatePythonPaths(prefix) {
    return pythonPaths.map(function (p) { return "" + prefix + p; }).join(':');
}
function generateNodePaths(envs, prefix) {
    var defaultPath = "/usr/local/lib/node_modules";
    var customPath = prefix + "/node_modules";
    var path;
    if (envs['NODE_PATH']) {
        path = envs['NODE_PATH'] + ":" + customPath + ":" + defaultPath;
    }
    else {
        path = customPath + ":" + defaultPath;
    }
    return duplicateRemoval(path);
}
function generateLibPath(envs, prefix) {
    var libPath = _.union(sysLibs.map(function (p) { return prefix + "/root" + p; }), fcLibs).join(':');
    if (envs['LD_LIBRARY_PATH']) {
        libPath = envs['LD_LIBRARY_PATH'] + ":" + libPath;
    }
    return duplicateRemoval(libPath);
}
function generatePath(envs, prefix) {
    var path = _.union(sysPaths.map(function (p) { return prefix + "/root" + p; }), fcPaths, funPaths.map(function (p) { return "" + prefix + p; }), sysPaths).join(':');
    if (envs['PATH']) {
        path = envs['PATH'] + ":" + path;
    }
    return duplicateRemoval(path);
}
function duplicateRemoval(str) {
    var spliceValue = str.split(':');
    return _.union(spliceValue).join(':');
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW52LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9sb2NhbC1pbnZva2UvZW52LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQSx3Q0FBNEI7QUFDNUIsNENBQTZDO0FBQzdDLDZCQUEwQztBQUMxQyx5Q0FBNkI7QUFDN0IsMkNBQStCO0FBQy9CLHNDQUEwQztBQUUxQyxJQUFNLE9BQU8sR0FBYTtJQUN4QixnQkFBZ0I7SUFDaEIsVUFBVTtJQUNWLDJCQUEyQjtJQUMzQixZQUFZO0lBQ1osTUFBTTtJQUNOLHVCQUF1QjtJQUN2QixxQ0FBcUM7SUFDckMscUNBQXFDO0NBQ3RDLENBQUM7QUFFRixJQUFNLE1BQU0sR0FBYTtJQUN2QixPQUFPO0lBQ1AsV0FBVztJQUNYLGdCQUFnQjtDQUNqQixDQUFDO0FBRUYsSUFBTSxRQUFRLEdBQWE7SUFDekIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixVQUFVO0lBQ1YsV0FBVztJQUNYLE9BQU87SUFDUCxNQUFNO0NBQ1AsQ0FBQztBQUVGLElBQU0sT0FBTyxHQUFhO0lBQ3hCLE9BQU87SUFDUCx5QkFBeUI7Q0FDMUIsQ0FBQztBQUdGLElBQU0sV0FBVyxHQUFhO0lBQzVCLHFDQUFxQztJQUNyQyxxQ0FBcUM7Q0FDdEMsQ0FBQztBQUVGLElBQU0sUUFBUSxHQUFhO0lBQ3pCLGFBQWE7SUFDYixvQkFBb0I7Q0FDckIsQ0FBQztBQUVGLDREQUE0RDtBQUM1RCxFQUFFO0FBQ0YsOEVBQThFO0FBQzlFLG1HQUFtRztBQUNuRyw4Q0FBOEM7QUFDOUMsRUFBRTtBQUNGLHFFQUFxRTtBQUNyRSxvRUFBb0U7QUFDcEUsRUFBRTtBQUNGLGtHQUFrRztBQUNsRyx5RUFBeUU7QUFDekUsU0FBZ0IsbUJBQW1CLENBQUMsT0FBTyxFQUFFLE9BQU87SUFDbEQsSUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFFeEMsSUFBSSxDQUFDLE9BQU8sRUFBRTtRQUFFLE9BQU8sSUFBSSxDQUFDO0tBQUU7SUFFOUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsVUFBQyxNQUFNO1FBRWhCLElBQUEsYUFBYSxHQUFLLE1BQU0sY0FBWCxDQUFZO1FBRWpDLElBQU0sTUFBTSxHQUFHLGFBQWEsQ0FBQztRQUU3QixJQUFNLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxLQUFHLE1BQU0sR0FBRyxDQUFHLEVBQWYsQ0FBZSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXpFLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBTSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQUksZ0JBQWtCLENBQUM7U0FDbEU7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQztTQUN2QztJQUNILENBQUMsQ0FBQyxDQUFDO0lBRUgsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDO0FBckJELGtEQXFCQztBQUVELFNBQXNCLHlCQUF5QixDQUFDLE9BQWUsRUFBRSxPQUFlOzs7Ozs7b0JBQ3hFLElBQUksR0FBRyxFQUFFLENBQUM7b0JBQ2hCLElBQUksQ0FBQyxPQUFPLEVBQUU7d0JBQUUsc0JBQU8sSUFBSSxFQUFDO3FCQUFFO29CQUN4QixTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFLDRCQUE0QixDQUFDLENBQUM7b0JBRXpFLHFCQUFNLEVBQUUsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEVBQUE7O29CQUFwQyxJQUFJLENBQUUsQ0FBQSxTQUE4QixDQUFBLEVBQUU7d0JBQUUsc0JBQU8sSUFBSSxFQUFDO3FCQUFFO29CQUV4QyxxQkFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxFQUFBOztvQkFBakMsS0FBSyxHQUFHLFNBQXlCO29CQUV2QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUUsRUFBRTt3QkFBRSxzQkFBTyxJQUFJLEVBQUM7cUJBQUU7b0JBRWQscUJBQU0sZUFBZSxDQUFDLFNBQVMsQ0FBQyxFQUFBOztvQkFBaEQsUUFBUSxHQUFRLFNBQWdDO29CQUV0RCxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTt3QkFFeEIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLG9CQUFrQixJQUFNLEVBQXhCLENBQXdCLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ3BGO29CQUNELHNCQUFPLElBQUksRUFBQzs7OztDQUNiO0FBbEJELDhEQWtCQztBQUVELFNBQWUsZUFBZSxDQUFDLFNBQVM7Ozs7Ozs7b0JBQ3RDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUM3QixzQkFBTyxFQUFFLEVBQUM7cUJBQ1g7b0JBQ2lCLHFCQUFNLE9BQU8sQ0FBQyxHQUFHLENBQ2pDLEVBQUUsQ0FBQyxXQUFXLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQzs2QkFDL0IsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBbkIsQ0FBbUIsQ0FBQzs2QkFDaEMsR0FBRyxDQUFDLFVBQU0sQ0FBQzs7d0NBQUkscUJBQU0sZ0JBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFBO3dDQUF4QyxzQkFBQSxTQUF3QyxFQUFBOztpQ0FBQSxDQUFDLENBQUMsRUFBQTs7b0JBSHhELFNBQVMsR0FBRyxTQUc0QztvQkFFOUQsc0JBQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7NkJBQ3hCLE1BQU0sQ0FBQyxVQUFDLEtBQVUsRUFBRSxJQUFTOzRCQUM1Qiw2REFBNkQ7NEJBQzdELElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzs0QkFDM0MsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQ0FFckMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs2QkFDdEI7NEJBQ0QsT0FBTyxLQUFLLENBQUM7d0JBQ2YsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFDOzs7O0NBQ1Y7QUFHRCxTQUFnQixNQUFNLENBQUMsT0FBWSxFQUFFLFNBQThCO0lBQ2pFLElBQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBRXhDLElBQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQztJQUUxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxlQUFlLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3hELElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxZQUFZLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFFckQsSUFBTSxpQkFBaUIsR0FBTSxNQUFNLFlBQVMsQ0FBQztJQUM3QyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7UUFDM0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsaUJBQWlCLENBQUM7S0FDNUM7SUFFRCxJQUFJLFNBQVMsRUFBRTtRQUNiLE9BQU8sYUFBYSxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztLQUN2QztJQUVELE9BQU8sSUFBSSxDQUFDO0FBQ2QsQ0FBQztBQW5CRCx3QkFtQkM7QUFFRCxTQUFTLGFBQWEsQ0FBQyxJQUFJLEVBQUUsU0FBNkI7SUFDeEQsSUFBTSxTQUFTLEdBQUcseUJBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMxQyxJQUFJLE9BQU8sQ0FBQztJQUNaLElBQUksU0FBUyxFQUFFO1FBQ2IsSUFBTSxRQUFRLEdBQUcsV0FBVyxDQUFDO1FBQzdCLE9BQU8sR0FBRyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7S0FDbEQ7U0FBTTtRQUNMLElBQUksT0FBTyxTQUFTLEtBQUssUUFBUSxFQUFFO1lBQ2pDLElBQU0sV0FBVyxHQUFpQixTQUFTLENBQUMsV0FBVyxDQUFDO1lBQ3hELENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFVBQUMsVUFBVTtnQkFDeEIsSUFBQSxRQUFRLEdBQUssdUJBQWlCLENBQUMsVUFBVSxDQUFDLFNBQWxDLENBQW1DO2dCQUNuRCxPQUFPLEdBQUcsc0JBQXNCLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ25ELENBQUMsQ0FBQyxDQUFDO1NBQ0o7S0FDRjtJQUNELE9BQU8sT0FBTyxDQUFDO0FBQ2pCLENBQUM7QUFFRCxTQUFTLHNCQUFzQixDQUFDLElBQUksRUFBRSxRQUFRO0lBRTVDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLGVBQWUsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDMUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLFlBQVksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDNUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLGlCQUFpQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztJQUV0RCxJQUFNLGNBQWMsR0FBRyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUVyRCxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTtRQUN0QixJQUFJLENBQUMsWUFBWSxDQUFDLEdBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFJLGNBQWdCLENBQUM7S0FDaEU7U0FBTTtRQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxjQUFjLENBQUM7S0FDckM7SUFFRCwrQkFBK0I7SUFDL0IsT0FBTyxJQUFJLENBQUM7QUFDZCxDQUFDO0FBRUQsU0FBUyxtQkFBbUIsQ0FBQyxNQUFNO0lBQ2pDLE9BQU8sV0FBVyxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUcsTUFBTSxHQUFHLENBQUcsRUFBZixDQUFlLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDekQsQ0FBQztBQUVELFNBQVMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLE1BQU07SUFDckMsSUFBTSxXQUFXLEdBQUcsNkJBQTZCLENBQUM7SUFDbEQsSUFBTSxVQUFVLEdBQU0sTUFBTSxrQkFBZSxDQUFDO0lBRTVDLElBQUksSUFBSSxDQUFDO0lBQ1QsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUU7UUFDckIsSUFBSSxHQUFNLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBSSxVQUFVLFNBQUksV0FBYSxDQUFDO0tBQzVEO1NBQU07UUFDTCxJQUFJLEdBQU0sVUFBVSxTQUFJLFdBQWEsQ0FBQztLQUN2QztJQUNELE9BQU8sZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDaEMsQ0FBQztBQUVELFNBQVMsZUFBZSxDQUFDLElBQUksRUFBRSxNQUFNO0lBQ25DLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBRyxNQUFNLGFBQVEsQ0FBRyxFQUFwQixDQUFvQixDQUFDLEVBQ3RDLE1BQU0sQ0FDUCxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUVaLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUU7UUFDM0IsT0FBTyxHQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFJLE9BQVMsQ0FBQztLQUNuRDtJQUNELE9BQU8sZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDbkMsQ0FBQztBQUVELFNBQVMsWUFBWSxDQUFDLElBQUksRUFBRSxNQUFNO0lBQ2hDLElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQ2hCLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBRyxNQUFNLGFBQVEsQ0FBRyxFQUFwQixDQUFvQixDQUFDLEVBQ3ZDLE9BQU8sRUFDUCxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsS0FBRyxNQUFNLEdBQUcsQ0FBRyxFQUFmLENBQWUsQ0FBQyxFQUNsQyxRQUFRLENBQ1QsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFFWixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtRQUNoQixJQUFJLEdBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFJLElBQU0sQ0FBQztLQUNsQztJQUVELE9BQU8sZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDaEMsQ0FBQztBQUdELFNBQVMsZ0JBQWdCLENBQUMsR0FBRztJQUMzQixJQUFNLFdBQVcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLE9BQU8sQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDeEMsQ0FBQyJ9