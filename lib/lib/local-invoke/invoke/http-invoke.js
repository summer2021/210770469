'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var node_watch_1 = __importDefault(require("node-watch"));
var rimraf = __importStar(require("rimraf"));
var ignore_1 = require("../ignore");
var invoke_1 = __importDefault(require("./invoke"));
var docker = __importStar(require("../../docker/docker"));
// import * as dockerOpts from '../../docker/docker-opts';
var dockerode_1 = __importDefault(require("dockerode"));
var runtime_1 = require("../../utils/runtime");
var logger_1 = __importDefault(require("../../../common/logger"));
// import { genProxyContainerName } from '../../definition';
var time_1 = require("../../utils/time");
var process_1 = require("../../utils/process");
var retry_1 = require("../../retry");
var core = __importStar(require("@serverless-devs/core"));
var stdout_formatter_1 = __importDefault(require("../../component/stdout-formatter"));
// import { genStateId, getInvokeContainerIdFromState, unsetInvokeContainerId } from '../../utils/state';
var state_1 = require("../../utils/state");
// import devnull from 'dev-null';
var dockerClient = new dockerode_1.default();
var HttpInvoke = /** @class */ (function (_super) {
    __extends(HttpInvoke, _super);
    function HttpInvoke(tunnelService, sessionId, creds, region, baseDir, serviceConfig, functionConfig, triggerConfig, debugPort, debugIde, tmpDir, debuggerPath, debugArgs, nasBaseDir) {
        var _this = _super.call(this, tunnelService, sessionId, creds, region, baseDir, serviceConfig, functionConfig, triggerConfig, debugPort, debugIde, tmpDir, debuggerPath, debugArgs, nasBaseDir) || this;
        process_1.setSigint();
        // exit container, when use ctrl + c
        process.on('SIGINT', function () { return __awaiter(_this, void 0, void 0, function () {
            var stopVm, e_1, killVm, e_2;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (this.watcher) {
                            this.watcher.close();
                        }
                        this.cleanUnzippedCodeDir();
                        if (!this.runner) return [3 /*break*/, 10];
                        stopVm = core.spinner('Received canncel request, stopping running function container...');
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 3, , 8]);
                        return [4 /*yield*/, this.runner.stop()];
                    case 2:
                        _b.sent();
                        stopVm.succeed('Stop function container successfully');
                        return [3 /*break*/, 8];
                    case 3:
                        e_1 = _b.sent();
                        stopVm.fail('Stop function container failed.');
                        logger_1.default.debug("Stop function container failed, error: " + e_1);
                        killVm = core.spinner('Killing old container...');
                        _b.label = 4;
                    case 4:
                        _b.trys.push([4, 6, , 7]);
                        return [4 /*yield*/, this.runner.kill()];
                    case 5:
                        _b.sent();
                        killVm.succeed('Kill function container successfully');
                        return [3 /*break*/, 7];
                    case 6:
                        e_2 = _b.sent();
                        killVm.fail('Kill old container failed, please kill it manually.');
                        logger_1.default.debug("Kill function container failed, error: " + e_2);
                        return [3 /*break*/, 7];
                    case 7: return [3 /*break*/, 8];
                    case 8: return [4 /*yield*/, state_1.unsetInvokeContainerId((_a = this.creds) === null || _a === void 0 ? void 0 : _a.AccountID, this.region, this.serviceName, this.functionName)];
                    case 9:
                        _b.sent();
                        this.runner = null;
                        _b.label = 10;
                    case 10:
                        if (!this.tunnelService) return [3 /*break*/, 12];
                        return [4 /*yield*/, this.tunnelService.clean()];
                    case 11:
                        _b.sent();
                        _b.label = 12;
                    case 12:
                        // 修复 windows 环境下 Ctrl C 后容器退出，但是程序会 block 住的问题
                        if (process.platform === 'win32') {
                            process.exit(0);
                        }
                        return [2 /*return*/];
                }
            });
        }); });
        return _this;
    }
    HttpInvoke.prototype._disableRunner = function (evt, name) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var oldRunner, tmpCodeDir, stopVm, e_3, killVm, e_4;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!this.runner) {
                            return [2 /*return*/];
                        }
                        logger_1.default.info("Detect code changes, file is " + name + ", event is " + evt + ", auto reloading...");
                        oldRunner = this.runner;
                        tmpCodeDir = this.unzippedCodeDir;
                        this.runner = null;
                        // this.containerName = docker.generateRamdomContainerName();
                        return [4 /*yield*/, this.init()];
                    case 1:
                        // this.containerName = docker.generateRamdomContainerName();
                        _b.sent();
                        return [4 /*yield*/, time_1.sleep(500)];
                    case 2:
                        _b.sent();
                        stopVm = core.spinner('Reloading success, stopping old function container...');
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 5, , 10]);
                        return [4 /*yield*/, oldRunner.stop()];
                    case 4:
                        _b.sent();
                        stopVm.succeed('Stop old function container successfully');
                        return [3 /*break*/, 10];
                    case 5:
                        e_3 = _b.sent();
                        stopVm.fail("Stop function container failed.");
                        logger_1.default.debug("Stop function container failed, error: " + e_3);
                        killVm = core.spinner('Killing old container...');
                        _b.label = 6;
                    case 6:
                        _b.trys.push([6, 8, , 9]);
                        return [4 /*yield*/, oldRunner.kill()];
                    case 7:
                        _b.sent();
                        killVm.succeed('Kill old container successfully');
                        return [3 /*break*/, 9];
                    case 8:
                        e_4 = _b.sent();
                        killVm.fail('Kill old container failed, please kill it manually.');
                        logger_1.default.debug("Kill function container failed, error: " + e_4);
                        return [3 /*break*/, 9];
                    case 9: return [3 /*break*/, 10];
                    case 10: return [4 /*yield*/, state_1.unsetInvokeContainerId((_a = this.creds) === null || _a === void 0 ? void 0 : _a.AccountID, this.region, this.serviceName, this.functionName)];
                    case 11:
                        _b.sent();
                        if (tmpCodeDir) {
                            rimraf.sync(tmpCodeDir);
                            logger_1.default.info("Clean tmp code dir " + tmpCodeDir + " successfully.\n");
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    // async _startRunner(): Promise<void> {
    //   // 检查代理容器是否存在，若不存在则 ca 启动失败
    //   if (this.tunnelService && !(await this.tunnelService.checkIfProxyContainerRunning())) {
    //     console.log();
    //     logger.error('\nFunction container starts failed.Start cleaning now.');
    //     if (this.tunnelService) {
    //       await this.tunnelService.clean();
    //     }
    //     throw new Error("Function container starts failed because proxy container is not running, please retry 'setup' method.");
    //   }
    //   const envs = await docker.generateDockerEnvs(
    //     this.creds,
    //     this.region,
    //     this.baseDir,
    //     this.serviceName,
    //     this.serviceConfig,
    //     this.functionName,
    //     this.functionConfig,
    //     this.debugPort,
    //     null,
    //     this.nasConfig,
    //     this.debugIde,
    //     this.debugArgs,
    //   );
    //   const cmd = docker.generateDockerCmd(this.runtime, true, this.functionConfig);
    //   const proxyContainerName: string = genProxyContainerName(this.sessionId);
    //   this.containerName = docker.generateRamdomContainerName();
    //   const opts = await dockerOpts.generateLocalStartOpts(proxyContainerName, this.runtime, this.containerName, this.mounts, cmd, envs, {
    //     debugPort: this.debugPort,
    //     dockerUser: this.dockerUser,
    //     imageName: this.imageName,
    //   });
    //   this.runner = await docker.startContainer(opts, process.stdout, process.stderr, {
    //     serviceName: this.serviceName,
    //     functionName: this.functionName,
    //   });
    //   await this.saveInvokeContainerId();
    //   const isDebug: boolean = process.env?.temp_params?.includes('--debug');
    //   // check if server is up
    //   if (!isCustomContainerRuntime(this.functionConfig?.runtime)) {
    //     logger.info(StdoutFormatter.stdoutFormatter.check('server in function container', 'is up.'));
    //     await promiseRetry(async (retry: any, times: number): Promise<any> => {
    //       try {
    //         if (!this.runner) {
    //           throw new Error('Function container is closed, exit!');
    //         }
    //         await sleep(1000);
    //         const caPort: number = this.functionConfig?.caPort || 9000;
    //         const res: any = await this.runner.exec(['curl', `127.0.0.1:${caPort}`], {
    //           outputStream: isDebug ? process.stdout : devnull(),
    //           errorStream: isDebug ? process.stderr : devnull(),
    //         });
    //         if (res === 0) {
    //           logger.info(`Server in function container is up!`);
    //           return;
    //         }
    //         logger.debug(`Server is not up. Result is :${res}`);
    //         logger.info(StdoutFormatter.stdoutFormatter.retry('checking server in function container', 'is up', '', times));
    //         retry(res);
    //       } catch (ex) {
    //         if (ex.message === 'Function container is closed, exit!') {
    //           throw ex;
    //         }
    //         logger.debug(`Checking server in function container failed, error: ${ex}`);
    //         logger.info(StdoutFormatter.stdoutFormatter.retry('checking server in function container', 'is up', '', times));
    //         retry(ex);
    //       }
    //     }, 20);
    //   }
    // }
    // private async saveInvokeContainerId(): Promise<void> {
    //   await setKVInState(
    //     'invokeContainerId',
    //     this.runner?.containerId,
    //     genStateId(this.creds?.AccountID, this.region, this.serviceName, this.functionName),
    //   );
    // }
    HttpInvoke.prototype.initWatch = function () {
        return __awaiter(this, void 0, void 0, function () {
            var ign_1, stopMutex_1, startMutex_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(!this.watcher && !runtime_1.isCustomContainerRuntime(this.runtime))) return [3 /*break*/, 2];
                        return [4 /*yield*/, ignore_1.isIgnored(this.baseDir)];
                    case 1:
                        ign_1 = _a.sent();
                        stopMutex_1 = false;
                        startMutex_1 = false;
                        this.watcher = node_watch_1.default(this.codeUri, {
                            recursive: true,
                            persistent: false,
                            filter: function (f) {
                                return ign_1 && !ign_1(f);
                            },
                        }, function (evt, name) { return __awaiter(_this, void 0, void 0, function () {
                            var maxStartMutexOccupiedTime, startMutexOccupiedTime, e_5;
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (stopMutex_1) {
                                            return [2 /*return*/];
                                        }
                                        maxStartMutexOccupiedTime = 300;
                                        startMutexOccupiedTime = 0;
                                        if (!startMutex_1) return [3 /*break*/, 7];
                                        _a.label = 1;
                                    case 1:
                                        if (!startMutex_1) return [3 /*break*/, 7];
                                        // TODO: 设置等待时间上限
                                        if (stopMutex_1) {
                                            return [2 /*return*/];
                                        }
                                        return [4 /*yield*/, time_1.sleep(3000)];
                                    case 2:
                                        _a.sent();
                                        startMutexOccupiedTime = startMutexOccupiedTime + 3;
                                        if (!(startMutexOccupiedTime > maxStartMutexOccupiedTime)) return [3 /*break*/, 6];
                                        return [4 /*yield*/, this.clean()];
                                    case 3:
                                        _a.sent();
                                        if (!this.tunnelService) return [3 /*break*/, 5];
                                        return [4 /*yield*/, this.tunnelService.clean()];
                                    case 4:
                                        _a.sent();
                                        _a.label = 5;
                                    case 5: throw new Error("Restart function container timeout after 300s!Please check if docker runs normally, then runs 'clean' method and try 'setup' again.");
                                    case 6: return [3 /*break*/, 1];
                                    case 7:
                                        stopMutex_1 = true;
                                        _a.label = 8;
                                    case 8:
                                        _a.trys.push([8, 12, , 13]);
                                        if (!this.runner) return [3 /*break*/, 10];
                                        return [4 /*yield*/, this._disableRunner(evt, name)];
                                    case 9:
                                        _a.sent();
                                        return [3 /*break*/, 11];
                                    case 10:
                                        logger_1.default.debug('detect code changes, but no runner found, starting....');
                                        _a.label = 11;
                                    case 11: return [3 /*break*/, 13];
                                    case 12:
                                        e_5 = _a.sent();
                                        logger_1.default.warning("Stop function container failed, please stop it manually.");
                                        logger_1.default.debug("Stop function container error: " + e_5);
                                        return [3 /*break*/, 13];
                                    case 13:
                                        startMutex_1 = true;
                                        stopMutex_1 = false;
                                        logger_1.default.info('Detecting code changes and Restarting funtion container...');
                                        return [4 /*yield*/, retry_1.promiseRetry(function (retry, times) { return __awaiter(_this, void 0, void 0, function () {
                                                var ex_1;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            _a.trys.push([0, 1, , 3]);
                                                            // TODO: restart the remote function
                                                            // await this._startRunner();
                                                            logger_1.default.info("Hot fix is not supported now! Please try 'setup' and 'invoke' again.");
                                                            return [2 /*return*/];
                                                        case 1:
                                                            ex_1 = _a.sent();
                                                            logger_1.default.error('Restart function container failed!');
                                                            if (ex_1 === null || ex_1 === void 0 ? void 0 : ex_1.message.includes('Function container starts failed because proxy container is not running')) {
                                                                throw ex_1;
                                                            }
                                                            logger_1.default.debug("Restart function container failed, error is: \n" + ex_1);
                                                            logger_1.default.info(stdout_formatter_1.default.stdoutFormatter.retry('function container', 'restart', '', times));
                                                            return [4 /*yield*/, time_1.sleep(100)];
                                                        case 2:
                                                            _a.sent();
                                                            retry(ex_1);
                                                            return [3 /*break*/, 3];
                                                        case 3: return [2 /*return*/];
                                                    }
                                                });
                                            }); })];
                                    case 14:
                                        _a.sent();
                                        startMutex_1 = false;
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        this.watcher.on('error', function (err) {
                            throw err;
                        });
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        });
    };
    HttpInvoke.prototype.initAndStartRunner = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.init()];
                    case 1:
                        _a.sent();
                        // await this._startRunner();
                        return [4 /*yield*/, this.initWatch()];
                    case 2:
                        // await this._startRunner();
                        _a.sent();
                        return [4 /*yield*/, this.setDebugIdeConfig()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HttpInvoke.prototype.clean = function () {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var invokeContainerId, container;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: return [4 /*yield*/, state_1.getInvokeContainerIdFromState((_a = this.creds) === null || _a === void 0 ? void 0 : _a.AccountID, this.region, this.serviceName, this.functionName)];
                    case 1:
                        invokeContainerId = _c.sent();
                        return [4 /*yield*/, dockerClient.getContainer(invokeContainerId)];
                    case 2:
                        container = _c.sent();
                        return [4 /*yield*/, docker.stopContainer(container)];
                    case 3:
                        _c.sent();
                        return [4 /*yield*/, state_1.unsetInvokeContainerId((_b = this.creds) === null || _b === void 0 ? void 0 : _b.AccountID, this.region, this.serviceName, this.functionName)];
                    case 4:
                        _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return HttpInvoke;
}(invoke_1.default));
exports.default = HttpInvoke;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHR0cC1pbnZva2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9zcmMvbGliL2xvY2FsLWludm9rZS9pbnZva2UvaHR0cC1pbnZva2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsWUFBWSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBRWIsMERBQStCO0FBSy9CLDZDQUFpQztBQUNqQyxvQ0FBZ0Q7QUFDaEQsb0RBQThCO0FBQzlCLDBEQUE4QztBQUM5QywwREFBMEQ7QUFDMUQsd0RBQStCO0FBRS9CLCtDQUErRDtBQUMvRCxrRUFBNEM7QUFFNUMsNERBQTREO0FBQzVELHlDQUF5QztBQUN6QywrQ0FBZ0Q7QUFHaEQscUNBQTJDO0FBQzNDLDBEQUE4QztBQUM5QyxzRkFBK0Q7QUFFL0QseUdBQXlHO0FBQ3pHLDJDQUEwRjtBQUMxRixrQ0FBa0M7QUFFbEMsSUFBTSxZQUFZLEdBQVEsSUFBSSxtQkFBTSxFQUFFLENBQUM7QUFFdkM7SUFBd0MsOEJBQU07SUFJNUMsb0JBQ0UsYUFBNEIsRUFDNUIsU0FBaUIsRUFDakIsS0FBbUIsRUFDbkIsTUFBYyxFQUNkLE9BQWUsRUFDZixhQUE0QixFQUM1QixjQUE4QixFQUM5QixhQUE2QixFQUM3QixTQUFrQixFQUNsQixRQUFjLEVBQ2QsTUFBZSxFQUNmLFlBQWtCLEVBQ2xCLFNBQWUsRUFDZixVQUFtQjtRQWRyQixZQWdCRSxrQkFDRSxhQUFhLEVBQ2IsU0FBUyxFQUNULEtBQUssRUFDTCxNQUFNLEVBQ04sT0FBTyxFQUNQLGFBQWEsRUFDYixjQUFjLEVBQ2QsYUFBYSxFQUNiLFNBQVMsRUFDVCxRQUFRLEVBQ1IsTUFBTSxFQUNOLFlBQVksRUFDWixTQUFTLEVBQ1QsVUFBVSxDQUNYLFNBb0NGO1FBbkNDLG1CQUFTLEVBQUUsQ0FBQztRQUNaLG9DQUFvQztRQUNwQyxPQUFPLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRTs7Ozs7O3dCQUNuQixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7NEJBQ2hCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7eUJBQ3RCO3dCQUNELElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOzZCQUN4QixJQUFJLENBQUMsTUFBTSxFQUFYLHlCQUFXO3dCQUNQLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLGtFQUFrRSxDQUFDLENBQUM7Ozs7d0JBRTlGLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLEVBQUE7O3dCQUF4QixTQUF3QixDQUFDO3dCQUN6QixNQUFNLENBQUMsT0FBTyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7Ozs7d0JBRXZELE1BQU0sQ0FBQyxJQUFJLENBQUMsaUNBQWlDLENBQUMsQ0FBQzt3QkFDL0MsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsNENBQTBDLEdBQUcsQ0FBQyxDQUFDO3dCQUN0RCxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQywwQkFBMEIsQ0FBQyxDQUFDOzs7O3dCQUV0RCxxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxFQUFBOzt3QkFBeEIsU0FBd0IsQ0FBQzt3QkFDekIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFDOzs7O3dCQUV2RCxNQUFNLENBQUMsSUFBSSxDQUFDLHFEQUFxRCxDQUFDLENBQUM7d0JBQ25FLGdCQUFNLENBQUMsS0FBSyxDQUFDLDRDQUEwQyxHQUFHLENBQUMsQ0FBQzs7OzRCQUdoRSxxQkFBTSw4QkFBc0IsT0FBQyxJQUFJLENBQUMsS0FBSywwQ0FBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBQTs7d0JBQXJHLFNBQXFHLENBQUM7d0JBQ3RHLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDOzs7NkJBRWpCLElBQUksQ0FBQyxhQUFhLEVBQWxCLHlCQUFrQjt3QkFDcEIscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsRUFBQTs7d0JBQWhDLFNBQWdDLENBQUM7Ozt3QkFFbkMsK0NBQStDO3dCQUMvQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEtBQUssT0FBTyxFQUFFOzRCQUNoQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUNqQjs7OzthQUNGLENBQUMsQ0FBQzs7SUFDTCxDQUFDO0lBRUssbUNBQWMsR0FBcEIsVUFBcUIsR0FBRyxFQUFFLElBQUk7Ozs7Ozs7d0JBQzVCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFOzRCQUNoQixzQkFBTzt5QkFDUjt3QkFFRCxnQkFBTSxDQUFDLElBQUksQ0FBQyxrQ0FBZ0MsSUFBSSxtQkFBYyxHQUFHLHdCQUFxQixDQUFDLENBQUM7d0JBQ2xGLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO3dCQUMxQixVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQzt3QkFDdEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7d0JBQ25CLDZEQUE2RDt3QkFDN0QscUJBQU0sSUFBSSxDQUFDLElBQUksRUFBRSxFQUFBOzt3QkFEakIsNkRBQTZEO3dCQUM3RCxTQUFpQixDQUFDO3dCQUNsQixxQkFBTSxZQUFLLENBQUMsR0FBRyxDQUFDLEVBQUE7O3dCQUFoQixTQUFnQixDQUFDO3dCQUNYLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLHVEQUF1RCxDQUFDLENBQUM7Ozs7d0JBRW5GLHFCQUFNLFNBQVMsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7d0JBQ3ZCLE1BQU0sQ0FBQyxPQUFPLENBQUMsMENBQTBDLENBQUMsQ0FBQzs7Ozt3QkFFM0QsTUFBTSxDQUFDLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO3dCQUMvQyxnQkFBTSxDQUFDLEtBQUssQ0FBQyw0Q0FBMEMsR0FBRyxDQUFDLENBQUM7d0JBQ3RELE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUM7Ozs7d0JBRXRELHFCQUFNLFNBQVMsQ0FBQyxJQUFJLEVBQUUsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7d0JBQ3ZCLE1BQU0sQ0FBQyxPQUFPLENBQUMsaUNBQWlDLENBQUMsQ0FBQzs7Ozt3QkFFbEQsTUFBTSxDQUFDLElBQUksQ0FBQyxxREFBcUQsQ0FBQyxDQUFDO3dCQUNuRSxnQkFBTSxDQUFDLEtBQUssQ0FBQyw0Q0FBMEMsR0FBRyxDQUFDLENBQUM7Ozs2QkFJaEUscUJBQU0sOEJBQXNCLE9BQUMsSUFBSSxDQUFDLEtBQUssMENBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUE7O3dCQUFyRyxTQUFxRyxDQUFDO3dCQUN0RyxJQUFJLFVBQVUsRUFBRTs0QkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzRCQUN4QixnQkFBTSxDQUFDLElBQUksQ0FBQyx3QkFBc0IsVUFBVSxxQkFBa0IsQ0FBQyxDQUFDO3lCQUNqRTs7Ozs7S0FDRjtJQUVELHdDQUF3QztJQUN4QyxnQ0FBZ0M7SUFDaEMsNEZBQTRGO0lBQzVGLHFCQUFxQjtJQUNyQiw4RUFBOEU7SUFDOUUsZ0NBQWdDO0lBQ2hDLDBDQUEwQztJQUMxQyxRQUFRO0lBQ1IsZ0lBQWdJO0lBQ2hJLE1BQU07SUFDTixrREFBa0Q7SUFDbEQsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixvQkFBb0I7SUFDcEIsd0JBQXdCO0lBQ3hCLDBCQUEwQjtJQUMxQix5QkFBeUI7SUFDekIsMkJBQTJCO0lBQzNCLHNCQUFzQjtJQUN0QixZQUFZO0lBQ1osc0JBQXNCO0lBQ3RCLHFCQUFxQjtJQUNyQixzQkFBc0I7SUFDdEIsT0FBTztJQUNQLG1GQUFtRjtJQUNuRiw4RUFBOEU7SUFDOUUsK0RBQStEO0lBQy9ELHlJQUF5STtJQUN6SSxpQ0FBaUM7SUFDakMsbUNBQW1DO0lBQ25DLGlDQUFpQztJQUNqQyxRQUFRO0lBQ1Isc0ZBQXNGO0lBQ3RGLHFDQUFxQztJQUNyQyx1Q0FBdUM7SUFDdkMsUUFBUTtJQUNSLHdDQUF3QztJQUN4Qyw0RUFBNEU7SUFDNUUsNkJBQTZCO0lBQzdCLG1FQUFtRTtJQUNuRSxvR0FBb0c7SUFDcEcsOEVBQThFO0lBQzlFLGNBQWM7SUFDZCw4QkFBOEI7SUFDOUIsb0VBQW9FO0lBQ3BFLFlBQVk7SUFDWiw2QkFBNkI7SUFDN0Isc0VBQXNFO0lBQ3RFLHFGQUFxRjtJQUNyRixnRUFBZ0U7SUFDaEUsK0RBQStEO0lBQy9ELGNBQWM7SUFDZCwyQkFBMkI7SUFDM0IsZ0VBQWdFO0lBQ2hFLG9CQUFvQjtJQUNwQixZQUFZO0lBQ1osK0RBQStEO0lBQy9ELDJIQUEySDtJQUMzSCxzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHNFQUFzRTtJQUN0RSxzQkFBc0I7SUFDdEIsWUFBWTtJQUNaLHNGQUFzRjtJQUN0RiwySEFBMkg7SUFDM0gscUJBQXFCO0lBQ3JCLFVBQVU7SUFDVixjQUFjO0lBQ2QsTUFBTTtJQUNOLElBQUk7SUFFSix5REFBeUQ7SUFDekQsd0JBQXdCO0lBQ3hCLDJCQUEyQjtJQUMzQixnQ0FBZ0M7SUFDaEMsMkZBQTJGO0lBQzNGLE9BQU87SUFDUCxJQUFJO0lBRUUsOEJBQVMsR0FBZjs7Ozs7Ozs2QkFDTSxDQUFBLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLGtDQUF3QixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQSxFQUF4RCx3QkFBd0Q7d0JBRTlDLHFCQUFNLGtCQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFBOzt3QkFBaEMsUUFBTSxTQUEwQjt3QkFJbEMsY0FBWSxLQUFLLENBQUM7d0JBQ2xCLGVBQWEsS0FBSyxDQUFDO3dCQUN2QixJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFLLENBQ2xCLElBQUksQ0FBQyxPQUFPLEVBQ1o7NEJBQ0UsU0FBUyxFQUFFLElBQUk7NEJBQ2YsVUFBVSxFQUFFLEtBQUs7NEJBQ2pCLE1BQU0sRUFBRSxVQUFDLENBQUM7Z0NBQ1IsT0FBTyxLQUFHLElBQUksQ0FBQyxLQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3hCLENBQUM7eUJBQ0YsRUFDRCxVQUFPLEdBQUcsRUFBRSxJQUFJOzs7Ozs7d0NBQ2QsSUFBSSxXQUFTLEVBQUU7NENBQ2Isc0JBQU87eUNBQ1I7d0NBRUsseUJBQXlCLEdBQVcsR0FBRyxDQUFDO3dDQUMxQyxzQkFBc0IsR0FBVyxDQUFDLENBQUM7NkNBQ25DLFlBQVUsRUFBVix3QkFBVTs7OzZDQUVMLFlBQVU7d0NBQ2YsaUJBQWlCO3dDQUNqQixJQUFJLFdBQVMsRUFBRTs0Q0FDYixzQkFBTzt5Q0FDUjt3Q0FDRCxxQkFBTSxZQUFLLENBQUMsSUFBSSxDQUFDLEVBQUE7O3dDQUFqQixTQUFpQixDQUFDO3dDQUNsQixzQkFBc0IsR0FBRyxzQkFBc0IsR0FBRyxDQUFDLENBQUM7NkNBQ2hELENBQUEsc0JBQXNCLEdBQUcseUJBQXlCLENBQUEsRUFBbEQsd0JBQWtEO3dDQUNwRCxxQkFBTSxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUE7O3dDQUFsQixTQUFrQixDQUFDOzZDQUNmLElBQUksQ0FBQyxhQUFhLEVBQWxCLHdCQUFrQjt3Q0FDcEIscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsRUFBQTs7d0NBQWhDLFNBQWdDLENBQUM7OzRDQUVuQyxNQUFNLElBQUksS0FBSyxDQUNiLHFJQUFxSSxDQUN0SSxDQUFDOzs7d0NBSVIsV0FBUyxHQUFHLElBQUksQ0FBQzs7Ozs2Q0FFWCxJQUFJLENBQUMsTUFBTSxFQUFYLHlCQUFXO3dDQUNiLHFCQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxFQUFBOzt3Q0FBcEMsU0FBb0MsQ0FBQzs7O3dDQUVyQyxnQkFBTSxDQUFDLEtBQUssQ0FBQyx3REFBd0QsQ0FBQyxDQUFDOzs7Ozt3Q0FHekUsZ0JBQU0sQ0FBQyxPQUFPLENBQUMsMERBQTBELENBQUMsQ0FBQzt3Q0FDM0UsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsb0NBQWtDLEdBQUcsQ0FBQyxDQUFDOzs7d0NBR3RELFlBQVUsR0FBRyxJQUFJLENBQUM7d0NBQ2xCLFdBQVMsR0FBRyxLQUFLLENBQUM7d0NBQ2xCLGdCQUFNLENBQUMsSUFBSSxDQUFDLDREQUE0RCxDQUFDLENBQUM7d0NBQzFFLHFCQUFNLG9CQUFZLENBQUMsVUFBTyxLQUFVLEVBQUUsS0FBYTs7Ozs7OzREQUUvQyxvQ0FBb0M7NERBQ3BDLDZCQUE2Qjs0REFDN0IsZ0JBQU0sQ0FBQyxJQUFJLENBQUMsc0VBQXNFLENBQUMsQ0FBQzs0REFDcEYsc0JBQU87Ozs0REFFUCxnQkFBTSxDQUFDLEtBQUssQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDOzREQUNuRCxJQUFJLElBQUUsYUFBRixJQUFFLHVCQUFGLElBQUUsQ0FBRSxPQUFPLENBQUMsUUFBUSxDQUFDLHlFQUF5RSxHQUFHO2dFQUNuRyxNQUFNLElBQUUsQ0FBQzs2REFDVjs0REFFRCxnQkFBTSxDQUFDLEtBQUssQ0FBQyxvREFBa0QsSUFBSSxDQUFDLENBQUM7NERBQ3JFLGdCQUFNLENBQUMsSUFBSSxDQUFDLDBCQUFlLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsRUFBRSxTQUFTLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7NERBQy9GLHFCQUFNLFlBQUssQ0FBQyxHQUFHLENBQUMsRUFBQTs7NERBQWhCLFNBQWdCLENBQUM7NERBQ2pCLEtBQUssQ0FBQyxJQUFFLENBQUMsQ0FBQzs7Ozs7aURBRWIsQ0FBQyxFQUFBOzt3Q0FqQkYsU0FpQkUsQ0FBQzt3Q0FDSCxZQUFVLEdBQUcsS0FBSyxDQUFDOzs7OzZCQUNwQixDQUNGLENBQUM7d0JBQ0YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUMsR0FBRzs0QkFDM0IsTUFBTSxHQUFHLENBQUM7d0JBQ1osQ0FBQyxDQUFDLENBQUM7Ozs7OztLQUVOO0lBRUssdUNBQWtCLEdBQXhCOzs7OzRCQUNFLHFCQUFNLElBQUksQ0FBQyxJQUFJLEVBQUUsRUFBQTs7d0JBQWpCLFNBQWlCLENBQUM7d0JBQ2xCLDZCQUE2Qjt3QkFDN0IscUJBQU0sSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFEdEIsNkJBQTZCO3dCQUM3QixTQUFzQixDQUFDO3dCQUN2QixxQkFBTSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBQTs7d0JBQTlCLFNBQThCLENBQUM7Ozs7O0tBQ2hDO0lBRUssMEJBQUssR0FBWDs7Ozs7OzRCQUNvQyxxQkFBTSxxQ0FBNkIsT0FBQyxJQUFJLENBQUMsS0FBSywwQ0FBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBQTs7d0JBQXhJLGlCQUFpQixHQUFXLFNBQTRHO3dCQUNqSCxxQkFBTSxZQUFZLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLEVBQUE7O3dCQUF6RSxTQUFTLEdBQWMsU0FBa0Q7d0JBQy9FLHFCQUFNLE1BQU0sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLEVBQUE7O3dCQUFyQyxTQUFxQyxDQUFDO3dCQUN0QyxxQkFBTSw4QkFBc0IsT0FBQyxJQUFJLENBQUMsS0FBSywwQ0FBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBQTs7d0JBQXJHLFNBQXFHLENBQUM7Ozs7O0tBQ3ZHO0lBQ0gsaUJBQUM7QUFBRCxDQUFDLEFBaFNELENBQXdDLGdCQUFNLEdBZ1M3QyJ9