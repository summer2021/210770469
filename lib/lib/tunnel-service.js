"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = __importDefault(require("../common/logger"));
var core = __importStar(require("@serverless-devs/core"));
var client_1 = require("./client");
var lodash_1 = __importDefault(require("lodash"));
var fc_deploy_1 = require("./component/fc-deploy");
var time_1 = require("./utils/time");
var docker_1 = require("./docker/docker");
var nested_object_assign_1 = __importDefault(require("nested-object-assign"));
var utils_1 = require("./utils/utils");
var path = __importStar(require("path"));
var os = __importStar(require("os"));
var fse = __importStar(require("fs-extra"));
var tunnel_service20210509_1 = require("@alicloud/tunnel-service20210509");
var tunnel_service20210509_2 = __importDefault(require("@alicloud/tunnel-service20210509"));
var definition_1 = require("./definition");
var process_1 = require("./utils/process");
var stream_1 = require("./utils/stream");
var fc_remote_invoke_1 = require("./component/fc-remote-invoke");
var devs_1 = require("./utils/devs");
var dockerode_1 = __importDefault(require("dockerode"));
var runtime_1 = require("./utils/runtime");
var stdout_formatter_1 = __importDefault(require("./component/stdout-formatter"));
var errorProcessor_1 = require("./errorProcessor");
var retry_1 = require("./retry");
var state_1 = require("./utils/state");
var docker = new dockerode_1.default();
var IDE_PYCHARM = 'pycharm';
var TunnelService = /** @class */ (function () {
    function TunnelService(credentials, userServiceConfig, userFunctionConfig, region, access, appName, path, userTriggerConfigList, userCustomDomainConfigList, debugPort, debugIde) {
        var _this = this;
        var _a, _b;
        this.credentials = credentials;
        this.userServiceConfig = userServiceConfig;
        this.userFunctionConfig = userFunctionConfig;
        this.userTriggerConfigList = userTriggerConfigList;
        this.userCustomDomainConfigList = userCustomDomainConfigList;
        this.debugPort = debugPort;
        this.debugIde = debugIde;
        this.region = region;
        this.access = access;
        this.appName = appName;
        this.path = path;
        var config = {
            accessKeyId: (_a = this.credentials) === null || _a === void 0 ? void 0 : _a.AccessKeyID,
            accessKeySecret: (_b = this.credentials) === null || _b === void 0 ? void 0 : _b.AccessKeySecret,
            regionId: this.region,
            endpoint: TunnelService.tunnerServiceHost,
        };
        this.client = new tunnel_service20210509_2.default(config);
        process_1.setSigint();
        // exit container, when use ctrl + c
        process.on('SIGINT', function () { return __awaiter(_this, void 0, void 0, function () {
            var _a, stdoutFilePath, stderrFilePath;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        // end stream
                        if (this.streamOfRunner) {
                            stream_1.writeEventToStreamAndClose(this.streamOfRunner);
                        }
                        if (!this.runner) return [3 /*break*/, 3];
                        logger_1.default.info("Received canncel request, stopping running proxy container.....");
                        return [4 /*yield*/, docker_1.stopContainer(this.runner)];
                    case 1:
                        _b.sent();
                        return [4 /*yield*/, this.unsetProxyContainerId()];
                    case 2:
                        _b.sent();
                        _b.label = 3;
                    case 3:
                        _a = this.genOutputFileOfProxyContainer(), stdoutFilePath = _a.stdoutFilePath, stderrFilePath = _a.stderrFilePath;
                        if (this.stdoutFileWriteStream) {
                            this.stdoutFileWriteStream.close(function (err) {
                                if (err) {
                                    logger_1.default.warning("Close stdout file of proxy container: " + stdoutFilePath + " failed!\nError: " + err);
                                }
                            });
                        }
                        if (this.stderrFileWriteStream) {
                            this.stderrFileWriteStream.close(function (err) {
                                if (err) {
                                    logger_1.default.warning("Close stderr file of proxy container: " + stderrFilePath + " failed!\nError: " + err);
                                }
                            });
                        }
                        return [2 /*return*/];
                }
            });
        }); });
    }
    TunnelService.prototype.setup = function () {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var alicloudClient, _c, createSessionVm, e_1, proxyContainerVm, e_2, checkVm, e_3;
            var _this = this;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        if (((_a = this.userFunctionConfig) === null || _a === void 0 ? void 0 : _a.caPort) && ((_b = this.userFunctionConfig) === null || _b === void 0 ? void 0 : _b.caPort) !== 9000) {
                            throw new Error("Proxied invoke only support caPort: 9000 for custom-container/custom runtime.Please change it temporarily and retry.");
                        }
                        alicloudClient = new client_1.AlicloudClient(this.credentials);
                        _c = this;
                        return [4 /*yield*/, alicloudClient.getFcClient(this.region)];
                    case 1:
                        _c.fcClient = _d.sent();
                        createSessionVm = core.spinner("Creating session...");
                        _d.label = 2;
                    case 2:
                        _d.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, retry_1.promiseRetry(function (retry, times) { return __awaiter(_this, void 0, void 0, function () {
                                var _a, ex_1;
                                return __generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0:
                                            _b.trys.push([0, 3, , 4]);
                                            _a = this;
                                            return [4 /*yield*/, this.createSession()];
                                        case 1:
                                            _a.session = _b.sent();
                                            createSessionVm.succeed("Session created, session id: " + this.session.sessionId + ".");
                                            return [4 /*yield*/, this.saveSession()];
                                        case 2:
                                            _b.sent();
                                            return [2 /*return*/];
                                        case 3:
                                            ex_1 = _b.sent();
                                            if (ex_1.code === 'AccessDenied') {
                                                throw ex_1;
                                            }
                                            logger_1.default.debug("Create session failed, error is: \n" + ex_1);
                                            logger_1.default.info(stdout_formatter_1.default.stdoutFormatter.retry('session', 'create', '', times));
                                            retry(ex_1);
                                            return [3 /*break*/, 4];
                                        case 4: return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 3:
                        _d.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _d.sent();
                        createSessionVm.fail("Create session failed.");
                        throw e_1;
                    case 5:
                        // TODO: empty sessioin
                        logger_1.default.info("Deploying remote function...");
                        return [4 /*yield*/, this.makeHelperFunction()];
                    case 6:
                        _d.sent();
                        proxyContainerVm = core.spinner("Starting proxy container...");
                        _d.label = 7;
                    case 7:
                        _d.trys.push([7, 9, , 10]);
                        return [4 /*yield*/, this.runProxyContainer()];
                    case 8:
                        _d.sent();
                        proxyContainerVm.succeed("Proxy container is running.");
                        return [3 /*break*/, 10];
                    case 9:
                        e_2 = _d.sent();
                        proxyContainerVm.fail("Start proxy container failed.");
                        throw e_2;
                    case 10:
                        checkVm = core.spinner("Checking if session is established...");
                        _d.label = 11;
                    case 11:
                        _d.trys.push([11, 13, , 14]);
                        return [4 /*yield*/, this.queryUntilSessionEstablished()];
                    case 12:
                        _d.sent();
                        checkVm.succeed("Session established!");
                        return [3 /*break*/, 14];
                    case 13:
                        e_3 = _d.sent();
                        checkVm.fail("Session establish fail.");
                        // TODO: clean 操作
                        throw e_3;
                    case 14: return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.generateSessionName = function () {
        return "session_" + this.region + "_" + this.userServiceConfig.name;
    };
    TunnelService.prototype.cleanFunctionContainer = function () {
        var _a, _b, _c, _d, _e, _f;
        return __awaiter(this, void 0, void 0, function () {
            var invokeContainerId, container;
            return __generator(this, function (_g) {
                switch (_g.label) {
                    case 0: return [4 /*yield*/, state_1.getInvokeContainerIdFromState((_a = this.credentials) === null || _a === void 0 ? void 0 : _a.AccountID, this.region, (_b = this.userServiceConfig) === null || _b === void 0 ? void 0 : _b.name, (_c = this.userFunctionConfig) === null || _c === void 0 ? void 0 : _c.name)];
                    case 1:
                        invokeContainerId = _g.sent();
                        return [4 /*yield*/, docker.getContainer(invokeContainerId)];
                    case 2:
                        container = _g.sent();
                        return [4 /*yield*/, docker_1.stopContainer(container)];
                    case 3:
                        _g.sent();
                        return [4 /*yield*/, state_1.unsetInvokeContainerId((_d = this.credentials) === null || _d === void 0 ? void 0 : _d.AccountID, this.region, (_e = this.userServiceConfig) === null || _e === void 0 ? void 0 : _e.name, (_f = this.userFunctionConfig) === null || _f === void 0 ? void 0 : _f.name)];
                    case 4:
                        _g.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.createSession = function () {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var sessionName, req, res, data;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        sessionName = this.generateSessionName();
                        req = new tunnel_service20210509_1.CreateSessionRequest({ sessionName: sessionName });
                        logger_1.default.debug("Session name is : " + sessionName);
                        return [4 /*yield*/, this.client.createSession(req)];
                    case 1:
                        res = _b.sent();
                        data = (_a = res === null || res === void 0 ? void 0 : res.body) === null || _a === void 0 ? void 0 : _a.data;
                        logger_1.default.debug("Create session result data: " + JSON.stringify(data, null, '  '));
                        return [2 /*return*/, {
                                name: data === null || data === void 0 ? void 0 : data.sessionName,
                                sessionId: data === null || data === void 0 ? void 0 : data.sessionId,
                                localInstanceId: data === null || data === void 0 ? void 0 : data.localInstanceId,
                                remoteInstanceId: data === null || data === void 0 ? void 0 : data.remoteInstanceId,
                            }];
                }
            });
        });
    };
    TunnelService.prototype.deleteSession = function (sessionId) {
        return __awaiter(this, void 0, void 0, function () {
            var res, body;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.client.deleteSession(sessionId)];
                    case 1:
                        res = _a.sent();
                        body = res === null || res === void 0 ? void 0 : res.body;
                        logger_1.default.debug("Delete session body: " + JSON.stringify(body, null, '  '));
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.genHelperServiceConfig = function () {
        var _a;
        var helperServiceConfig = lodash_1.default.cloneDeep(this.userServiceConfig);
        helperServiceConfig.name = "SESSION-" + ((_a = this.session) === null || _a === void 0 ? void 0 : _a.sessionId.substring(0, 7));
        // 开启公网访问
        helperServiceConfig.internetAccess = true;
        // 删除 nas 配置
        if (definition_1.isAutoConfig(helperServiceConfig === null || helperServiceConfig === void 0 ? void 0 : helperServiceConfig.nasConfig)) {
            delete helperServiceConfig.nasConfig;
        }
        return helperServiceConfig;
    };
    TunnelService.prototype.genHelperFunctionConfig = function () {
        var _this = this;
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w;
        var helperFunctionConfig = {
            name: (_a = this.userFunctionConfig) === null || _a === void 0 ? void 0 : _a.name,
            description: (_b = this.userFunctionConfig) === null || _b === void 0 ? void 0 : _b.description,
            runtime: (_c = this.userFunctionConfig) === null || _c === void 0 ? void 0 : _c.runtime,
            handler: (_d = this.userFunctionConfig) === null || _d === void 0 ? void 0 : _d.handler,
            timeout: (_e = this.userFunctionConfig) === null || _e === void 0 ? void 0 : _e.timeout,
            memorySize: (_f = this.userFunctionConfig) === null || _f === void 0 ? void 0 : _f.memorySize,
            // TODO: codeUri or ossKey
            codeUri: (_g = this.userFunctionConfig) === null || _g === void 0 ? void 0 : _g.codeUri,
            environmentVariables: {
                TUNNEL_SERVICE_HOST: TunnelService.tunnerServiceHost,
                TUNNEL_SERVICE_INSTANCE_ID: (_h = this.session) === null || _h === void 0 ? void 0 : _h.remoteInstanceId,
                TUNNEL_SERVICE_SESSION_ID: (_j = this.session) === null || _j === void 0 ? void 0 : _j.sessionId,
                TUNNEL_SERVICE_AK_ID: (_k = this.credentials) === null || _k === void 0 ? void 0 : _k.AccessKeyID,
                TUNNEL_SERVICE_AK_SECRET: (_l = this.credentials) === null || _l === void 0 ? void 0 : _l.AccessKeySecret,
                ENABLE_TS_REMOTE_DEBUG: true,
            },
        };
        // Add TS_DEBUG_HTTP_TRIGGER_ADJUST env for nodejs/python/php/java http trigger
        if (!lodash_1.default.isEmpty(definition_1.getHttpTrigger(this.userTriggerConfigList))) {
            TunnelService.runtimeListNeedSetTsAdjustFLag.forEach(function (runtime) {
                var _a;
                if (((_a = _this.userFunctionConfig) === null || _a === void 0 ? void 0 : _a.runtime.indexOf(runtime)) !== -1) {
                    Object.assign(helperFunctionConfig.environmentVariables, {
                        TS_DEBUG_HTTP_TRIGGER_ADJUST: true,
                    });
                }
            });
        }
        if (((_m = this.userFunctionConfig) === null || _m === void 0 ? void 0 : _m.initializationTimeout) && ((_o = this.userFunctionConfig) === null || _o === void 0 ? void 0 : _o.initializer)) {
            Object.assign(helperFunctionConfig, {
                initializationTimeout: (_p = this.userFunctionConfig) === null || _p === void 0 ? void 0 : _p.initializationTimeout,
                initializer: (_q = this.userFunctionConfig) === null || _q === void 0 ? void 0 : _q.initializer,
            });
        }
        if ((_r = this.userFunctionConfig) === null || _r === void 0 ? void 0 : _r.instanceLifecycleConfig) {
            Object.assign(helperFunctionConfig, {
                instanceLifecycleConfig: (_s = this.userFunctionConfig) === null || _s === void 0 ? void 0 : _s.instanceLifecycleConfig,
            });
        }
        if ((_t = this.userFunctionConfig) === null || _t === void 0 ? void 0 : _t.instanceConcurrency) {
            Object.assign(helperFunctionConfig, {
                instanceConcurrency: (_u = this.userFunctionConfig) === null || _u === void 0 ? void 0 : _u.instanceConcurrency,
            });
        }
        if ((_v = this.userFunctionConfig) === null || _v === void 0 ? void 0 : _v.asyncConfiguration) {
            Object.assign(helperFunctionConfig, {
                asyncConfiguration: (_w = this.userFunctionConfig) === null || _w === void 0 ? void 0 : _w.asyncConfiguration,
            });
        }
        return helperFunctionConfig;
    };
    TunnelService.prototype.genHelperCustomDomainConfig = function () {
        var _this = this;
        if (lodash_1.default.isEmpty(this.userCustomDomainConfigList)) {
            return [];
        }
        var customDomainConfigList = [];
        for (var _i = 0, _a = this.userCustomDomainConfigList; _i < _a.length; _i++) {
            var userDomain = _a[_i];
            var routeConfigList = userDomain === null || userDomain === void 0 ? void 0 : userDomain.routeConfigs.map(function (useRouter) {
                var _a, _b;
                if ((useRouter === null || useRouter === void 0 ? void 0 : useRouter.serviceName) &&
                    (useRouter === null || useRouter === void 0 ? void 0 : useRouter.serviceName) === ((_a = _this.userServiceConfig) === null || _a === void 0 ? void 0 : _a.name) && (useRouter === null || useRouter === void 0 ? void 0 : useRouter.functionName) &&
                    (useRouter === null || useRouter === void 0 ? void 0 : useRouter.functionName) === ((_b = _this.userFunctionConfig) === null || _b === void 0 ? void 0 : _b.name)) {
                    var router = lodash_1.default.cloneDeep(useRouter);
                    router === null || router === void 0 ? true : delete router.serviceName;
                    router === null || router === void 0 ? true : delete router.functionName;
                    return router;
                }
                if (!(useRouter === null || useRouter === void 0 ? void 0 : useRouter.serviceName) && !(useRouter === null || useRouter === void 0 ? void 0 : useRouter.functionName)) {
                    return useRouter;
                }
            });
            var domain = {
                domainName: userDomain === null || userDomain === void 0 ? void 0 : userDomain.domainName,
                protocol: userDomain === null || userDomain === void 0 ? void 0 : userDomain.protocol,
                routeConfigs: routeConfigList.filter(function (r) { return r; }),
            };
            if (userDomain === null || userDomain === void 0 ? void 0 : userDomain.certConfig) {
                Object.assign(domain, {
                    certConfig: domain === null || domain === void 0 ? void 0 : domain.certConfig,
                });
            }
            customDomainConfigList.push(domain);
        }
        return customDomainConfigList;
    };
    TunnelService.prototype.makeHelperFunction = function () {
        return __awaiter(this, void 0, void 0, function () {
            var helperServiceConfig, helperFunctionConfig, helperTriggerConfigList, helperCustomDomainConfigList, fcDeployComponent, fcDeployComponentInputs, fcDeployComponentIns, deployRes, setHelperVm, e_4, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        helperServiceConfig = this.genHelperServiceConfig();
                        helperFunctionConfig = this.genHelperFunctionConfig();
                        helperTriggerConfigList = this.userTriggerConfigList;
                        helperCustomDomainConfigList = this.genHelperCustomDomainConfig();
                        fcDeployComponent = new fc_deploy_1.FcDeployComponent(this.region, helperServiceConfig, this.access, this.appName, this.path, helperFunctionConfig, helperTriggerConfigList, helperCustomDomainConfigList);
                        fcDeployComponentInputs = fcDeployComponent.genComponentInputs('fc-deploy', 'fc-deploy-project', '--use-local', 'deploy');
                        return [4 /*yield*/, core.loadComponent("devsapp/fc-deploy")];
                    case 1:
                        fcDeployComponentIns = _a.sent();
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 9, , 11]);
                        return [4 /*yield*/, fcDeployComponentIns.deploy(fcDeployComponentInputs)];
                    case 3:
                        deployRes = _a.sent();
                        return [4 /*yield*/, this.saveHelperFunctionDeployRes(deployRes)];
                    case 4:
                        _a.sent();
                        setHelperVm = core.spinner("Setting remote function with 1 provison and 0 elasticity");
                        _a.label = 5;
                    case 5:
                        _a.trys.push([5, 7, , 8]);
                        return [4 /*yield*/, this.setHelperFunctionConfig(helperServiceConfig.name, helperFunctionConfig.name)];
                    case 6:
                        _a.sent();
                        setHelperVm.succeed("Remote function is set to 1 provison and 0 elasticity.");
                        return [3 /*break*/, 8];
                    case 7:
                        e_4 = _a.sent();
                        setHelperVm.fail("Fail to set provison and elasticity for remote function.");
                        throw e_4;
                    case 8: return [3 /*break*/, 11];
                    case 9:
                        e_5 = _a.sent();
                        return [4 /*yield*/, fcDeployComponentIns.remove(fcDeployComponentInputs)];
                    case 10:
                        _a.sent();
                        errorProcessor_1.processMakeHelperFunctionErr(e_5);
                        return [3 /*break*/, 11];
                    case 11: return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.saveHelperFunctionDeployRes = function (deployRes) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, devs_1.setKVInState('helperConfig', deployRes, this.genStateId())];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.saveSession = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, devs_1.setKVInState('session', this.session, this.genStateId())];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.genStateId = function () {
        var _a, _b;
        return this.credentials.AccountID + "-" + this.region + "-" + ((_a = this.userServiceConfig) === null || _a === void 0 ? void 0 : _a.name) + "-" + ((_b = this.userFunctionConfig) === null || _b === void 0 ? void 0 : _b.name);
    };
    TunnelService.prototype.setHelperFunctionProvision = function (helperServiceName, helperFunctionName, targetProvision, targetAlias) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var alias, alicloudClient, _c, e_6, provisionRes, retryCnt;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0:
                        alias = targetAlias || 'LATEST';
                        if (!!this.fcClient) return [3 /*break*/, 2];
                        alicloudClient = new client_1.AlicloudClient(this.credentials);
                        _c = this;
                        return [4 /*yield*/, alicloudClient.getFcClient(this.region)];
                    case 1:
                        _c.fcClient = _d.sent();
                        _d.label = 2;
                    case 2:
                        _d.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, this.fcClient.putProvisionConfig(helperServiceName, helperFunctionName, alias, { target: targetProvision })];
                    case 3:
                        _d.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        e_6 = _d.sent();
                        throw new Error("Put provision config error: " + e_6 + ", please make sure that your account has updateService permission.");
                    case 5: return [4 /*yield*/, this.fcClient.getProvisionConfig(helperServiceName, helperFunctionName, alias)];
                    case 6:
                        provisionRes = _d.sent();
                        retryCnt = 0;
                        _d.label = 7;
                    case 7:
                        if (!(((_a = provisionRes === null || provisionRes === void 0 ? void 0 : provisionRes.data) === null || _a === void 0 ? void 0 : _a.current) !== 1 && retryCnt <= TunnelService.maxRetryCnt)) return [3 /*break*/, 10];
                        return [4 /*yield*/, this.fcClient.getProvisionConfig(helperServiceName, helperFunctionName, alias)];
                    case 8:
                        provisionRes = _d.sent();
                        retryCnt = retryCnt + 1;
                        logger_1.default.debug("Retry setting provision " + retryCnt + " times.");
                        return [4 /*yield*/, time_1.sleep(3000)];
                    case 9:
                        _d.sent();
                        return [3 /*break*/, 7];
                    case 10:
                        if (((_b = provisionRes === null || provisionRes === void 0 ? void 0 : provisionRes.data) === null || _b === void 0 ? void 0 : _b.current) !== 1) {
                            logger_1.default.debug(JSON.stringify(provisionRes, null, '  '));
                            // TODO: 指定具体权限
                            throw new Error("Set/get provision of helper function error.Please make sure you have the related ram permission.");
                        }
                        logger_1.default.debug("Set provision result: " + JSON.stringify(provisionRes === null || provisionRes === void 0 ? void 0 : provisionRes.data));
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.unsetHelperFunctionConfig = function (serviceName, functionName, alias) {
        return __awaiter(this, void 0, void 0, function () {
            var method, path, alicloudClient, _a, elasticityRes;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: 
                    // 预留设置为 0
                    return [4 /*yield*/, this.setHelperFunctionProvision(serviceName, functionName, 0, alias)];
                    case 1:
                        // 预留设置为 0
                        _b.sent();
                        method = 'DELETE';
                        path = "/services/" + serviceName + "." + alias + "/functions/" + functionName + "/on-demand-config";
                        if (!!this.client) return [3 /*break*/, 3];
                        alicloudClient = new client_1.AlicloudClient(this.credentials);
                        _a = this;
                        return [4 /*yield*/, alicloudClient.getFcClient(this.region)];
                    case 2:
                        _a.fcClient = _b.sent();
                        _b.label = 3;
                    case 3: return [4 /*yield*/, this.fcClient.request(method, path, null, JSON.stringify({}))];
                    case 4:
                        elasticityRes = _b.sent();
                        logger_1.default.debug("On-demand config delete result: " + (elasticityRes === null || elasticityRes === void 0 ? void 0 : elasticityRes.statusCode));
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.setHelperFunctionConfig = function (serviceName, functionName) {
        return __awaiter(this, void 0, void 0, function () {
            var alias, method, path, alicloudClient, _a, elasticityRes;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        alias = 'LATEST';
                        // Set provision to 1
                        return [4 /*yield*/, this.setHelperFunctionProvision(serviceName, functionName, 1, alias)];
                    case 1:
                        // Set provision to 1
                        _b.sent();
                        method = 'PUT';
                        path = "/services/" + serviceName + "." + alias + "/functions/" + functionName + "/on-demand-config";
                        if (!!this.fcClient) return [3 /*break*/, 3];
                        alicloudClient = new client_1.AlicloudClient(this.credentials);
                        _a = this;
                        return [4 /*yield*/, alicloudClient.getFcClient(this.region)];
                    case 2:
                        _a.fcClient = _b.sent();
                        _b.label = 3;
                    case 3: return [4 /*yield*/, this.fcClient.request(method, path, null, JSON.stringify({ maximumInstanceCount: 0 }))];
                    case 4:
                        elasticityRes = _b.sent();
                        logger_1.default.debug("On-demand config put result: " + (elasticityRes === null || elasticityRes === void 0 ? void 0 : elasticityRes.statusCode));
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.genOutputFileOfProxyContainer = function () {
        var _a;
        if (!lodash_1.default.isEmpty(this.session)) {
            var outputDirOfContainer = path.join(TunnelService.cacheDirPath, (_a = this.session) === null || _a === void 0 ? void 0 : _a.sessionId);
            // await fse.ensureDir(outputDirOfContainer);
            var stdoutFilePath = path.join(outputDirOfContainer, 'stdout.log');
            var stderrFilePath = path.join(outputDirOfContainer, 'stderr.log');
            return {
                stdoutFilePath: stdoutFilePath,
                stderrFilePath: stderrFilePath,
            };
        }
        return {};
    };
    TunnelService.prototype.runProxyContainer = function () {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var opts, _b, stdoutFilePath, stderrFilePath, proxyContainer;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0: 
                    // pull image if need
                    return [4 /*yield*/, docker_1.pullImageIfNeed(TunnelService.proxyImaggeRegistry, TunnelService.proxyImageRepo, TunnelService.proxyImageName, TunnelService.proxyImageVersion)];
                    case 1:
                        // pull image if need
                        _c.sent();
                        opts = this.generateProxyContainerOpts();
                        _b = this.genOutputFileOfProxyContainer(), stdoutFilePath = _b.stdoutFilePath, stderrFilePath = _b.stderrFilePath;
                        return [4 /*yield*/, fse.ensureDir(path.dirname(stdoutFilePath))];
                    case 2:
                        _c.sent();
                        logger_1.default.debug("Container: " + (opts === null || opts === void 0 ? void 0 : opts.name) + " stdout to: " + stdoutFilePath + ", stderr to: " + stderrFilePath);
                        this.stdoutFileWriteStream = fse.createWriteStream(stdoutFilePath, { flag: 'w+', encoding: 'utf-8', autoClose: true });
                        this.stderrFileWriteStream = fse.createWriteStream(stderrFilePath, { flag: 'w+', encoding: 'utf-8', autoClose: true });
                        return [4 /*yield*/, docker_1.runContainer(opts, this.stdoutFileWriteStream, this.stderrFileWriteStream)];
                    case 3:
                        proxyContainer = _c.sent();
                        this.streamOfRunner = proxyContainer === null || proxyContainer === void 0 ? void 0 : proxyContainer.stream;
                        this.runner = proxyContainer === null || proxyContainer === void 0 ? void 0 : proxyContainer.container;
                        return [4 /*yield*/, this.saveProxyContainerId((_a = this.runner) === null || _a === void 0 ? void 0 : _a.id)];
                    case 4:
                        _c.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.saveProxyContainerId = function (containerId) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, devs_1.setKVInState('proxyContainerId', containerId, this.genStateId())];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.unsetProxyContainerId = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, devs_1.unsetKVInState('proxyContainerId', this.genStateId())];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.generateProxyContainerDebugOpts = function () {
        var _a, _b;
        var _c;
        var runtime = (_c = this.userFunctionConfig) === null || _c === void 0 ? void 0 : _c.runtime;
        if (runtime_1.isCustomContainerRuntime(runtime)) {
            return {};
        }
        var exposedPort = this.debugPort + "/tcp";
        if (this.debugIde === IDE_PYCHARM) {
            if (runtime !== 'python2.7' && runtime !== 'python3') {
                throw new Error(IDE_PYCHARM + " debug config only support for runtime [python2.7, python3]");
            }
            else {
                return {};
            }
        }
        else if (runtime === 'php7.2') {
            return {};
        }
        else {
            return {
                ExposedPorts: (_a = {},
                    _a[exposedPort] = {},
                    _a),
                HostConfig: {
                    PortBindings: (_b = {},
                        _b[exposedPort] = [
                            {
                                HostIp: '',
                                HostPort: "" + this.debugPort,
                            },
                        ],
                        _b),
                },
            };
        }
    };
    TunnelService.prototype.generateProxyContainerOpts = function () {
        var _a;
        var imageName = TunnelService.proxyImaggeRegistry + "/" + TunnelService.proxyImageRepo + "/" + TunnelService.proxyImageName + ":" + TunnelService.proxyImageVersion;
        var containerName = definition_1.genProxyContainerName((_a = this.session) === null || _a === void 0 ? void 0 : _a.sessionId);
        var ioOpts = {
            OpenStdin: false,
            Tty: false,
            StdinOnce: true,
            AttachStdin: true,
            AttachStdout: true,
            AttachStderr: true,
        };
        var hostOpts = {
            HostConfig: {
                AutoRemove: true,
                Privileged: true,
                Mounts: [],
            },
        };
        var debugOpts = {};
        if (this.debugPort) {
            debugOpts = this.generateProxyContainerDebugOpts();
        }
        var opts = nested_object_assign_1.default({
            Env: this.generateProxyContainerEnv(),
            Image: imageName,
            name: containerName,
            User: '0:0',
        }, ioOpts, hostOpts, debugOpts);
        var encryptedOpts = lodash_1.default.cloneDeep(opts);
        if (encryptedOpts === null || encryptedOpts === void 0 ? void 0 : encryptedOpts.Env) {
            var encryptedEnv = encryptedOpts.Env.map(function (e) {
                if (e.startsWith('TUNNEL_SERVICE_AK_ID') || e.startsWith('TUNNEL_SERVICE_AK_SECRET')) {
                    var keyValueList = e.split('=');
                    var encrptedVal = utils_1.mark(keyValueList[1]);
                    return keyValueList[0] + "=" + encrptedVal;
                }
                else {
                    return e;
                }
            });
            encryptedOpts.Env = encryptedEnv;
        }
        logger_1.default.debug("Tunnel service proxy container options: " + JSON.stringify(encryptedOpts, null, '  '));
        return opts;
    };
    TunnelService.prototype.generateProxyContainerEnv = function () {
        var _a, _b, _c, _d;
        var envs = {
            TUNNEL_SERVICE_HOST: TunnelService.tunnerServiceHost,
            TUNNEL_SERVICE_SESSION_ID: (_a = this.session) === null || _a === void 0 ? void 0 : _a.sessionId,
            TUNNEL_SERVICE_INSTANCE_ID: (_b = this.session) === null || _b === void 0 ? void 0 : _b.localInstanceId,
            TUNNEL_SERVICE_AK_ID: (_c = this.credentials) === null || _c === void 0 ? void 0 : _c.AccessKeyID,
            TUNNEL_SERVICE_AK_SECRET: (_d = this.credentials) === null || _d === void 0 ? void 0 : _d.AccessKeySecret,
        };
        return lodash_1.default.map(envs || {}, function (v, k) { return k + "=" + v; });
    };
    TunnelService.prototype.queryUntilSessionEstablished = function () {
        var _a, _b, _c, _d, _e, _f;
        return __awaiter(this, void 0, void 0, function () {
            var res, state, retryCnt;
            return __generator(this, function (_g) {
                switch (_g.label) {
                    case 0: return [4 /*yield*/, this.client.getSession((_a = this.session) === null || _a === void 0 ? void 0 : _a.sessionId)];
                    case 1:
                        res = _g.sent();
                        state = (_c = (_b = res === null || res === void 0 ? void 0 : res.body) === null || _b === void 0 ? void 0 : _b.data) === null || _c === void 0 ? void 0 : _c.status;
                        retryCnt = 0;
                        _g.label = 2;
                    case 2:
                        if (!(state !== 'ESTABLISHED' && retryCnt < TunnelService.maxRetryCnt)) return [3 /*break*/, 5];
                        return [4 /*yield*/, time_1.sleep(3000)];
                    case 3:
                        _g.sent();
                        return [4 /*yield*/, this.client.getSession((_d = this.session) === null || _d === void 0 ? void 0 : _d.sessionId)];
                    case 4:
                        res = _g.sent();
                        state = (_f = (_e = res === null || res === void 0 ? void 0 : res.body) === null || _e === void 0 ? void 0 : _e.data) === null || _f === void 0 ? void 0 : _f.status;
                        retryCnt = retryCnt + 1;
                        return [3 /*break*/, 2];
                    case 5:
                        if (state !== 'ESTABLISHED') {
                            throw new Error("Session establish fail, body in response is: " + JSON.stringify(res === null || res === void 0 ? void 0 : res.body, null, '  '));
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.invokeHelperFunction = function (args) {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function () {
            var helperConfig, helperServiceConfig, helperFunctionConfig, fcRemoteInvokeComponent, inputs, fcRemoteInvokeComponentIns;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, this.checkIfProxyContainerRunning()];
                    case 1:
                        if (!!(_d.sent())) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.clean()];
                    case 2:
                        _d.sent();
                        return [4 /*yield*/, this.cleanFunctionContainer()];
                    case 3:
                        _d.sent();
                        throw new Error("Proxy container is not running, please run 'clean' method and retry 'setup' method.");
                    case 4: return [4 /*yield*/, state_1.getHelperConfigFromState((_a = this.credentials) === null || _a === void 0 ? void 0 : _a.AccountID, this.region, (_b = this.userServiceConfig) === null || _b === void 0 ? void 0 : _b.name, (_c = this.userFunctionConfig) === null || _c === void 0 ? void 0 : _c.name)];
                    case 5:
                        helperConfig = _d.sent();
                        helperServiceConfig = helperConfig === null || helperConfig === void 0 ? void 0 : helperConfig.service;
                        helperFunctionConfig = helperConfig === null || helperConfig === void 0 ? void 0 : helperConfig.function;
                        // const helperTriggerConfigList: TriggerConfig[] = helperConfig?.triggers;
                        logger_1.default.info("Invoking helper service: " + (helperServiceConfig === null || helperServiceConfig === void 0 ? void 0 : helperServiceConfig.name) + ", function: " + (helperFunctionConfig === null || helperFunctionConfig === void 0 ? void 0 : helperFunctionConfig.name) + " in region: " + this.region + " to make local function run.");
                        fcRemoteInvokeComponent = new fc_remote_invoke_1.FcRemoteInvokeComponent(this.region, helperServiceConfig === null || helperServiceConfig === void 0 ? void 0 : helperServiceConfig.name, this.access, this.appName, this.path, helperFunctionConfig === null || helperFunctionConfig === void 0 ? void 0 : helperFunctionConfig.name);
                        inputs = fcRemoteInvokeComponent.genComponentInputs('fc-remote-invoke', 'fc-remote-invoke-project', args, 'invoke');
                        return [4 /*yield*/, core.loadComponent("devsapp/fc-remote-invoke")];
                    case 6:
                        fcRemoteInvokeComponentIns = _d.sent();
                        return [4 /*yield*/, fcRemoteInvokeComponentIns.invoke(inputs)];
                    case 7:
                        _d.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.checkIfProxyContainerRunning = function () {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function () {
            var proxyContainerId, runningContainers;
            return __generator(this, function (_d) {
                switch (_d.label) {
                    case 0: return [4 /*yield*/, state_1.getProxyContainerIdFromState((_a = this.credentials) === null || _a === void 0 ? void 0 : _a.AccountID, this.region, (_b = this.userServiceConfig) === null || _b === void 0 ? void 0 : _b.name, (_c = this.userFunctionConfig) === null || _c === void 0 ? void 0 : _c.name)];
                    case 1:
                        proxyContainerId = _d.sent();
                        if (!proxyContainerId) {
                            return [2 /*return*/, false];
                        }
                        return [4 /*yield*/, docker.listContainers()];
                    case 2:
                        runningContainers = (_d.sent()).map(function (container) { return container === null || container === void 0 ? void 0 : container.Id; });
                        if (runningContainers.includes(proxyContainerId)) {
                            return [2 /*return*/, true];
                        }
                        return [2 /*return*/, false];
                }
            });
        });
    };
    TunnelService.prototype.clean = function () {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j;
        return __awaiter(this, void 0, void 0, function () {
            var helperConfig, session, proxyContainerId, helperServiceConfig, helperFunctionConfig, helperTriggerConfigList, c, e_7, unsetConfigVm, e_8, fcDeployComponent, fcDeployComponentIns, fcDeployComponentInputs, e_9, deleteSessionVm, e_10;
            return __generator(this, function (_k) {
                switch (_k.label) {
                    case 0: return [4 /*yield*/, state_1.getHelperConfigFromState((_a = this.credentials) === null || _a === void 0 ? void 0 : _a.AccountID, this.region, (_b = this.userServiceConfig) === null || _b === void 0 ? void 0 : _b.name, (_c = this.userFunctionConfig) === null || _c === void 0 ? void 0 : _c.name)];
                    case 1:
                        helperConfig = _k.sent();
                        return [4 /*yield*/, state_1.getSessionFromState((_d = this.credentials) === null || _d === void 0 ? void 0 : _d.AccountID, this.region, (_e = this.userServiceConfig) === null || _e === void 0 ? void 0 : _e.name, (_f = this.userFunctionConfig) === null || _f === void 0 ? void 0 : _f.name)];
                    case 2:
                        session = _k.sent();
                        return [4 /*yield*/, state_1.getProxyContainerIdFromState((_g = this.credentials) === null || _g === void 0 ? void 0 : _g.AccountID, this.region, (_h = this.userServiceConfig) === null || _h === void 0 ? void 0 : _h.name, (_j = this.userFunctionConfig) === null || _j === void 0 ? void 0 : _j.name)];
                    case 3:
                        proxyContainerId = _k.sent();
                        helperServiceConfig = helperConfig === null || helperConfig === void 0 ? void 0 : helperConfig.service;
                        helperFunctionConfig = helperConfig === null || helperConfig === void 0 ? void 0 : helperConfig.function;
                        helperTriggerConfigList = helperConfig === null || helperConfig === void 0 ? void 0 : helperConfig.triggers;
                        if (!proxyContainerId) return [3 /*break*/, 8];
                        _k.label = 4;
                    case 4:
                        _k.trys.push([4, 7, , 8]);
                        c = docker.getContainer(proxyContainerId);
                        return [4 /*yield*/, docker_1.stopContainer(c)];
                    case 5:
                        _k.sent();
                        return [4 /*yield*/, this.unsetProxyContainerId()];
                    case 6:
                        _k.sent();
                        return [3 /*break*/, 8];
                    case 7:
                        e_7 = _k.sent();
                        logger_1.default.warning(stdout_formatter_1.default.stdoutFormatter.warn('stop proxy container', "containerId: " + proxyContainerId, e_7 === null || e_7 === void 0 ? void 0 : e_7.message));
                        logger_1.default.debug("Stop proxy container: " + proxyContainerId + " error: " + e_7);
                        return [3 /*break*/, 8];
                    case 8:
                        unsetConfigVm = core.spinner("Unsetting helper function config...");
                        _k.label = 9;
                    case 9:
                        _k.trys.push([9, 11, , 12]);
                        return [4 /*yield*/, this.unsetHelperFunctionConfig(helperServiceConfig === null || helperServiceConfig === void 0 ? void 0 : helperServiceConfig.name, helperFunctionConfig === null || helperFunctionConfig === void 0 ? void 0 : helperFunctionConfig.name, 'LATEST')];
                    case 10:
                        _k.sent();
                        unsetConfigVm.succeed("Unset helper function provision and on-demand config done.");
                        return [3 /*break*/, 12];
                    case 11:
                        e_8 = _k.sent();
                        unsetConfigVm.fail("Unset error.");
                        logger_1.default.error(e_8 === null || e_8 === void 0 ? void 0 : e_8.message);
                        logger_1.default.debug("Error: " + e_8);
                        return [3 /*break*/, 12];
                    case 12:
                        _k.trys.push([12, 15, , 16]);
                        fcDeployComponent = new fc_deploy_1.FcDeployComponent(this.region, helperServiceConfig, this.access, this.appName, this.path, helperFunctionConfig, helperTriggerConfigList);
                        return [4 /*yield*/, core.loadComponent("devsapp/fc-deploy")];
                    case 13:
                        fcDeployComponentIns = _k.sent();
                        fcDeployComponentInputs = fcDeployComponent.genComponentInputs('fc-deploy', 'fc-deploy-project', 'service -y', 'remove');
                        return [4 /*yield*/, fcDeployComponentIns.remove(fcDeployComponentInputs)];
                    case 14:
                        _k.sent();
                        return [3 /*break*/, 16];
                    case 15:
                        e_9 = _k.sent();
                        logger_1.default.warning(stdout_formatter_1.default.stdoutFormatter.warn('remove helper service', "serviceName: " + (helperServiceConfig === null || helperServiceConfig === void 0 ? void 0 : helperServiceConfig.name) + ", functionName: " + (helperFunctionConfig === null || helperFunctionConfig === void 0 ? void 0 : helperFunctionConfig.name), e_9 === null || e_9 === void 0 ? void 0 : e_9.message));
                        logger_1.default.debug("Error: " + e_9);
                        return [3 /*break*/, 16];
                    case 16:
                        deleteSessionVm = core.spinner("Deleting session: " + (session === null || session === void 0 ? void 0 : session.sessionId) + "...");
                        _k.label = 17;
                    case 17:
                        _k.trys.push([17, 19, , 20]);
                        return [4 /*yield*/, this.deleteSession(session === null || session === void 0 ? void 0 : session.sessionId)];
                    case 18:
                        _k.sent();
                        deleteSessionVm.succeed("Delete session: " + (session === null || session === void 0 ? void 0 : session.sessionId) + " done.");
                        return [3 /*break*/, 20];
                    case 19:
                        e_10 = _k.sent();
                        deleteSessionVm.fail("Delete error.");
                        logger_1.default.error(e_10 === null || e_10 === void 0 ? void 0 : e_10.message);
                        logger_1.default.debug("Error: " + e_10);
                        return [3 /*break*/, 20];
                    case 20: return [2 /*return*/];
                }
            });
        });
    };
    TunnelService.prototype.getSession = function () {
        return this.session;
    };
    TunnelService.maxRetryCnt = 40;
    TunnelService.tunnerServiceHost = 'tunnel-service.cn-hangzhou.aliyuncs.com';
    // private static defaultFunctionImage: string = `registry.${TunnelService.defaultRegion}.aliyuncs.com/aliyunfc/ts-remote:v0.2`;
    TunnelService.proxyImageName = 'ts-online-local';
    TunnelService.proxyImageStableVersion = 'v0.1.0';
    // private static readonly helperImageStableVersion: string = 'v0.1.0';
    // private static readonly helperImageVersion: string =
    //   process.env['TUNNEL_SERVICE_HELPER_IMAGE_LATEST_VERSION'] || TunnelService.helperImageStableVersion;
    TunnelService.proxyImageVersion = process.env['TUNNEL_SERVICE_PROXY_IMAGE_LATEST_VERSION'] || TunnelService.proxyImageStableVersion;
    TunnelService.proxyImageRepo = 'aliyunfc';
    TunnelService.proxyImaggeRegistry = 'registry.cn-hangzhou.aliyuncs.com';
    TunnelService.cacheDirPath = path.join(os.homedir(), '.s', 'cache', 'fc-tunnel-invoke');
    TunnelService.runtimeListNeedSetTsAdjustFLag = ['nodejs', 'python', 'php', 'java'];
    return TunnelService;
}());
exports.default = TunnelService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHVubmVsLXNlcnZpY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvbGliL3R1bm5lbC1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLDREQUFzQztBQUN0QywwREFBOEM7QUFHOUMsbUNBQTBDO0FBRzFDLGtEQUF1QjtBQUN2QixtREFBMEQ7QUFDMUQscUNBQXFDO0FBQ3JDLDBDQUErRTtBQUMvRSw4RUFBc0Q7QUFDdEQsdUNBQXFDO0FBQ3JDLHlDQUE2QjtBQUM3QixxQ0FBeUI7QUFDekIsNENBQWdDO0FBRWhDLDJFQU8wQztBQUMxQyw0RkFBc0Q7QUFFdEQsMkNBQW1GO0FBQ25GLDJDQUE0QztBQUM1Qyx5Q0FBNEQ7QUFFNUQsaUVBQXVFO0FBQ3ZFLHFDQUE0RDtBQUM1RCx3REFBK0I7QUFDL0IsMkNBQTJEO0FBQzNELGtGQUEyRDtBQUMzRCxtREFBZ0U7QUFDaEUsaUNBQXVDO0FBQ3ZDLHVDQU11QjtBQUV2QixJQUFNLE1BQU0sR0FBUSxJQUFJLG1CQUFNLEVBQUUsQ0FBQztBQUNqQyxJQUFNLFdBQVcsR0FBVyxTQUFTLENBQUM7QUFFdEM7SUFvQ0UsdUJBQ0UsV0FBeUIsRUFDekIsaUJBQWdDLEVBQ2hDLGtCQUFrQyxFQUNsQyxNQUFjLEVBQ2QsTUFBYyxFQUNkLE9BQWUsRUFDZixJQUFTLEVBQ1QscUJBQXVDLEVBQ3ZDLDBCQUFpRCxFQUNqRCxTQUFrQixFQUNsQixRQUFpQjtRQVhuQixpQkE4REM7O1FBakRDLElBQUksQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQztRQUMzQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUM7UUFDN0MsSUFBSSxDQUFDLHFCQUFxQixHQUFHLHFCQUFxQixDQUFDO1FBQ25ELElBQUksQ0FBQywwQkFBMEIsR0FBRywwQkFBMEIsQ0FBQztRQUU3RCxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN6QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztRQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFNLE1BQU0sR0FBUTtZQUNsQixXQUFXLFFBQUUsSUFBSSxDQUFDLFdBQVcsMENBQUUsV0FBVztZQUMxQyxlQUFlLFFBQUUsSUFBSSxDQUFDLFdBQVcsMENBQUUsZUFBZTtZQUNsRCxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDckIsUUFBUSxFQUFFLGFBQWEsQ0FBQyxpQkFBaUI7U0FDMUMsQ0FBQztRQUNGLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxnQ0FBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pDLG1CQUFTLEVBQUUsQ0FBQztRQUNaLG9DQUFvQztRQUNwQyxPQUFPLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRTs7Ozs7d0JBQ25CLGFBQWE7d0JBQ2IsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFOzRCQUN2QixtQ0FBMEIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7eUJBQ2pEOzZCQUVHLElBQUksQ0FBQyxNQUFNLEVBQVgsd0JBQVc7d0JBQ2IsZ0JBQU0sQ0FBQyxJQUFJLENBQUMsaUVBQWlFLENBQUMsQ0FBQzt3QkFDL0UscUJBQU0sc0JBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUFoQyxTQUFnQyxDQUFDO3dCQUNqQyxxQkFBTSxJQUFJLENBQUMscUJBQXFCLEVBQUUsRUFBQTs7d0JBQWxDLFNBQWtDLENBQUM7Ozt3QkFHL0IsS0FBcUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFLEVBQXZFLGNBQWMsb0JBQUEsRUFBRSxjQUFjLG9CQUFBLENBQTBDO3dCQUNoRixJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTs0QkFDOUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxVQUFDLEdBQUc7Z0NBQ25DLElBQUksR0FBRyxFQUFFO29DQUNQLGdCQUFNLENBQUMsT0FBTyxDQUFDLDJDQUF5QyxjQUFjLHlCQUFvQixHQUFLLENBQUMsQ0FBQztpQ0FDbEc7NEJBQ0gsQ0FBQyxDQUFDLENBQUM7eUJBQ0o7d0JBQ0QsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7NEJBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFHO2dDQUNuQyxJQUFJLEdBQUcsRUFBRTtvQ0FDUCxnQkFBTSxDQUFDLE9BQU8sQ0FBQywyQ0FBeUMsY0FBYyx5QkFBb0IsR0FBSyxDQUFDLENBQUM7aUNBQ2xHOzRCQUNILENBQUMsQ0FBQyxDQUFDO3lCQUNKOzs7O2FBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVZLDZCQUFLLEdBQWxCOzs7Ozs7Ozt3QkFDRSxJQUFJLE9BQUEsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxNQUFNLEtBQUksT0FBQSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLE1BQU0sTUFBSyxJQUFJLEVBQUU7NEJBQy9FLE1BQU0sSUFBSSxLQUFLLENBQUMsc0hBQXNILENBQUMsQ0FBQzt5QkFDekk7d0JBRUssY0FBYyxHQUFtQixJQUFJLHVCQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUM1RSxLQUFBLElBQUksQ0FBQTt3QkFBWSxxQkFBTSxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTdELEdBQUssUUFBUSxHQUFHLFNBQTZDLENBQUM7d0JBRXhELGVBQWUsR0FBUSxJQUFJLENBQUMsT0FBTyxDQUFDLHFCQUFxQixDQUFDLENBQUM7Ozs7d0JBRS9ELHFCQUFNLG9CQUFZLENBQUMsVUFBTyxLQUFVLEVBQUUsS0FBYTs7Ozs7OzRDQUUvQyxLQUFBLElBQUksQ0FBQTs0Q0FBVyxxQkFBTSxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUE7OzRDQUF6QyxHQUFLLE9BQU8sR0FBRyxTQUEwQixDQUFDOzRDQUMxQyxlQUFlLENBQUMsT0FBTyxDQUFDLGtDQUFnQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsTUFBRyxDQUFDLENBQUM7NENBQ25GLHFCQUFNLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBQTs7NENBQXhCLFNBQXdCLENBQUM7NENBQ3pCLHNCQUFPOzs7NENBRVAsSUFBSSxJQUFFLENBQUMsSUFBSSxLQUFLLGNBQWMsRUFBRTtnREFDOUIsTUFBTSxJQUFFLENBQUM7NkNBQ1Y7NENBQ0QsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsd0NBQXNDLElBQUksQ0FBQyxDQUFDOzRDQUN6RCxnQkFBTSxDQUFDLElBQUksQ0FBQywwQkFBZSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsU0FBUyxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQzs0Q0FDbkYsS0FBSyxDQUFDLElBQUUsQ0FBQyxDQUFDOzs7OztpQ0FFYixDQUFDLEVBQUE7O3dCQWRGLFNBY0UsQ0FBQzs7Ozt3QkFFSCxlQUFlLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7d0JBQy9DLE1BQU0sR0FBQyxDQUFDOzt3QkFFVix1QkFBdUI7d0JBRXZCLGdCQUFNLENBQUMsSUFBSSxDQUFDLDhCQUE4QixDQUFDLENBQUM7d0JBQzVDLHFCQUFNLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxFQUFBOzt3QkFBL0IsU0FBK0IsQ0FBQzt3QkFFMUIsZ0JBQWdCLEdBQVEsSUFBSSxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDOzs7O3dCQUV4RSxxQkFBTSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBQTs7d0JBQTlCLFNBQThCLENBQUM7d0JBQy9CLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDOzs7O3dCQUV4RCxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsK0JBQStCLENBQUMsQ0FBQzt3QkFDdkQsTUFBTSxHQUFDLENBQUM7O3dCQUVKLE9BQU8sR0FBUSxJQUFJLENBQUMsT0FBTyxDQUFDLHVDQUF1QyxDQUFDLENBQUM7Ozs7d0JBRXpFLHFCQUFNLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxFQUFBOzt3QkFBekMsU0FBeUMsQ0FBQzt3QkFDMUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDOzs7O3dCQUV4QyxPQUFPLENBQUMsSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUM7d0JBQ3hDLGlCQUFpQjt3QkFDakIsTUFBTSxHQUFDLENBQUM7Ozs7O0tBRVg7SUFFTywyQ0FBbUIsR0FBM0I7UUFDRSxPQUFPLGFBQVcsSUFBSSxDQUFDLE1BQU0sU0FBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBTSxDQUFDO0lBQ2pFLENBQUM7SUFFYSw4Q0FBc0IsR0FBcEM7Ozs7Ozs0QkFDb0MscUJBQU0scUNBQTZCLE9BQ25FLElBQUksQ0FBQyxXQUFXLDBDQUFFLFNBQVMsRUFDM0IsSUFBSSxDQUFDLE1BQU0sUUFDWCxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLElBQUksUUFDNUIsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxJQUFJLENBQzlCLEVBQUE7O3dCQUxLLGlCQUFpQixHQUFXLFNBS2pDO3dCQUM0QixxQkFBTSxNQUFNLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDLEVBQUE7O3dCQUFuRSxTQUFTLEdBQWMsU0FBNEM7d0JBQ3pFLHFCQUFNLHNCQUFhLENBQUMsU0FBUyxDQUFDLEVBQUE7O3dCQUE5QixTQUE4QixDQUFDO3dCQUMvQixxQkFBTSw4QkFBc0IsT0FBQyxJQUFJLENBQUMsV0FBVywwQ0FBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sUUFBRSxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLElBQUksUUFBRSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLElBQUksQ0FBQyxFQUFBOzt3QkFBbkksU0FBbUksQ0FBQzs7Ozs7S0FDckk7SUFFYSxxQ0FBYSxHQUEzQjs7Ozs7Ozt3QkFDUSxXQUFXLEdBQVcsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7d0JBQ2pELEdBQUcsR0FBeUIsSUFBSSw2Q0FBb0IsQ0FBQyxFQUFFLFdBQVcsYUFBQSxFQUFFLENBQUMsQ0FBQzt3QkFDNUUsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsdUJBQXFCLFdBQWEsQ0FBQyxDQUFDO3dCQUVkLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxFQUFBOzt3QkFBakUsR0FBRyxHQUEwQixTQUFvQzt3QkFFakUsSUFBSSxTQUFrQyxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsSUFBSSwwQ0FBRSxJQUFJLENBQUM7d0JBQzVELGdCQUFNLENBQUMsS0FBSyxDQUFDLGlDQUErQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFHLENBQUMsQ0FBQzt3QkFDaEYsc0JBQU87Z0NBQ0wsSUFBSSxFQUFFLElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxXQUFXO2dDQUN2QixTQUFTLEVBQUUsSUFBSSxhQUFKLElBQUksdUJBQUosSUFBSSxDQUFFLFNBQVM7Z0NBQzFCLGVBQWUsRUFBRSxJQUFJLGFBQUosSUFBSSx1QkFBSixJQUFJLENBQUUsZUFBZTtnQ0FDdEMsZ0JBQWdCLEVBQUUsSUFBSSxhQUFKLElBQUksdUJBQUosSUFBSSxDQUFFLGdCQUFnQjs2QkFDekMsRUFBQzs7OztLQUNIO0lBRWEscUNBQWEsR0FBM0IsVUFBNEIsU0FBaUI7Ozs7OzRCQUNSLHFCQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxFQUFBOzt3QkFBdkUsR0FBRyxHQUEwQixTQUEwQzt3QkFDdkUsSUFBSSxHQUE4QixHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsSUFBSSxDQUFDO3dCQUNsRCxnQkFBTSxDQUFDLEtBQUssQ0FBQywwQkFBd0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBRyxDQUFDLENBQUM7Ozs7O0tBQzFFO0lBRU8sOENBQXNCLEdBQTlCOztRQUNFLElBQU0sbUJBQW1CLEdBQWtCLGdCQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9FLG1CQUFtQixDQUFDLElBQUksR0FBRyxvQkFBVyxJQUFJLENBQUMsT0FBTywwQ0FBRSxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUcsQ0FBQztRQUNoRixTQUFTO1FBQ1QsbUJBQW1CLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztRQUMxQyxZQUFZO1FBQ1osSUFBSSx5QkFBWSxDQUFDLG1CQUFtQixhQUFuQixtQkFBbUIsdUJBQW5CLG1CQUFtQixDQUFFLFNBQVMsQ0FBQyxFQUFFO1lBQ2hELE9BQU8sbUJBQW1CLENBQUMsU0FBUyxDQUFDO1NBQ3RDO1FBQ0QsT0FBTyxtQkFBbUIsQ0FBQztJQUM3QixDQUFDO0lBRU8sK0NBQXVCLEdBQS9CO1FBQUEsaUJBb0RDOztRQW5EQyxJQUFNLG9CQUFvQixHQUFtQjtZQUMzQyxJQUFJLFFBQUUsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxJQUFJO1lBQ25DLFdBQVcsUUFBRSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLFdBQVc7WUFDakQsT0FBTyxRQUFFLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsT0FBTztZQUN6QyxPQUFPLFFBQUUsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxPQUFPO1lBQ3pDLE9BQU8sUUFBRSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLE9BQU87WUFDekMsVUFBVSxRQUFFLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsVUFBVTtZQUMvQywwQkFBMEI7WUFDMUIsT0FBTyxRQUFFLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsT0FBTztZQUN6QyxvQkFBb0IsRUFBRTtnQkFDcEIsbUJBQW1CLEVBQUUsYUFBYSxDQUFDLGlCQUFpQjtnQkFDcEQsMEJBQTBCLFFBQUUsSUFBSSxDQUFDLE9BQU8sMENBQUUsZ0JBQWdCO2dCQUMxRCx5QkFBeUIsUUFBRSxJQUFJLENBQUMsT0FBTywwQ0FBRSxTQUFTO2dCQUNsRCxvQkFBb0IsUUFBRSxJQUFJLENBQUMsV0FBVywwQ0FBRSxXQUFXO2dCQUNuRCx3QkFBd0IsUUFBRSxJQUFJLENBQUMsV0FBVywwQ0FBRSxlQUFlO2dCQUMzRCxzQkFBc0IsRUFBRSxJQUFJO2FBQzdCO1NBQ0YsQ0FBQztRQUNGLCtFQUErRTtRQUMvRSxJQUFJLENBQUMsZ0JBQUMsQ0FBQyxPQUFPLENBQUMsMkJBQWMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxFQUFFO1lBQzFELGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFPOztnQkFDM0QsSUFBSSxPQUFBLEtBQUksQ0FBQyxrQkFBa0IsMENBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLE9BQU0sQ0FBQyxDQUFDLEVBQUU7b0JBQzVELE1BQU0sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsb0JBQW9CLEVBQUU7d0JBQ3ZELDRCQUE0QixFQUFFLElBQUk7cUJBQ25DLENBQUMsQ0FBQztpQkFDSjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLE9BQUEsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxxQkFBcUIsWUFBSSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLFdBQVcsQ0FBQSxFQUFFO1lBQzFGLE1BQU0sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ2xDLHFCQUFxQixRQUFFLElBQUksQ0FBQyxrQkFBa0IsMENBQUUscUJBQXFCO2dCQUNyRSxXQUFXLFFBQUUsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxXQUFXO2FBQ2xELENBQUMsQ0FBQztTQUNKO1FBQ0QsVUFBSSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLHVCQUF1QixFQUFFO1lBQ3BELE1BQU0sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ2xDLHVCQUF1QixRQUFFLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsdUJBQXVCO2FBQzFFLENBQUMsQ0FBQztTQUNKO1FBQ0QsVUFBSSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLG1CQUFtQixFQUFFO1lBQ2hELE1BQU0sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ2xDLG1CQUFtQixRQUFFLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsbUJBQW1CO2FBQ2xFLENBQUMsQ0FBQztTQUNKO1FBQ0QsVUFBSSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLGtCQUFrQixFQUFFO1lBQy9DLE1BQU0sQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ2xDLGtCQUFrQixRQUFFLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsa0JBQWtCO2FBQ2hFLENBQUMsQ0FBQztTQUNKO1FBRUQsT0FBTyxvQkFBb0IsQ0FBQztJQUM5QixDQUFDO0lBRU8sbURBQTJCLEdBQW5DO1FBQUEsaUJBbUNDO1FBbENDLElBQUksZ0JBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLEVBQUU7WUFDOUMsT0FBTyxFQUFFLENBQUM7U0FDWDtRQUNELElBQUksc0JBQXNCLEdBQXlCLEVBQUUsQ0FBQztRQUN0RCxLQUF5QixVQUErQixFQUEvQixLQUFBLElBQUksQ0FBQywwQkFBMEIsRUFBL0IsY0FBK0IsRUFBL0IsSUFBK0IsRUFBRTtZQUFyRCxJQUFNLFVBQVUsU0FBQTtZQUNuQixJQUFNLGVBQWUsR0FBa0IsVUFBVSxhQUFWLFVBQVUsdUJBQVYsVUFBVSxDQUFFLFlBQVksQ0FBQyxHQUFHLENBQUMsVUFBQyxTQUFTOztnQkFDNUUsSUFDRSxDQUFBLFNBQVMsYUFBVCxTQUFTLHVCQUFULFNBQVMsQ0FBRSxXQUFXO29CQUN0QixDQUFBLFNBQVMsYUFBVCxTQUFTLHVCQUFULFNBQVMsQ0FBRSxXQUFXLGFBQUssS0FBSSxDQUFDLGlCQUFpQiwwQ0FBRSxJQUFJLENBQUEsS0FDdkQsU0FBUyxhQUFULFNBQVMsdUJBQVQsU0FBUyxDQUFFLFlBQVksQ0FBQTtvQkFDdkIsQ0FBQSxTQUFTLGFBQVQsU0FBUyx1QkFBVCxTQUFTLENBQUUsWUFBWSxhQUFLLEtBQUksQ0FBQyxrQkFBa0IsMENBQUUsSUFBSSxDQUFBLEVBQ3pEO29CQUNBLElBQU0sTUFBTSxHQUFnQixnQkFBQyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDNUMsTUFBTSxhQUFOLE1BQU0sNEJBQU4sTUFBTSxDQUFFLFdBQVcsQ0FBQztvQkFDcEIsTUFBTSxhQUFOLE1BQU0sNEJBQU4sTUFBTSxDQUFFLFlBQVksQ0FBQztvQkFDNUIsT0FBTyxNQUFNLENBQUM7aUJBQ2Y7Z0JBQ0QsSUFBSSxFQUFDLFNBQVMsYUFBVCxTQUFTLHVCQUFULFNBQVMsQ0FBRSxXQUFXLENBQUEsSUFBSSxFQUFDLFNBQVMsYUFBVCxTQUFTLHVCQUFULFNBQVMsQ0FBRSxZQUFZLENBQUEsRUFBRTtvQkFDdkQsT0FBTyxTQUFTLENBQUM7aUJBQ2xCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFNLE1BQU0sR0FBdUI7Z0JBQ2pDLFVBQVUsRUFBRSxVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsVUFBVTtnQkFDbEMsUUFBUSxFQUFFLFVBQVUsYUFBVixVQUFVLHVCQUFWLFVBQVUsQ0FBRSxRQUFRO2dCQUM5QixZQUFZLEVBQUUsZUFBZSxDQUFDLE1BQU0sQ0FBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsRUFBRCxDQUFDLENBQUM7YUFDL0MsQ0FBQztZQUNGLElBQUksVUFBVSxhQUFWLFVBQVUsdUJBQVYsVUFBVSxDQUFFLFVBQVUsRUFBRTtnQkFDMUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7b0JBQ3BCLFVBQVUsRUFBRSxNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsVUFBVTtpQkFDL0IsQ0FBQyxDQUFDO2FBQ0o7WUFDRCxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDckM7UUFDRCxPQUFPLHNCQUFzQixDQUFDO0lBQ2hDLENBQUM7SUFFYSwwQ0FBa0IsR0FBaEM7Ozs7Ozt3QkFDUSxtQkFBbUIsR0FBa0IsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7d0JBQ25FLG9CQUFvQixHQUFtQixJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQzt3QkFFdEUsdUJBQXVCLEdBQW9CLElBQUksQ0FBQyxxQkFBcUIsQ0FBQzt3QkFDdEUsNEJBQTRCLEdBQXlCLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO3dCQUN4RixpQkFBaUIsR0FBc0IsSUFBSSw2QkFBaUIsQ0FDaEUsSUFBSSxDQUFDLE1BQU0sRUFDWCxtQkFBbUIsRUFDbkIsSUFBSSxDQUFDLE1BQU0sRUFDWCxJQUFJLENBQUMsT0FBTyxFQUNaLElBQUksQ0FBQyxJQUFJLEVBQ1Qsb0JBQW9CLEVBQ3BCLHVCQUF1QixFQUN2Qiw0QkFBNEIsQ0FDN0IsQ0FBQzt3QkFDSSx1QkFBdUIsR0FBZSxpQkFBaUIsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO3dCQUMxRyxxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLEVBQUE7O3dCQUF6RSxvQkFBb0IsR0FBUSxTQUE2Qzs7Ozt3QkFFdEQscUJBQU0sb0JBQW9CLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUE7O3dCQUEzRSxTQUFTLEdBQVEsU0FBMEQ7d0JBQ2pGLHFCQUFNLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxTQUFTLENBQUMsRUFBQTs7d0JBQWpELFNBQWlELENBQUM7d0JBRTVDLFdBQVcsR0FBUSxJQUFJLENBQUMsT0FBTyxDQUFDLDBEQUEwRCxDQUFDLENBQUM7Ozs7d0JBRWhHLHFCQUFNLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEVBQUE7O3dCQUF2RixTQUF1RixDQUFDO3dCQUN4RixXQUFXLENBQUMsT0FBTyxDQUFDLHdEQUF3RCxDQUFDLENBQUM7Ozs7d0JBRTlFLFdBQVcsQ0FBQyxJQUFJLENBQUMsMERBQTBELENBQUMsQ0FBQzt3QkFDN0UsTUFBTSxHQUFDLENBQUM7Ozs7d0JBR1YscUJBQU0sb0JBQW9CLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUE7O3dCQUExRCxTQUEwRCxDQUFDO3dCQUMzRCw2Q0FBNEIsQ0FBQyxHQUFDLENBQUMsQ0FBQzs7Ozs7O0tBRW5DO0lBRUssbURBQTJCLEdBQWpDLFVBQWtDLFNBQWM7Ozs7NEJBQzlDLHFCQUFNLG1CQUFZLENBQUMsY0FBYyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsRUFBQTs7d0JBQWhFLFNBQWdFLENBQUM7Ozs7O0tBQ2xFO0lBRUssbUNBQVcsR0FBakI7Ozs7NEJBQ0UscUJBQU0sbUJBQVksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsRUFBQTs7d0JBQTlELFNBQThELENBQUM7Ozs7O0tBQ2hFO0lBRUQsa0NBQVUsR0FBVjs7UUFDRSxPQUFVLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxTQUFJLElBQUksQ0FBQyxNQUFNLGdCQUFJLElBQUksQ0FBQyxpQkFBaUIsMENBQUUsSUFBSSxpQkFBSSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLElBQUksQ0FBRSxDQUFDO0lBQ3pILENBQUM7SUFFSyxrREFBMEIsR0FBaEMsVUFDRSxpQkFBeUIsRUFDekIsa0JBQTBCLEVBQzFCLGVBQXVCLEVBQ3ZCLFdBQW9COzs7Ozs7O3dCQUVkLEtBQUssR0FBVyxXQUFXLElBQUksUUFBUSxDQUFDOzZCQUUxQyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQWQsd0JBQWM7d0JBQ1YsY0FBYyxHQUFtQixJQUFJLHVCQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUM1RSxLQUFBLElBQUksQ0FBQTt3QkFBWSxxQkFBTSxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTdELEdBQUssUUFBUSxHQUFHLFNBQTZDLENBQUM7Ozs7d0JBRzlELHFCQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxFQUFFLEVBQUUsTUFBTSxFQUFFLGVBQWUsRUFBRSxDQUFDLEVBQUE7O3dCQUFqSCxTQUFpSCxDQUFDOzs7O3dCQUVsSCxNQUFNLElBQUksS0FBSyxDQUFDLGlDQUErQixHQUFDLHVFQUFvRSxDQUFDLENBQUM7NEJBRWhHLHFCQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxDQUFDLEVBQUE7O3dCQUF4RyxZQUFZLEdBQVEsU0FBb0Y7d0JBQ3hHLFFBQVEsR0FBVyxDQUFDLENBQUM7Ozs2QkFDbEIsQ0FBQSxPQUFBLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxJQUFJLDBDQUFFLE9BQU8sTUFBSyxDQUFDLElBQUksUUFBUSxJQUFJLGFBQWEsQ0FBQyxXQUFXLENBQUE7d0JBQ2hFLHFCQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLEVBQUUsa0JBQWtCLEVBQUUsS0FBSyxDQUFDLEVBQUE7O3dCQUFuRyxZQUFZLEdBQUcsU0FBb0YsQ0FBQzt3QkFDcEcsUUFBUSxHQUFHLFFBQVEsR0FBRyxDQUFDLENBQUM7d0JBQ3hCLGdCQUFNLENBQUMsS0FBSyxDQUFDLDZCQUEyQixRQUFRLFlBQVMsQ0FBQyxDQUFDO3dCQUMzRCxxQkFBTSxZQUFLLENBQUMsSUFBSSxDQUFDLEVBQUE7O3dCQUFqQixTQUFpQixDQUFDOzs7d0JBRXBCLElBQUksT0FBQSxZQUFZLGFBQVosWUFBWSx1QkFBWixZQUFZLENBQUUsSUFBSSwwQ0FBRSxPQUFPLE1BQUssQ0FBQyxFQUFFOzRCQUNyQyxnQkFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQzs0QkFDdkQsZUFBZTs0QkFDZixNQUFNLElBQUksS0FBSyxDQUFDLGtHQUFrRyxDQUFDLENBQUM7eUJBQ3JIO3dCQUNELGdCQUFNLENBQUMsS0FBSyxDQUFDLDJCQUF5QixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxJQUFJLENBQUcsQ0FBQyxDQUFDOzs7OztLQUM3RTtJQUVhLGlEQUF5QixHQUF2QyxVQUF3QyxXQUFtQixFQUFFLFlBQW9CLEVBQUUsS0FBYzs7Ozs7O29CQUMvRixVQUFVO29CQUNWLHFCQUFNLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBQTs7d0JBRDFFLFVBQVU7d0JBQ1YsU0FBMEUsQ0FBQzt3QkFFckUsTUFBTSxHQUFXLFFBQVEsQ0FBQzt3QkFDMUIsSUFBSSxHQUFXLGVBQWEsV0FBVyxTQUFJLEtBQUssbUJBQWMsWUFBWSxzQkFBbUIsQ0FBQzs2QkFDaEcsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFaLHdCQUFZO3dCQUNSLGNBQWMsR0FBbUIsSUFBSSx1QkFBYyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDNUUsS0FBQSxJQUFJLENBQUE7d0JBQVkscUJBQU0sY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUE7O3dCQUE3RCxHQUFLLFFBQVEsR0FBRyxTQUE2QyxDQUFDOzs0QkFFckMscUJBQU0sSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFBOzt3QkFBeEYsYUFBYSxHQUFRLFNBQW1FO3dCQUM5RixnQkFBTSxDQUFDLEtBQUssQ0FBQyxzQ0FBbUMsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLFVBQVUsQ0FBRSxDQUFDLENBQUM7Ozs7O0tBQzlFO0lBRWEsK0NBQXVCLEdBQXJDLFVBQXNDLFdBQW1CLEVBQUUsWUFBb0I7Ozs7Ozt3QkFDdkUsS0FBSyxHQUFXLFFBQVEsQ0FBQzt3QkFDL0IscUJBQXFCO3dCQUNyQixxQkFBTSxJQUFJLENBQUMsMEJBQTBCLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLEVBQUE7O3dCQUQxRSxxQkFBcUI7d0JBQ3JCLFNBQTBFLENBQUM7d0JBRXJFLE1BQU0sR0FBVyxLQUFLLENBQUM7d0JBQ3ZCLElBQUksR0FBVyxlQUFhLFdBQVcsU0FBSSxLQUFLLG1CQUFjLFlBQVksc0JBQW1CLENBQUM7NkJBQ2hHLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBZCx3QkFBYzt3QkFDVixjQUFjLEdBQW1CLElBQUksdUJBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQzVFLEtBQUEsSUFBSSxDQUFBO3dCQUFZLHFCQUFNLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFBOzt3QkFBN0QsR0FBSyxRQUFRLEdBQUcsU0FBNkMsQ0FBQzs7NEJBRXJDLHFCQUFNLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxvQkFBb0IsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUE7O3dCQUFqSCxhQUFhLEdBQVEsU0FBNEY7d0JBQ3ZILGdCQUFNLENBQUMsS0FBSyxDQUFDLG1DQUFnQyxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsVUFBVSxDQUFFLENBQUMsQ0FBQzs7Ozs7S0FDM0U7SUFDTyxxREFBNkIsR0FBckM7O1FBQ0UsSUFBSSxDQUFDLGdCQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUM1QixJQUFNLG9CQUFvQixHQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksUUFBRSxJQUFJLENBQUMsT0FBTywwQ0FBRSxTQUFTLENBQUMsQ0FBQztZQUNwRyw2Q0FBNkM7WUFDN0MsSUFBTSxjQUFjLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxZQUFZLENBQUMsQ0FBQztZQUM3RSxJQUFNLGNBQWMsR0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLFlBQVksQ0FBQyxDQUFDO1lBQzdFLE9BQU87Z0JBQ0wsY0FBYyxnQkFBQTtnQkFDZCxjQUFjLGdCQUFBO2FBQ2YsQ0FBQztTQUNIO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBQ2EseUNBQWlCLEdBQS9COzs7Ozs7O29CQUNFLHFCQUFxQjtvQkFDckIscUJBQU0sd0JBQWUsQ0FDbkIsYUFBYSxDQUFDLG1CQUFtQixFQUNqQyxhQUFhLENBQUMsY0FBYyxFQUM1QixhQUFhLENBQUMsY0FBYyxFQUM1QixhQUFhLENBQUMsaUJBQWlCLENBQ2hDLEVBQUE7O3dCQU5ELHFCQUFxQjt3QkFDckIsU0FLQyxDQUFDO3dCQUVJLElBQUksR0FBUSxJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQzt3QkFDOUMsS0FBcUMsSUFBSSxDQUFDLDZCQUE2QixFQUFFLEVBQXZFLGNBQWMsb0JBQUEsRUFBRSxjQUFjLG9CQUFBLENBQTBDO3dCQUNoRixxQkFBTSxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBQTs7d0JBQWpELFNBQWlELENBQUM7d0JBQ2xELGdCQUFNLENBQUMsS0FBSyxDQUFDLGlCQUFjLElBQUksYUFBSixJQUFJLHVCQUFKLElBQUksQ0FBRSxJQUFJLHFCQUFlLGNBQWMscUJBQWdCLGNBQWdCLENBQUMsQ0FBQzt3QkFDcEcsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7d0JBQ3ZILElBQUksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO3dCQUMzRixxQkFBTSxxQkFBWSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUE7O3dCQUF0RyxjQUFjLEdBQVEsU0FBZ0Y7d0JBQzVHLElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxhQUFkLGNBQWMsdUJBQWQsY0FBYyxDQUFFLE1BQU0sQ0FBQzt3QkFDN0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFjLGFBQWQsY0FBYyx1QkFBZCxjQUFjLENBQUUsU0FBUyxDQUFDO3dCQUN4QyxxQkFBTSxJQUFJLENBQUMsb0JBQW9CLE9BQUMsSUFBSSxDQUFDLE1BQU0sMENBQUUsRUFBRSxDQUFDLEVBQUE7O3dCQUFoRCxTQUFnRCxDQUFDOzs7OztLQUNsRDtJQUVhLDRDQUFvQixHQUFsQyxVQUFtQyxXQUFtQjs7Ozs0QkFDcEQscUJBQU0sbUJBQVksQ0FBQyxrQkFBa0IsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLEVBQUE7O3dCQUF0RSxTQUFzRSxDQUFDOzs7OztLQUN4RTtJQUVhLDZDQUFxQixHQUFuQzs7Ozs0QkFDRSxxQkFBTSxxQkFBYyxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxFQUFBOzt3QkFBM0QsU0FBMkQsQ0FBQzs7Ozs7S0FDN0Q7SUFDTyx1REFBK0IsR0FBdkM7OztRQUNFLElBQU0sT0FBTyxTQUFXLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsT0FBTyxDQUFDO1FBQ3pELElBQUksa0NBQXdCLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDckMsT0FBTyxFQUFFLENBQUM7U0FDWDtRQUNELElBQU0sV0FBVyxHQUFNLElBQUksQ0FBQyxTQUFTLFNBQU0sQ0FBQztRQUU1QyxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssV0FBVyxFQUFFO1lBQ2pDLElBQUksT0FBTyxLQUFLLFdBQVcsSUFBSSxPQUFPLEtBQUssU0FBUyxFQUFFO2dCQUNwRCxNQUFNLElBQUksS0FBSyxDQUFJLFdBQVcsZ0VBQTZELENBQUMsQ0FBQzthQUM5RjtpQkFBTTtnQkFDTCxPQUFPLEVBQUUsQ0FBQzthQUNYO1NBQ0Y7YUFBTSxJQUFJLE9BQU8sS0FBSyxRQUFRLEVBQUU7WUFDL0IsT0FBTyxFQUFFLENBQUM7U0FDWDthQUFNO1lBQ0wsT0FBTztnQkFDTCxZQUFZO29CQUNWLEdBQUMsV0FBVyxJQUFHLEVBQUU7dUJBQ2xCO2dCQUNELFVBQVUsRUFBRTtvQkFDVixZQUFZO3dCQUNWLEdBQUMsV0FBVyxJQUFHOzRCQUNiO2dDQUNFLE1BQU0sRUFBRSxFQUFFO2dDQUNWLFFBQVEsRUFBRSxLQUFHLElBQUksQ0FBQyxTQUFXOzZCQUM5Qjt5QkFDRjsyQkFDRjtpQkFDRjthQUNGLENBQUM7U0FDSDtJQUNILENBQUM7SUFDTyxrREFBMEIsR0FBbEM7O1FBQ0UsSUFBTSxTQUFTLEdBQWMsYUFBYSxDQUFDLG1CQUFtQixTQUFJLGFBQWEsQ0FBQyxjQUFjLFNBQUksYUFBYSxDQUFDLGNBQWMsU0FBSSxhQUFhLENBQUMsaUJBQW1CLENBQUM7UUFDcEssSUFBTSxhQUFhLEdBQVcsa0NBQXFCLE9BQUMsSUFBSSxDQUFDLE9BQU8sMENBQUUsU0FBUyxDQUFDLENBQUM7UUFDN0UsSUFBTSxNQUFNLEdBQUc7WUFDYixTQUFTLEVBQUUsS0FBSztZQUNoQixHQUFHLEVBQUUsS0FBSztZQUNWLFNBQVMsRUFBRSxJQUFJO1lBQ2YsV0FBVyxFQUFFLElBQUk7WUFDakIsWUFBWSxFQUFFLElBQUk7WUFDbEIsWUFBWSxFQUFFLElBQUk7U0FDbkIsQ0FBQztRQUNGLElBQU0sUUFBUSxHQUFHO1lBQ2YsVUFBVSxFQUFFO2dCQUNWLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsTUFBTSxFQUFFLEVBQUU7YUFDWDtTQUNGLENBQUM7UUFDRixJQUFJLFNBQVMsR0FBUSxFQUFFLENBQUM7UUFDeEIsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLFNBQVMsR0FBRyxJQUFJLENBQUMsK0JBQStCLEVBQUUsQ0FBQztTQUNwRDtRQUNELElBQU0sSUFBSSxHQUFRLDhCQUFrQixDQUNsQztZQUNFLEdBQUcsRUFBRSxJQUFJLENBQUMseUJBQXlCLEVBQUU7WUFDckMsS0FBSyxFQUFFLFNBQVM7WUFDaEIsSUFBSSxFQUFFLGFBQWE7WUFDbkIsSUFBSSxFQUFFLEtBQUs7U0FDWixFQUNELE1BQU0sRUFDTixRQUFRLEVBQ1IsU0FBUyxDQUNWLENBQUM7UUFDRixJQUFNLGFBQWEsR0FBUSxnQkFBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxJQUFJLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxHQUFHLEVBQUU7WUFDdEIsSUFBTSxZQUFZLEdBQVEsYUFBYSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBQyxDQUFTO2dCQUN4RCxJQUFJLENBQUMsQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLENBQUMsVUFBVSxDQUFDLDBCQUEwQixDQUFDLEVBQUU7b0JBQ3BGLElBQU0sWUFBWSxHQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzVDLElBQU0sV0FBVyxHQUFXLFlBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbEQsT0FBVSxZQUFZLENBQUMsQ0FBQyxDQUFDLFNBQUksV0FBYSxDQUFDO2lCQUM1QztxQkFBTTtvQkFDTCxPQUFPLENBQUMsQ0FBQztpQkFDVjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsYUFBYSxDQUFDLEdBQUcsR0FBRyxZQUFZLENBQUM7U0FDbEM7UUFDRCxnQkFBTSxDQUFDLEtBQUssQ0FBQyw2Q0FBMkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBRyxDQUFDLENBQUM7UUFFckcsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRU8saURBQXlCLEdBQWpDOztRQUNFLElBQU0sSUFBSSxHQUFRO1lBQ2hCLG1CQUFtQixFQUFFLGFBQWEsQ0FBQyxpQkFBaUI7WUFDcEQseUJBQXlCLFFBQUUsSUFBSSxDQUFDLE9BQU8sMENBQUUsU0FBUztZQUNsRCwwQkFBMEIsUUFBRSxJQUFJLENBQUMsT0FBTywwQ0FBRSxlQUFlO1lBQ3pELG9CQUFvQixRQUFFLElBQUksQ0FBQyxXQUFXLDBDQUFFLFdBQVc7WUFDbkQsd0JBQXdCLFFBQUUsSUFBSSxDQUFDLFdBQVcsMENBQUUsZUFBZTtTQUM1RCxDQUFDO1FBQ0YsT0FBTyxnQkFBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxFQUFFLFVBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSyxPQUFHLENBQUMsU0FBSSxDQUFHLEVBQVgsQ0FBVyxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUVhLG9EQUE0QixHQUExQzs7Ozs7OzRCQUNnQyxxQkFBTSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsT0FBQyxJQUFJLENBQUMsT0FBTywwQ0FBRSxTQUFTLENBQUMsRUFBQTs7d0JBQS9FLEdBQUcsR0FBdUIsU0FBcUQ7d0JBQy9FLEtBQUssZUFBVyxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsSUFBSSwwQ0FBRSxJQUFJLDBDQUFFLE1BQU0sQ0FBQzt3QkFDeEMsUUFBUSxHQUFXLENBQUMsQ0FBQzs7OzZCQUNsQixDQUFBLEtBQUssS0FBSyxhQUFhLElBQUksUUFBUSxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUE7d0JBQ3BFLHFCQUFNLFlBQUssQ0FBQyxJQUFJLENBQUMsRUFBQTs7d0JBQWpCLFNBQWlCLENBQUM7d0JBQ1oscUJBQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLE9BQUMsSUFBSSxDQUFDLE9BQU8sMENBQUUsU0FBUyxDQUFDLEVBQUE7O3dCQUEzRCxHQUFHLEdBQUcsU0FBcUQsQ0FBQzt3QkFDNUQsS0FBSyxlQUFHLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxJQUFJLDBDQUFFLElBQUksMENBQUUsTUFBTSxDQUFDO3dCQUNoQyxRQUFRLEdBQUcsUUFBUSxHQUFHLENBQUMsQ0FBQzs7O3dCQUUxQixJQUFJLEtBQUssS0FBSyxhQUFhLEVBQUU7NEJBQzNCLE1BQU0sSUFBSSxLQUFLLENBQUMsa0RBQWdELElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFHLENBQUMsQ0FBQzt5QkFDMUc7Ozs7O0tBQ0Y7SUFFWSw0Q0FBb0IsR0FBakMsVUFBa0MsSUFBYTs7Ozs7OzRCQUN2QyxxQkFBTSxJQUFJLENBQUMsNEJBQTRCLEVBQUUsRUFBQTs7NkJBQTNDLENBQUMsQ0FBQyxTQUF5QyxDQUFDLEVBQTVDLHdCQUE0Qzt3QkFDOUMscUJBQU0sSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFBOzt3QkFBbEIsU0FBa0IsQ0FBQzt3QkFDbkIscUJBQU0sSUFBSSxDQUFDLHNCQUFzQixFQUFFLEVBQUE7O3dCQUFuQyxTQUFtQyxDQUFDO3dCQUNwQyxNQUFNLElBQUksS0FBSyxDQUFDLHFGQUFxRixDQUFDLENBQUM7NEJBRS9FLHFCQUFNLGdDQUF3QixPQUN0RCxJQUFJLENBQUMsV0FBVywwQ0FBRSxTQUFTLEVBQzNCLElBQUksQ0FBQyxNQUFNLFFBQ1gsSUFBSSxDQUFDLGlCQUFpQiwwQ0FBRSxJQUFJLFFBQzVCLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsSUFBSSxDQUM5QixFQUFBOzt3QkFMSyxZQUFZLEdBQVEsU0FLekI7d0JBRUssbUJBQW1CLEdBQWtCLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxPQUFPLENBQUM7d0JBQzNELG9CQUFvQixHQUFtQixZQUFZLGFBQVosWUFBWSx1QkFBWixZQUFZLENBQUUsUUFBUSxDQUFDO3dCQUNwRSwyRUFBMkU7d0JBQzNFLGdCQUFNLENBQUMsSUFBSSxDQUNULCtCQUE0QixtQkFBbUIsYUFBbkIsbUJBQW1CLHVCQUFuQixtQkFBbUIsQ0FBRSxJQUFJLHNCQUFlLG9CQUFvQixhQUFwQixvQkFBb0IsdUJBQXBCLG9CQUFvQixDQUFFLElBQUkscUJBQWUsSUFBSSxDQUFDLE1BQU0saUNBQThCLENBQ3ZKLENBQUM7d0JBQ0ksdUJBQXVCLEdBQTRCLElBQUksMENBQXVCLENBQ2xGLElBQUksQ0FBQyxNQUFNLEVBQ1gsbUJBQW1CLGFBQW5CLG1CQUFtQix1QkFBbkIsbUJBQW1CLENBQUUsSUFBSSxFQUN6QixJQUFJLENBQUMsTUFBTSxFQUNYLElBQUksQ0FBQyxPQUFPLEVBQ1osSUFBSSxDQUFDLElBQUksRUFDVCxvQkFBb0IsYUFBcEIsb0JBQW9CLHVCQUFwQixvQkFBb0IsQ0FBRSxJQUFJLENBQzNCLENBQUM7d0JBU0ksTUFBTSxHQUFlLHVCQUF1QixDQUFDLGtCQUFrQixDQUFDLGtCQUFrQixFQUFFLDBCQUEwQixFQUFFLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQzt3QkFDOUYscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxFQUFBOzt3QkFBdEYsMEJBQTBCLEdBQVEsU0FBb0Q7d0JBQzVGLHFCQUFNLDBCQUEwQixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQS9DLFNBQStDLENBQUM7Ozs7O0tBQ2pEO0lBRVksb0RBQTRCLEdBQXpDOzs7Ozs7NEJBQ21DLHFCQUFNLG9DQUE0QixPQUNqRSxJQUFJLENBQUMsV0FBVywwQ0FBRSxTQUFTLEVBQzNCLElBQUksQ0FBQyxNQUFNLFFBQ1gsSUFBSSxDQUFDLGlCQUFpQiwwQ0FBRSxJQUFJLFFBQzVCLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsSUFBSSxDQUM5QixFQUFBOzt3QkFMSyxnQkFBZ0IsR0FBVyxTQUtoQzt3QkFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7NEJBQ3JCLHNCQUFPLEtBQUssRUFBQzt5QkFDZDt3QkFDK0IscUJBQU0sTUFBTSxDQUFDLGNBQWMsRUFBRSxFQUFBOzt3QkFBdkQsaUJBQWlCLEdBQVEsQ0FBQyxTQUE2QixDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsU0FBUyxXQUFLLFNBQVMsYUFBVCxTQUFTLHVCQUFULFNBQVMsQ0FBRSxFQUFFLEdBQUEsQ0FBQzt3QkFDaEcsSUFBSSxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsRUFBRTs0QkFDaEQsc0JBQU8sSUFBSSxFQUFDO3lCQUNiO3dCQUNELHNCQUFPLEtBQUssRUFBQzs7OztLQUNkO0lBRVksNkJBQUssR0FBbEI7Ozs7Ozs0QkFDNEIscUJBQU0sZ0NBQXdCLE9BQ3RELElBQUksQ0FBQyxXQUFXLDBDQUFFLFNBQVMsRUFDM0IsSUFBSSxDQUFDLE1BQU0sUUFDWCxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLElBQUksUUFDNUIsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxJQUFJLENBQzlCLEVBQUE7O3dCQUxLLFlBQVksR0FBUSxTQUt6Qjt3QkFDd0IscUJBQU0sMkJBQW1CLE9BQ2hELElBQUksQ0FBQyxXQUFXLDBDQUFFLFNBQVMsRUFDM0IsSUFBSSxDQUFDLE1BQU0sUUFDWCxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLElBQUksUUFDNUIsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxJQUFJLENBQzlCLEVBQUE7O3dCQUxLLE9BQU8sR0FBWSxTQUt4Qjt3QkFDZ0MscUJBQU0sb0NBQTRCLE9BQ2pFLElBQUksQ0FBQyxXQUFXLDBDQUFFLFNBQVMsRUFDM0IsSUFBSSxDQUFDLE1BQU0sUUFDWCxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLElBQUksUUFDNUIsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxJQUFJLENBQzlCLEVBQUE7O3dCQUxLLGdCQUFnQixHQUFXLFNBS2hDO3dCQUNLLG1CQUFtQixHQUFrQixZQUFZLGFBQVosWUFBWSx1QkFBWixZQUFZLENBQUUsT0FBTyxDQUFDO3dCQUMzRCxvQkFBb0IsR0FBbUIsWUFBWSxhQUFaLFlBQVksdUJBQVosWUFBWSxDQUFFLFFBQVEsQ0FBQzt3QkFDOUQsdUJBQXVCLEdBQW9CLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxRQUFRLENBQUM7NkJBRXBFLGdCQUFnQixFQUFoQix3QkFBZ0I7Ozs7d0JBRVYsQ0FBQyxHQUFRLE1BQU0sQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzt3QkFDckQscUJBQU0sc0JBQWEsQ0FBQyxDQUFDLENBQUMsRUFBQTs7d0JBQXRCLFNBQXNCLENBQUM7d0JBQ3ZCLHFCQUFNLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxFQUFBOzt3QkFBbEMsU0FBa0MsQ0FBQzs7Ozt3QkFFbkMsZ0JBQU0sQ0FBQyxPQUFPLENBQUMsMEJBQWUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLGtCQUFnQixnQkFBa0IsRUFBRSxHQUFDLGFBQUQsR0FBQyx1QkFBRCxHQUFDLENBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDN0gsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsMkJBQXlCLGdCQUFnQixnQkFBVyxHQUFHLENBQUMsQ0FBQzs7O3dCQUtwRSxhQUFhLEdBQVEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDOzs7O3dCQUU3RSxxQkFBTSxJQUFJLENBQUMseUJBQXlCLENBQUMsbUJBQW1CLGFBQW5CLG1CQUFtQix1QkFBbkIsbUJBQW1CLENBQUUsSUFBSSxFQUFFLG9CQUFvQixhQUFwQixvQkFBb0IsdUJBQXBCLG9CQUFvQixDQUFFLElBQUksRUFBRSxRQUFRLENBQUMsRUFBQTs7d0JBQXJHLFNBQXFHLENBQUM7d0JBQ3RHLGFBQWEsQ0FBQyxPQUFPLENBQUMsNERBQTRELENBQUMsQ0FBQzs7Ozt3QkFFcEYsYUFBYSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt3QkFDbkMsZ0JBQU0sQ0FBQyxLQUFLLENBQUMsR0FBQyxhQUFELEdBQUMsdUJBQUQsR0FBQyxDQUFFLE9BQU8sQ0FBQyxDQUFDO3dCQUN6QixnQkFBTSxDQUFDLEtBQUssQ0FBQyxZQUFVLEdBQUcsQ0FBQyxDQUFDOzs7O3dCQUl0QixpQkFBaUIsR0FBc0IsSUFBSSw2QkFBaUIsQ0FDaEUsSUFBSSxDQUFDLE1BQU0sRUFDWCxtQkFBbUIsRUFDbkIsSUFBSSxDQUFDLE1BQU0sRUFDWCxJQUFJLENBQUMsT0FBTyxFQUNaLElBQUksQ0FBQyxJQUFJLEVBQ1Qsb0JBQW9CLEVBQ3BCLHVCQUF1QixDQUN4QixDQUFDO3dCQUNnQyxxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLG1CQUFtQixDQUFDLEVBQUE7O3dCQUF6RSxvQkFBb0IsR0FBUSxTQUE2Qzt3QkFFekUsdUJBQXVCLEdBQWUsaUJBQWlCLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLG1CQUFtQixFQUFFLFlBQVksRUFBRSxRQUFRLENBQUMsQ0FBQzt3QkFDM0kscUJBQU0sb0JBQW9CLENBQUMsTUFBTSxDQUFDLHVCQUF1QixDQUFDLEVBQUE7O3dCQUExRCxTQUEwRCxDQUFDOzs7O3dCQUUzRCxnQkFBTSxDQUFDLE9BQU8sQ0FDWiwwQkFBZSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQ2xDLHVCQUF1QixFQUN2QixtQkFBZ0IsbUJBQW1CLGFBQW5CLG1CQUFtQix1QkFBbkIsbUJBQW1CLENBQUUsSUFBSSwwQkFBbUIsb0JBQW9CLGFBQXBCLG9CQUFvQix1QkFBcEIsb0JBQW9CLENBQUUsSUFBSSxDQUFFLEVBQ3hGLEdBQUMsYUFBRCxHQUFDLHVCQUFELEdBQUMsQ0FBRSxPQUFPLENBQ1gsQ0FDRixDQUFDO3dCQUNGLGdCQUFNLENBQUMsS0FBSyxDQUFDLFlBQVUsR0FBRyxDQUFDLENBQUM7Ozt3QkFJeEIsZUFBZSxHQUFRLElBQUksQ0FBQyxPQUFPLENBQUMsd0JBQXFCLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxTQUFTLFNBQUssQ0FBQyxDQUFDOzs7O3dCQUV0RixxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxTQUFTLENBQUMsRUFBQTs7d0JBQTVDLFNBQTRDLENBQUM7d0JBQzdDLGVBQWUsQ0FBQyxPQUFPLENBQUMsc0JBQW1CLE9BQU8sYUFBUCxPQUFPLHVCQUFQLE9BQU8sQ0FBRSxTQUFTLFlBQVEsQ0FBQyxDQUFDOzs7O3dCQUV2RSxlQUFlLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dCQUN0QyxnQkFBTSxDQUFDLEtBQUssQ0FBQyxJQUFDLGFBQUQsSUFBQyx1QkFBRCxJQUFDLENBQUUsT0FBTyxDQUFDLENBQUM7d0JBQ3pCLGdCQUFNLENBQUMsS0FBSyxDQUFDLFlBQVUsSUFBRyxDQUFDLENBQUM7Ozs7OztLQUUvQjtJQUVNLGtDQUFVLEdBQWpCO1FBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3RCLENBQUM7SUF4cUJ1Qix5QkFBVyxHQUFXLEVBQUUsQ0FBQztJQUN6QiwrQkFBaUIsR0FBVyx5Q0FBeUMsQ0FBQztJQUM5RixnSUFBZ0k7SUFDeEcsNEJBQWMsR0FBVyxpQkFBaUIsQ0FBQztJQUMzQyxxQ0FBdUIsR0FBVyxRQUFRLENBQUM7SUFDbkUsdUVBQXVFO0lBQ3ZFLHVEQUF1RDtJQUN2RCx5R0FBeUc7SUFDakYsK0JBQWlCLEdBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkNBQTJDLENBQUMsSUFBSSxhQUFhLENBQUMsdUJBQXVCLENBQUM7SUFDNUUsNEJBQWMsR0FBVyxVQUFVLENBQUM7SUFDcEMsaUNBQW1CLEdBQVcsbUNBQW1DLENBQUM7SUFDbEUsMEJBQVksR0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLGtCQUFrQixDQUFDLENBQUM7SUFDbEYsNENBQThCLEdBQWEsQ0FBQyxRQUFRLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztJQTRwQnpHLG9CQUFDO0NBQUEsQUF6ckJELElBeXJCQztrQkF6ckJvQixhQUFhIn0=