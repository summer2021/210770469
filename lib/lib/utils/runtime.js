"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isCustomContainerRuntime = void 0;
function isCustomContainerRuntime(runtime) {
    return runtime === 'custom-container';
}
exports.isCustomContainerRuntime = isCustomContainerRuntime;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicnVudGltZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NyYy9saWIvdXRpbHMvcnVudGltZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSxTQUFnQix3QkFBd0IsQ0FBQyxPQUFlO0lBQ3BELE9BQU8sT0FBTyxLQUFLLGtCQUFrQixDQUFDO0FBQzFDLENBQUM7QUFGRCw0REFFQyJ9