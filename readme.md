## 组件说明

fc-remote-invoke 在远程建立一个与本服务相同的调试服务，辅助用户进行远程调试

## 测试运行

进入 `example` 目录下的任意一个测试环境，启动调试 session：

```bash
cd example/python-http
s setup
```

此时终端被阻塞，另开一终端进行调试调用：

```bash
s invoke
```

可以看到调用成功：

```bash
https://1986114430573743.cn-qingdao.fc.aliyuncs.com/2016-08-15/proxy/SESSION-S-a7b0b/http-trigger-function/
========= FC invoke Logs begin =========
FC Initialize Start RequestId: d9073464-f25a-4f18-a396-32a57c1f188a
2021-08-13T12:55:18.754Z d9073464-f25a-4f18-a396-32a57c1f188a [INFO] initializing
FC Initialize End RequestId: d9073464-f25a-4f18-a396-32a57c1f188a
FC Invoke Start RequestId: 43016bf4-a2c1-4af1-9c49-2284ad3a14ce
2021-08-13T12:55:45.776Z 43016bf4-a2c1-4af1-9c49-2284ad3a14ce [INFO] debug1
2021-08-13T12:55:45.776Z 43016bf4-a2c1-4af1-9c49-2284ad3a14ce [INFO] debug2
2021-08-13T12:55:45.776Z 43016bf4-a2c1-4af1-9c49-2284ad3a14ce [INFO] debug3
2021-08-13T12:55:45.776Z 43016bf4-a2c1-4af1-9c49-2284ad3a14ce [INFO] debug4
FC Invoke End RequestId: 43016bf4-a2c1-4af1-9c49-2284ad3a14ce

Duration: 4.34 ms, Billed Duration: 5 ms, Memory Size: 128 MB, Max Memory Used: 97.83 MB
========= FC invoke Logs end =========

FC Invoke Result:
Hello world!



End of method: invoke
```

最后，进行远程调试资源的清除：

```bash
s clean
```

## 单步调试

以 vscode 为例，进行 python 代码的单步调试。

产生调试配置，建立 session，此时终端阻塞：

```bash
s setup --config vscode --debug-port 3000
```

开启 vscode 下的调试模式，设立对应断点。并新建另一终端，运行以下命令启动调试：

```bash
s invoke
```

此时可以看到程序运行并停留在断点处。
